package eu.easyrad.erad.Activities;

import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Fragments.CalibrateWheelDialog;
import eu.easyrad.erad.Fragments.ChooseFileDialog;
import eu.easyrad.erad.Fragments.SetupFragment;
import eu.easyrad.erad.Fragments.LandingScreenFragment;
import eu.easyrad.erad.Fragments.OptionsFragment;
import eu.easyrad.erad.Fragments.RadarConnectionFragment;
import eu.easyrad.erad.Fragments.ReadFragment;
import eu.easyrad.erad.Fragments.RecordDialog;
import eu.easyrad.erad.Fragments.SettingsFragment;
import eu.easyrad.erad.Fragments.SpectrogramFragment;
import eu.easyrad.erad.Fragments.TestWheelDialog;
import eu.easyrad.erad.Fragments.WaveFragment;
import eu.easyrad.erad.Fragments.WriteFragment;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Model.EradFileHeader;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.Model.Utilities;
import eu.easyrad.erad.R;
import eu.easyrad.erad.View.Label;

public class MainActivity extends FragmentActivity implements OptionsFragment.onOptionSelectedListener,
        OptionsFragment.onMediumSelectedListener,
        OptionsFragment.onPaletteSelectedListener,
        OptionsFragment.onRecordOptionSelectedListener,
        LandingScreenFragment.onOptionSelectedListener,
        RadarConnectionFragment.RadarConnectionListener,
        WaveFragment.onFilterAppliedListener,
        WaveFragment.onRemoteFilterAppliedListener,
        SetupFragment.SetUpListener,
        CalibrateWheelDialog.WheelCalibrationListener,
        RecordDialog.RecordDialogInterface,
        ChooseFileDialog.SelectedFileListener,
        ReadFragment.FileCheckerListener,
        SpectrogramFragment.FileNavigationListener,
        WriteFragment.RecordingFinishedCallback,
        TestWheelDialog.WheelTestingListener{

    //Logging
    public static final String TAG = MainActivity.class.getSimpleName();

    //Fragments
    SetupFragment setupFragment;
    OptionsFragment optionsFragment;
    WaveFragment waveFragment;
    SpectrogramFragment spectroFragment;
    LandingScreenFragment landingScreenFragment;
    RadarConnectionFragment radarConnectionFragment;
    WriteFragment writeFragment;
    ReadFragment readFragment;
    SettingsFragment settingsFragment;

    android.support.v4.app.FragmentManager manager;

    FrameLayout landingScreenPlaceholder;
    FrameLayout wavePlaceholder;
    FrameLayout setupPlaceholder;
    FrameLayout preferencePlaceholder;


    PreferenceManager preferenceManager;


    short workMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }

        super.onCreate(savedInstanceState);

        setContentView(eu.easyrad.erad.R.layout.activity_layout);

        preferenceManager = PreferenceManager.getInstance(this);
        manager = getSupportFragmentManager();

        landingScreenPlaceholder = (FrameLayout) findViewById(eu.easyrad.erad.R.id.landing_screen_placeholder);
        wavePlaceholder = (FrameLayout) findViewById(eu.easyrad.erad.R.id.wave_placeholder);
        setupPlaceholder = (FrameLayout) findViewById(R.id.setup_placeholder);
        preferencePlaceholder = (FrameLayout) findViewById(R.id.preference_placeholder);

        landingScreenFragment = (LandingScreenFragment) manager.findFragmentById(eu.easyrad.erad.R.id.landing_screen_placeholder);
        waveFragment = (WaveFragment) manager.findFragmentById(eu.easyrad.erad.R.id.wave_placeholder);
        optionsFragment = (OptionsFragment) manager.findFragmentById(eu.easyrad.erad.R.id.options_placeholder);
        spectroFragment = (SpectrogramFragment) manager.findFragmentById(eu.easyrad.erad.R.id.spectrogram_placeholder);

        //if first time run
        if (!preferenceManager.isSetupComplete()) {
            if (BuildConfig.DEBUG){
                Log.i(TAG, "setUpIs not Complete");
            }
            if (setupFragment == null) {
                setupFragment = SetupFragment.newInstance();
            }
            setupPlaceholder.setVisibility(View.VISIBLE);
            manager.beginTransaction().add(R.id.setup_placeholder, setupFragment, "setupFragment").commit();

        }

        if (savedInstanceState!=null){
            workMode = savedInstanceState.getShort("workMode");

            switch (workMode){
                case Constants.MODE_LANDING_SCREEN:
                    landingScreenPlaceholder.setVisibility(View.VISIBLE);
                    break;

                case Constants.MODE_SCAN:
                    radarConnectionFragment = (RadarConnectionFragment) manager.findFragmentByTag("radarConnectionFrag");
                    landingScreenPlaceholder.setVisibility(View.GONE);
                    wavePlaceholder.setVisibility(View.VISIBLE);
                    break;

                case Constants.MODE_READ_2D:

                    landingScreenPlaceholder.setVisibility(View.GONE);
                    wavePlaceholder.setVisibility(View.GONE);
                    readFragment = (ReadFragment)manager.findFragmentByTag("readFragment");

                    try {
                        optionsFragment.setHeaderInfo(readFragment.getHeader());
                    } catch (IOException e){
                        e.printStackTrace();
                    }

                    try {

                        if (spectroFragment.getRecordedWithWheel()){
                            readFragment.fetchStepIndices();
                        }
                    } catch (NullPointerException e){
                        e.printStackTrace();
                    }

                    break;

                case Constants.MODE_READ_3d:
                    landingScreenPlaceholder.setVisibility(View.GONE);
                    wavePlaceholder.setVisibility(View.GONE);
                    break;

                case Constants.MODE_SETTINGS:
                    landingScreenPlaceholder.setVisibility(View.GONE);
                    preferencePlaceholder.setVisibility(View.VISIBLE);
                    break;

            }
        } else {
            FragmentTransaction transaction = manager.beginTransaction();
            if (landingScreenFragment==null){
                landingScreenFragment = LandingScreenFragment.newInstance();
            }
            landingScreenPlaceholder.setVisibility(View.VISIBLE);
            workMode = Constants.MODE_LANDING_SCREEN;
            transaction.add(R.id.landing_screen_placeholder, landingScreenFragment).commit();


        }



    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttachFragment");
        }
        super.onAttachFragment(fragment);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }

        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putShort("workMode", workMode);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        if (BuildConfig.DEBUG){
            Log.i(TAG, "onRestoreInstanceState");
        }

        super.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    public void onStart() {

        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }

        super.onStart();

    }

    @Override
    public void onResume() {

        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }

        super.onResume();

    }

    @Override
    public void onPause() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }

        super.onPause();

    }

    @Override
    public void onStop() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onStop();

    }

    @Override
    public void onDestroy() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }

        super.onDestroy();
        preferenceManager = null;

    }

    @Override
    public void onBackPressed() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onBackPressed");
        }

        if (manager.getBackStackEntryCount() > 0) {

            if (radarConnectionFragment != null) {
                radarConnectionFragment.pauseConnection();
                radarConnectionFragment.setComingFromCableFall(false);
            }

            if (writeFragment!=null ){
                this.onRecordOptionSelected(1);
                optionsFragment.uncheckRecording();

            }

            if (waveFragment != null) {
                waveFragment.pauseSurfaceThread();
            }

            if (spectroFragment != null) {
                spectroFragment.pauseSpectroThread();
            }

            if (readFragment!=null){
                readFragment.setInflatedUI(false);
            }

            landingScreenPlaceholder.setVisibility(View.VISIBLE);
            workMode = Constants.MODE_LANDING_SCREEN;
            manager.popBackStack();

        } else {

            if (radarConnectionFragment != null) {
                radarConnectionFragment.pauseConnection();
                radarConnectionFragment.stopConnectionThread();
                manager.beginTransaction().remove(radarConnectionFragment).commit();
            }


            if (readFragment!=null){
                manager.beginTransaction().remove(readFragment).commit();
            }
            super.onBackPressed();

        }
    }






    private void takeScreenshot() {

        if (BuildConfig.DEBUG){
            Log.i(TAG, "takeScreenshot");
        }

        try {
            // image naming and path  to include sd card  appending name you choose for file
//            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "EasyRad");
            directory.mkdirs();
            Calendar now = Calendar.getInstance();
            String filename = now.get(Calendar.YEAR) +"_"+ (now.get(Calendar.MONTH) +1) +"_"+ now.get(Calendar.DAY_OF_MONTH) + "_"+ now.get(Calendar.HOUR_OF_DAY)+now.get(Calendar.MINUTE)+ ".jpeg" ;

            String path = directory.getAbsolutePath() + File.separator + filename ;

            File imageFile = new File(path);
            MediaScannerConnection.scanFile(this, new String[]{path}, null, null); // file to appear in folder


            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap panel = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            Bitmap spectro = spectroFragment.getBitmap();

            Bitmap composite = Bitmap.createBitmap(v1.getWidth(), v1.getHeight(), Bitmap.Config.RGB_565);

            Canvas canvas = new Canvas();
            canvas.setBitmap(composite);
            Rect source;
            Rect destination;
            if (workMode==Constants.MODE_SCAN) {
                source = new Rect(wavePlaceholder.getWidth(), 0, panel.getWidth(), panel.getHeight());
                destination = new Rect(0, 0, composite.getWidth() - wavePlaceholder.getWidth(), composite.getHeight());
            } else {
                source = new Rect(0,0, v1.getWidth(), v1.getHeight());
                destination = new Rect(0,0,v1.getWidth(), v1.getHeight());
            }
            canvas.drawBitmap(panel, source, destination, null);

            source = new Rect(0,0,spectro.getWidth(), spectro.getHeight());
            destination = new Rect(Utilities.dpToPx(this, 125),0,composite.getWidth(), composite.getHeight());
            canvas.drawBitmap(spectro, source, destination,null);


            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            composite.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


            Toast toast = Toast.makeText(this, filename + " saved in " + "/EasyRad", Toast.LENGTH_LONG);
            toast.show();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void changeFragmentSize(final FrameLayout fragmentContainer, int horizontal, int vertical){
        ValueAnimator animator;
        if (Utilities.isLandscape(this)){
            int beginningWidth = fragmentContainer.getWidth();
            animator = ValueAnimator.ofInt(beginningWidth, horizontal);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int val = (Integer) animation.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = fragmentContainer.getLayoutParams();
                    layoutParams.width = val;
                    fragmentContainer.setLayoutParams(layoutParams);
                }
            });
        } else {
            int beginningHeight = fragmentContainer.getHeight();
            animator = ValueAnimator.ofInt(beginningHeight, vertical);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int val = (Integer) animation.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = fragmentContainer.getLayoutParams();
                    layoutParams.height = val;
                    fragmentContainer.setLayoutParams(layoutParams);
                }
            });
        }

        animator.setDuration(250);
        animator.start();


    }

    public short getWorkMode(){
        return workMode;
    }

    private void displayWorkingFragments(short workMode){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "displayWorkingFragments: workmode:  "  + workMode);
        }

        android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();

        //remove landing screen
        landingScreenPlaceholder.setVisibility(View.GONE);
        preferencePlaceholder.setVisibility(View.GONE);

        if (landingScreenFragment!=null) {
            ft.remove(landingScreenFragment);
        }

        //setup wavefragment
        if (workMode==Constants.MODE_SCAN){
            wavePlaceholder.setVisibility(View.VISIBLE);
            if (waveFragment==null){
                waveFragment = WaveFragment.newInstance();
            }
            ft.add(eu.easyrad.erad.R.id.wave_placeholder, waveFragment);
        } else if (workMode==Constants.MODE_READ_2D || workMode == Constants.MODE_READ_3d){
            wavePlaceholder.setVisibility(View.GONE);
        }

        //setup options frag
        if (optionsFragment == null){
            optionsFragment = OptionsFragment.newInstance();
        }
        optionsFragment.setWorkMode(workMode);
        ft.add(eu.easyrad.erad.R.id.options_placeholder, optionsFragment);

        //setup spectrofrag
        spectroFragment = (SpectrogramFragment) manager.findFragmentById(R.id.spectrogram_placeholder);
        if (spectroFragment==null){
            Log.i(TAG, "spectrofrag== null");
            spectroFragment = SpectrogramFragment.newInstance();
        }
        spectroFragment.setWorkMode(workMode);
        ft.add(eu.easyrad.erad.R.id.spectrogram_placeholder, spectroFragment);

        ft.addToBackStack("workingScreen");
        ft.commit();

    }



    public void notifyCalibrateWheelDialog(){
        CalibrateWheelDialog calibrateWheelDialog = (CalibrateWheelDialog) manager.findFragmentByTag("calibrateWheelDialog");
        if (calibrateWheelDialog !=null){
            calibrateWheelDialog.toggleView();
        }
    }

    public void updateTestWheelDialog(){
        TestWheelDialog testWheelDialog = (TestWheelDialog) manager.findFragmentByTag("testWheelDialog");
        if (testWheelDialog!=null){
            testWheelDialog.setProgress(radarConnectionFragment.getTestSteps());
        }
    }

    public void incrementLocationTracker(){
        Log.i(TAG, "incrementLocationTracker");
//        spectroFragment = (SpectrogramFragment)manager.findFragmentById(R.id.spectrogram_placeholder);
        if (spectroFragment!=null){
            spectroFragment.incrementLocationProgress();
        }
    }

    public void decrementLocationTracker(){
        Log.i(TAG, "decrementLocationTracker");
        if (spectroFragment!=null){
            spectroFragment.decrementLocationProgress();
        }
    }


    /*Listens to landingScreenFragment option selection*/
    @Override
    public void onOptionSelected(int id) {
        switch (id) {
            case eu.easyrad.erad.R.id.connect_to_radar_button:
                FragmentTransaction transaction = manager.beginTransaction();
                radarConnectionFragment = (RadarConnectionFragment) manager.findFragmentByTag("radarConnectionFrag");

                if (radarConnectionFragment == null) {

                    radarConnectionFragment = RadarConnectionFragment.newInstance();
                    transaction.add(radarConnectionFragment, "radarConnectionFrag");
                    transaction.commit();

                } else {
                    radarConnectionFragment.checkUsbDevice();
                }

                break;

            case eu.easyrad.erad.R.id.open_file_button:

                if (radarConnectionFragment != null) {
                    manager.beginTransaction().remove(radarConnectionFragment).commit();
                    radarConnectionFragment = null;
                }

                if (writeFragment != null){
                    manager.beginTransaction().remove(writeFragment).commit();
                    writeFragment = null;
                }

                ChooseFileDialog chooseFileDialog = ChooseFileDialog.newInstance();
                chooseFileDialog.show(manager, "fileChooser");

                break;

            case eu.easyrad.erad.R.id.settings_button:

                settingsFragment = (SettingsFragment)manager.findFragmentById(eu.easyrad.erad.R.id.preference_placeholder);
                landingScreenPlaceholder.setVisibility(View.GONE);
                preferencePlaceholder.setVisibility(View.VISIBLE);
                if (settingsFragment==null){
                    settingsFragment = SettingsFragment.newInstance();
                }
                workMode = Constants.MODE_SETTINGS;
                manager
                        .beginTransaction()
                        .remove(landingScreenFragment)
                        .add(eu.easyrad.erad.R.id.preference_placeholder, settingsFragment)
                        .addToBackStack("workingScreen")
                        .commit();

        }
    }





    /*Listens to toggle selection in OptionsFragment and their respective states*/
    @Override
    public void onOptionSelected(int id, boolean isChecked) {

        String radar = preferenceManager.getRadar();

        switch (id) {
            //on wave toggle state change
            case eu.easyrad.erad.R.id.wave_toggle_btn:
                if (isChecked) {
                    changeFragmentSize(wavePlaceholder, Utilities.dpToPx(this, 90),Utilities.dpToPx(this, 90));
                    waveFragment.fadeOutControls();

                } else {
                    changeFragmentSize(wavePlaceholder, Utilities.dpToPx(this, 180), Utilities.dpToPx(this, 180));
                    waveFragment.fadeInControls();

                }
                break;

            //on connection toggle state change
            case eu.easyrad.erad.R.id.connection_toggle_btn:
                if (isChecked) {
                    if (radarConnectionFragment != null) {

                        waveFragment.startWave();
                        spectroFragment.startSpectroSurface();
                        radarConnectionFragment.resumeConnection();

                    }

                } else {
                    if (writeFragment!=null){
                        this.onRecordOptionSelected(1);
                        optionsFragment.uncheckRecording();
                        optionsFragment.enableOptions();
                    }
                    if (radarConnectionFragment != null) {
                        radarConnectionFragment.pauseConnection();
                        spectroFragment.pauseSpectroThread();
                        waveFragment.pauseSurfaceThread();
                    }
                }
                break;

            //on time window toggle state change
            case eu.easyrad.erad.R.id.time_window_btn:
                if (isChecked) {


                    if (radarConnectionFragment != null) {
                        radarConnectionFragment.changeTimeWindow(1);
                    }
                    if (spectroFragment != null) {
                        switch (radar) {
                            case "100":
                                spectroFragment.setTimeWindow(150);
                                break;
                            case "500":
                                spectroFragment.setTimeWindow(100);
                                break;
                            case "1500":
                                spectroFragment.setTimeWindow(15);
                                break;
                            case "300":
                                spectroFragment.setTimeWindow(150);
                                break;

                        }
                    }
                } else {

                    if (radarConnectionFragment != null) {
                        radarConnectionFragment.changeTimeWindow(7);
                    }
                    if (spectroFragment != null) {
                        switch (radar) {
                            case "100":
                                spectroFragment.setTimeWindow(75);
                                break;
                            case "500":
                                spectroFragment.setTimeWindow(50);
                                break;
                            case "1500":
                                spectroFragment.setTimeWindow((float) 7.5);
                                break;
                            case "300":
                                spectroFragment.setTimeWindow(75);
                                break;
                        }
                    }
                }
                break;

            //on fast fourier transformation filter toggle state change
            case eu.easyrad.erad.R.id.fft_btn:
                if (workMode==Constants.MODE_SCAN) {
                    if (radarConnectionFragment != null) {
                        radarConnectionFragment.applyFFT(isChecked);
                    }

                } else if (workMode==Constants.MODE_READ_2D){
                    if (readFragment!=null){
                        spectroFragment.resetCanvas();
                        readFragment.setWithFFT(isChecked);

                        readFragment.setInflatedUI(spectroFragment.spectroSurfaceIsReady());


                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int availableWidth;
                        if (Utilities.isLandscape(this)) {
                            availableWidth = displayMetrics.widthPixels - (Utilities.dpToPx(this, 90) + Utilities.dpToPx(this, 40));
                        } else {
                            availableWidth = displayMetrics.widthPixels - Utilities.dpToPx(this, 40);
                        }
                        readFragment.getFileToTrace(availableWidth / Utilities.dpToPx(this, 2));

                    }
                }

                break;

            case eu.easyrad.erad.R.id.spectro_update_toggle:

                if (spectroFragment != null) {
                    spectroFragment.setUpdatesWithWheel(isChecked);
                }
                if (radarConnectionFragment!=null){
                    radarConnectionFragment.setRecordingWithWheel(isChecked);
                }

                break;

        }
    }

    /*Listens to how much the wave is manipulated in a certain region*/
    @Override
    public void onFilterApplied(float state, int partition) {

        if (radarConnectionFragment != null) {
            radarConnectionFragment.applySignalFilter(state, partition);
        }
    }

    /*Listens to which radar power level is selected*/
    @Override
    public void onRemoteFilterApplied(int filter) {

        if (radarConnectionFragment != null) {
            radarConnectionFragment.changePowerLevel(filter);
        }
    }

    /*Listens to which palette is selected in OptionsFrag*/
    @Override
    public void onPaletteSelected(int id) {
        if (workMode==Constants.MODE_READ_2D){
            spectroFragment.resetCanvas();
            readFragment = (ReadFragment)manager.findFragmentByTag("readFragment");
            readFragment.setInflatedUI(spectroFragment.spectroSurfaceIsReady());

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int availableWidth;
            if (Utilities.isLandscape(this)) {
                availableWidth = displayMetrics.widthPixels - (Utilities.dpToPx(this, 90) + Utilities.dpToPx(this, 40));
            } else {
                availableWidth = displayMetrics.widthPixels - Utilities.dpToPx(this, 40);
            }
            readFragment.getFileToTrace(availableWidth/Utilities.dpToPx(this, 2));

        }
        switch (id) {
            case eu.easyrad.erad.R.id.mono:
                spectroFragment.changePalette(1);

                break;
            case eu.easyrad.erad.R.id.red:
                spectroFragment.changePalette(2);

                break;
            case eu.easyrad.erad.R.id.blue:
                spectroFragment.changePalette(3);

                break;
            case eu.easyrad.erad.R.id.green:
                spectroFragment.changePalette(4);

                break;
            case eu.easyrad.erad.R.id.palette5:
                spectroFragment.changePalette(5);

                break;
            case eu.easyrad.erad.R.id.palette_rainbow:
                spectroFragment.changePalette(6);
                break;
        }

    }

    /*Listens to which recording option is selected in OptionsFrag*/
    @Override
    public void onRecordOptionSelected(int id) {

        switch (id) {
            case eu.easyrad.erad.R.id.snapshot:
                if (radarConnectionFragment!=null){
                    radarConnectionFragment.pauseConnection();
                }
                if (optionsFragment != null) {
                    optionsFragment.connectionToggle.setChecked(false);
                }
                takeScreenshot();
                break;

            case eu.easyrad.erad.R.id.record_2d:

                radarConnectionFragment.pauseConnection();
                RecordDialog recordDialog2d = RecordDialog.newInstance(true);
                recordDialog2d.show(manager, "recordDialog");

                break;


            case eu.easyrad.erad.R.id.record_3d:

                radarConnectionFragment.pauseConnection();
                RecordDialog recordDialog3d = RecordDialog.newInstance(false);
                recordDialog3d.show(manager, "recordDialog");

                break;


            case 1: // Stop recording

                try {
                    writeFragment.stopWritingErad();
                    FragmentTransaction transaction1 = manager.beginTransaction();
                    transaction1.remove(writeFragment);
                    transaction1.commit();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
                break;

            case R.id.export_to_segy:
                Toast toast = Toast.makeText(this, getResources().getString(R.string.export_to_segy_begin), Toast.LENGTH_LONG);
                toast.show();
                readFragment.copyToSegy();
        }
    }

    /*Listens to which medium is selected in OptionsFragment*/
    @Override
    public void onMediumSelected(int id) {
        switch (id) {
            case eu.easyrad.erad.R.id.air:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.AIR);
                }
                break;
            case eu.easyrad.erad.R.id.water:

                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.WATER);
                }
                break;
            case eu.easyrad.erad.R.id.sand_dry:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.SAND_DRY);
                }
                break;
            case eu.easyrad.erad.R.id.sand_wet:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.SAND_WET);
                }
                break;
            case eu.easyrad.erad.R.id.clay_wet:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.CLAY_WET);
                }
                break;
            case eu.easyrad.erad.R.id.granite:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.GRANITE);
                }

                break;
            case eu.easyrad.erad.R.id.basalt:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.BASALT);
                }
                break;
            case eu.easyrad.erad.R.id.concrete:
                if (spectroFragment != null) {
                    spectroFragment.changeMedium(Label.CONCRETE);
                }

        }

    }








    @Override
    public void onRadarStarted(boolean isCalibrating){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onRadarStarted");
        }

        String measurementSystem;
        if (setupFragment!=null){
            measurementSystem = setupFragment.getMeasurementSystem();
        } else {
            measurementSystem = PreferenceManager.getInstance().getMeasurementSystem();
        }
        // true if isCalibrating false if isTesting
        if (isCalibrating) {
            DialogFragment dialogFragment = CalibrateWheelDialog.newInstance(measurementSystem);
            dialogFragment.show(manager, "calibrateWheelDialog");
        } else {

            DialogFragment dialogFragment = TestWheelDialog.newInstance(measurementSystem, PreferenceManager.getInstance().getWheelStepsPerMeter());
            dialogFragment.show(manager, "testWheelDialog");
        }


    }

    @Override
    public void onRadarConnected() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onRadarConnected");
        }

        workMode = Constants.MODE_SCAN;
        displayWorkingFragments(workMode);

    }

    @Override
    public void onRadarDisconnected(){

        if (BuildConfig.DEBUG){
            Log.i(TAG, "onRadarDisconnected");
        }

        if (workMode == Constants.MODE_SCAN){
            if (writeFragment!=null){
                this.onRecordOptionSelected(1);
                optionsFragment.enableOptions();
                optionsFragment.uncheckRecording();
            }

            optionsFragment.connectionToggle.setChecked(false);
            radarConnectionFragment.setComingFromCableFall(true);

        }

        TestWheelDialog testWheelDialog = (TestWheelDialog) manager.findFragmentByTag("testWheelDialog");
        if (testWheelDialog!=null){
            testWheelDialog.dismiss();
        }

        CalibrateWheelDialog calibrateWheelDialog = (CalibrateWheelDialog) manager.findFragmentByTag("calibrateWheelDialog");
        if (calibrateWheelDialog!=null) {
            calibrateWheelDialog.dismiss();
        }


        radarConnectionFragment.stopConnectionThread();
        radarConnectionFragment.checkUsbDevice();
/*
        else {
            radarConnectionFragment.stopConnectionThread();
            radarConnectio nFragment.setIsCalibrating(true);
            radarConnectionFragment.resetWheelCalibration();
            radarConnectionFragment.checkUsbDevice();
        }
*/

    }






    @Override
    public void onSetupComplete() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSetupComplete");
        }

        FragmentTransaction transaction = manager.beginTransaction();
        setupFragment = (SetupFragment) manager.findFragmentById(R.id.setup_placeholder);
        transaction.remove(setupFragment);
        setupPlaceholder.setVisibility(View.GONE);
        landingScreenPlaceholder.setVisibility(View.VISIBLE);
        workMode = Constants.MODE_LANDING_SCREEN;
        if (manager.getBackStackEntryCount() > 0){

            manager.popBackStack();
        } else {
            landingScreenFragment = (LandingScreenFragment) manager.findFragmentById(R.id.landing_screen_placeholder);
            if (landingScreenFragment == null) {
                landingScreenFragment = LandingScreenFragment.newInstance();
                transaction.add(eu.easyrad.erad.R.id.landing_screen_placeholder, landingScreenFragment);

            }
        }
        transaction.commit();
    }





    @Override
    public void prepareForCalibration(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "prepareForCalibration");
        }

        radarConnectionFragment = (RadarConnectionFragment) manager.findFragmentByTag("radarConnectionFrag");
        if (radarConnectionFragment == null) {
            FragmentTransaction transaction = manager.beginTransaction();
            radarConnectionFragment = RadarConnectionFragment.newInstance();
            transaction.add(radarConnectionFragment, "radarConnectionFrag");
            transaction.commit();

        } else {
            radarConnectionFragment.checkUsbDevice();
        }
        radarConnectionFragment.setIsCalibrating(true);
    }

    @Override
    public void onWheelCalibrationRestart() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onWheelCalibrationRestart");
        }
        radarConnectionFragment.resumeConnection();
        radarConnectionFragment.resetWheelCalibration();

    }

    @Override
    public void onWheelCalibrationCancelled(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onWheelCalibrationCancelled");
        }

        if (PreferenceManager.getInstance().getWheelStepsPerMeter()==0) {
            radarConnectionFragment.resetWheelCalibration();
        }
        radarConnectionFragment.setIsCalibrating(false);
        radarConnectionFragment.stopConnectionThread();

        Toast toast = Toast.makeText(this, getResources().getString(R.string.calibration_cancelled_toast), Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onWheelCalibrationEnd() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onWheelCalibrationEnd");
        }
        int steps = radarConnectionFragment.getWheelCalibrationSteps();


        if (steps > 5){

            String system;

            setupFragment = (SetupFragment)manager.findFragmentById(R.id.setup_placeholder);
            if (setupFragment!=null){
                setupFragment.enableTestButton();
                system = setupFragment.getMeasurementSystem();
            } else {
                system = preferenceManager.getMeasurementSystem();
            }
            if (system=="imperial"){
                steps = (int) ((steps/91.4)*100);
            }
            preferenceManager.setWheelStepsPerMeter(steps);
            preferenceManager.setSupportsWheel(true);
        }

        radarConnectionFragment.setIsCalibrating(false);
        radarConnectionFragment.stopConnectionThread();
        String toastText = getResources().getString(R.string.calibration_done_toast) + " "  + steps + " steps.";
        Toast toast = Toast.makeText(this, toastText, Toast.LENGTH_LONG);
        toast.show();
    }





    @Override
    public void prepareForTesting(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "prepareForCalibration");
        }
        radarConnectionFragment = (RadarConnectionFragment)manager.findFragmentByTag("radarConnectionFrag");
        if (radarConnectionFragment == null) {
            FragmentTransaction transaction = manager.beginTransaction();
            radarConnectionFragment = RadarConnectionFragment.newInstance();
            transaction.add(radarConnectionFragment, "radarConnectionFrag");
            transaction.commit();

        } else {
            radarConnectionFragment.checkUsbDevice();
        }
        radarConnectionFragment.setTesting(true);
    }

    @Override
    public void onWheelTestingRestart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onWheelTestingRestart");
        }
        radarConnectionFragment.setTesting(true);
        radarConnectionFragment.resumeConnection();
        radarConnectionFragment.resetTestingSteps();

    }

    @Override
    public void onWheelTestingEnd(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onWheelTestingEnd");
        }
        radarConnectionFragment.stopConnectionThread();
        radarConnectionFragment.setTesting(false);

        radarConnectionFragment.resetTestingSteps();
    }




    @Override
    public void onRecordPositiveClick(boolean is2d, float x, float y, String operator, String location) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onRecordPositiveClick");
        }

        if (is2d){
        radarConnectionFragment.resumeConnection();

        FragmentTransaction transaction = manager.beginTransaction();
        writeFragment = (WriteFragment) manager.findFragmentByTag("writeFragment");
        if (writeFragment == null){
            writeFragment = WriteFragment.newInstance();

        }
        transaction.add(writeFragment, "writeFragment");
        transaction.commit();

        optionsFragment.disableOptions();

        Bundle params = new Bundle();
        params.putString("radar", preferenceManager.getRadar());
        params.putBoolean("is2d", is2d);
        params.putBoolean("timeWindow", optionsFragment.getTimeWindow());
        params.putFloat("x", x);
        params.putFloat("y", y);

        if (optionsFragment.getUpdateMode()) {

            params.putInt("steps", preferenceManager.getWheelStepsPerMeter());

        } else {
            params.putInt("steps", 0);
        }
        params.putInt("medium", optionsFragment.getMedium());
        params.putString("operator", operator);
        params.putString("location", location);

        writeFragment.setParams(params);
        writeFragment.writeErad();
        } else {
            spectroFragment.setStepsPerMeter((short)PreferenceManager.getInstance().getWheelStepsPerMeter());
            spectroFragment.setX(300);
            spectroFragment.setY(300);
            spectroFragment.setRecording3D(true);
            radarConnectionFragment.setRecording3d(true);
        }


    }

    @Override
    public void onRecordNegativeClick() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onRecordNegativeClick");
        }

        RecordDialog recordDialog = (RecordDialog) manager.findFragmentByTag("recordDialog");
        if (recordDialog != null) {
            recordDialog.dismiss();
        }
        optionsFragment.uncheckRecording();


    }

    @Override
    public void recordingFinished(String filename) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "recordingFinished");
        }
        //do the read fragment magic

        readFragment = (ReadFragment) manager.findFragmentByTag("readFragment");
        optionsFragment.enableOptions();

        if (readFragment!=null){
            try {
                readFragment.setJustRecorded(true);
                readFragment.checkFile(new File(filename));
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        } else  {
            FragmentTransaction transaction = manager.beginTransaction();
            readFragment = ReadFragment.newInstance();
            readFragment.setJustRecorded(true);

            transaction.add(readFragment, "readFragment");
            transaction.commit();
            readFragment.setFile(filename);

        }


    }





    @Override
    public void onFileSelected(String path) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onFileSelected");
        }

        FragmentTransaction transaction = manager.beginTransaction();
        readFragment = (ReadFragment) manager.findFragmentByTag("readFragment");

        if (readFragment!=null){
            try {
                readFragment.checkFile(new File(path));
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        } else  {

            readFragment = ReadFragment.newInstance();
            transaction.add(readFragment, "readFragment");
            transaction.commit();
            readFragment.setFile(path);

        }


    }

    @Override
    public void onFileOk(EradFileHeader header) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onFileOK");
        }


        workMode = Constants.MODE_READ_2D;

        if (manager.findFragmentById(eu.easyrad.erad.R.id.spectrogram_placeholder)==null){
        displayWorkingFragments(workMode);
        spectroFragment.setMedium(header.getDielectric_coefficient());
        spectroFragment.setWindow(header.getTimeWindow());

        } else {
            spectroFragment.changeMedium(header.getDielectric_coefficient());
            spectroFragment.setTimeWindow(header.getTimeWindow());
            spectroFragment.resetCanvas();
            readFragment.setInflatedUI(spectroFragment.spectroSurfaceIsReady());
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int availableWidth;
        if (Utilities.isLandscape(this)) {
            availableWidth = displayMetrics.widthPixels - (Utilities.dpToPx(this, 90) + Utilities.dpToPx(this, 40));
        } else {
            availableWidth = displayMetrics.widthPixels - Utilities.dpToPx(this, 40);
        }

        int neededWidth = (int) header.getTraceCount()*Utilities.dpToPx(this, 3);

        spectroFragment.setRecordedWithWheel(header.getWheelCalibrationSteps() != 0);
        if (header.getWheelCalibrationSteps()!=0){
            spectroFragment.setStepsPerMeter(header.getWheelCalibrationSteps());
            readFragment.fetchStepIndices();
        }


        optionsFragment.setHeaderInfo(header);
        readFragment = (ReadFragment) manager.findFragmentByTag("readFragment");
        spectroFragment.setTraceCount((int) header.getTraceCount());

        if (neededWidth>availableWidth){
            readFragment.getFileToTrace(availableWidth/Utilities.dpToPx(this, 2));
        } else {
            readFragment.getWholeFile();
        }

    }

    @Override
    public void onFileNotOk() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onFIleNOtOk");
        }

        Toast toast = Toast.makeText(this, getResources().getString(R.string.file_corrupt), Toast.LENGTH_LONG);
        toast.show();
        ChooseFileDialog chooseFileDialog = ChooseFileDialog.newInstance();
        chooseFileDialog.show(manager, "fileChooser");
    }

    @Override
    public void onStepIndicesReady(ArrayList<Integer> steps){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStepIndicesReady");
        }

        if (spectroFragment!=null){
            spectroFragment.setStepIndices(steps);
            spectroFragment.updateLabels();
        }
    }


    @Override
    public void onNavigateBack(int index) {
        if (readFragment!=null){
            readFragment.fetchTrace(index, false);
        }
    }

    @Override
    public void onNavigateForth(int index) {
        if (readFragment!=null){
            readFragment.fetchTrace(index, true);
        }
    }



}