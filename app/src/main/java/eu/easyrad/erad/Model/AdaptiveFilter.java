package eu.easyrad.erad.Model;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import eu.easyrad.erad.BuildConfig;

/**
 * Created by vanya on 16-2-24.
 */
public class AdaptiveFilter {

    public final static String TAG = AdaptiveFilter.class.getSimpleName();

    int sampleSize = 30;
    List<float[]> samples;
    float[] identity;
    Gradient gradient;

    public AdaptiveFilter(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new AdaptiveFilter");
        }
        samples = new ArrayList<>();
        identity = new float[581];

        float[] x = new float[]{-128, 0, 128};
        float[] y = new float[]{(float)0.9, 1, (float)0.9};
        gradient = new Gradient(x, y);
    }


    public void feed(float[] data){

        if (samples.size() < sampleSize) {
            samples.add(data);
        } else {
            samples.remove(0);
            samples.add(data);
        }


        for (int i = 0; i < identity.length; i++) {
            float[] values = new float[samples.size()];
            for (int j = 0; j < samples.size(); j++) {
                values[j] = samples.get(j)[i];

            }

            identity[i] = calculateMean(values);
        }

    }

    private static float calculateMode(float[] values){
        int modeCount = 0;
        float mode = 0;

        int currCount;

        for (float candidateMode:values){
            currCount = 0;
            for (float element: values){
                if (candidateMode == element){
                    currCount++;
                }
            }

            if (currCount>modeCount){
                modeCount = currCount;
                mode = candidateMode;
            }
        }

        return mode;
    }


    public float[] apply(float[] data){
        float[] result = new float[data.length];

        for (int i=0; i<561; i++){

            if ((identity[i] - data[i])<128 && (identity[i] - data[i])> -128 ){ //test the threshold

                result[i] = (identity[i] - data[i]) + 128;


                if (result[i] > 255){
                    result[i] = 255;
                } else if (result[i] < 0){
                    result[i] = 0;
                }
            } else {

                result[i] = data[i];
            }
        }
        for (int i=561; i<data.length; i++){
            result[i] = data[i];
        }

        return result;

    }

    public float[] applyForRead(float[] data){
        float[] result = new float[data.length];
        result[0] = data[0];
        for (int i=1; i<562; i++) {
            if ((identity[i] - data[i]) < 128 && (identity[i] - data[i]) > -128) { //test the threshold

                result[i] = (identity[i] - data[i]) + 128;


                if (result[i] > 255) {
                    result[i] = 255;
                } else if (result[i] < 0) {
                    result[i] = 0;
                }
            } else {
                result[i] = data[i];
            }
        }
        for (int i = 562; i<data.length; i++){
            result[i] = data[i];
        }
        return result;
    }

    public float[] test(){
        return identity;
    }

    public static float calculateMean(float[] data){
        float sum = 0;

        for (int i=0; i<data.length; i++){
            sum += data[i];
        }

        return (sum/data.length);

    }


}
