package eu.easyrad.erad.Model;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by vanya on 16-1-12.
 */
public class FileNameAdapter extends ArrayAdapter<File> {

    public static final String TAG = FileNameAdapter.class.getSimpleName();

    private final Context context;
    private final ArrayList<File> files;



    public FileNameAdapter(Context context, ArrayList<File> files){
        super(context, eu.easyrad.erad.R.layout.list_files_layout, files);
        this.context = context;
        this.files = files;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(eu.easyrad.erad.R.layout.list_files_layout, parent, false); //TODO
        TextView textView = (TextView)rowView.findViewById(eu.easyrad.erad.R.id.text_file);
        ImageView imageView = (ImageView)rowView.findViewById(eu.easyrad.erad.R.id.icon_file);




        if (files.get(position).isDirectory() && position!=0){//&&
            imageView.setImageResource(eu.easyrad.erad.R.drawable.icon_file_green);
        } else {
            imageView.setImageResource(eu.easyrad.erad.R.drawable.icon_connection_green);
        }

        //no icon at Current directory
        if (position == 0){
            imageView.setVisibility(View.GONE);
            textView.setTypeface(null, Typeface.BOLD);
            rowView.setClickable(false);
        }
        //the second element is <parent if the current directory is not sdcard
        if (position == 1 && !files.get(0).getAbsolutePath().equals(Environment.getExternalStorageDirectory().getAbsolutePath())){
            imageView.setImageResource(eu.easyrad.erad.R.drawable.icon_arrow_back_green);
        }

        textView.setText(files.get(position).getName());

        rowView.setBackground(ContextCompat.getDrawable(context, eu.easyrad.erad.R.drawable.color_selector_listview));
        return rowView;

    }

}
