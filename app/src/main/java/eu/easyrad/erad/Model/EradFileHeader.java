package eu.easyrad.erad.Model;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.View.Label;

/**
 * Created by vanya on 15-12-22.
 */
public class EradFileHeader {

    public static final String TAG = EradFileHeader.class.getSimpleName();

    /*Opening bytes magic number
    * HEX: 00 45 41 53 59 52 41 44
    * ASCII null EASYRAD */
    byte magicNum1;
    byte magicNum2;
    byte magicNum3;
    byte magicNum4;
    byte magicNum5;
    byte magicNum6;
    byte magicNum7;
    byte magicNum8;


    /*Version number
    * 1 - DEC 2015*/
    byte versionNumber;


    /*Recording software version number
    * 1 - DEC 2015 */
    byte softwareVersionNumber;


    /*Radar Type
    * 1 - SCUDO 100
    * 2 - SCUDO 500
    * 3 - SCUDO 1500
    * 4 - DIPOLE 300*/
    short radarType;

    /*YEAR*/
    short year;

    /*MONTH*/
    short month;

    /*DAY*/
    short day;


    /*Dimensionality of data
        * 1 - 2D
        * 2 - 3D*/
    short dimension;


    /*Offset to data*/
    short offset;


    /*Time window*/
    float timeWindow;


    /*X in meters for 3D data
    * related to distance between beginning of single slice and end*/
    float x;

    /*Y in meters for 3D data
    * related to number of consecutive slices in a recording */
    float y;

    /*Number of steps for 1m distance
    * wheel calibration*/
    short wheelCalibrationSteps;


    /*Medium in dielectric coefficient*/
    float dielectric_coefficient;

    /*Operator Name up to 64 chars in UTF-8*/
    String operator;

    /*Location up to 100 chars in UTF-8*/
    String location;

    /*Trace count, EradFileHeader doesn't record it only stores as a value on read*/
    long traceCount;


    static final short OFFSET_LOCATION = 200; //201st byte

    public byte getVersionNumber() {
        return versionNumber;
    }

    public long getTraceCount() {
        return traceCount;
    }

    public byte getSoftwareVersionNumber() {
        return softwareVersionNumber;
    }

    public short getRadarType() {
        return radarType;
    }

    public short getYear() {
        return year;
    }

    public short getMonth() {
        return month;
    }

    public short getDay() {
        return day;
    }

    public short getDimension() {
        return dimension;
    }

    public short getOffset() {
        return offset;
    }

    public float getTimeWindow() {
        return timeWindow;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public short getWheelCalibrationSteps() {
        return wheelCalibrationSteps;
    }

    public float getDielectric_coefficient() {
        return dielectric_coefficient;
    }

    public String getOperator() {
        return operator;
    }

    public String getLocation() {
        return location;
    }

    public void setRadarType(short radarType) {
        this.radarType = radarType;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public void setMonth(short month) {
        this.month = month;
    }

    public void setDay(short day) {
        this.day = day;
    }

    public void setDimension(short dimension) {
        this.dimension = dimension;
    }

    public void setOffset(short offset) {
        this.offset = offset;
    }

    public void setTimeWindow(float timeWindow) {
        this.timeWindow = timeWindow;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setWheelCalibrationSteps(short wheelCalibrationSteps) {
        this.wheelCalibrationSteps = wheelCalibrationSteps;
    }

    public void setDielectric_coefficient(float dielectric_coefficient){
        this.dielectric_coefficient = dielectric_coefficient;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public void setLocation(String location) {
        this.location = location;
    }

    public void setTraceCount(long traceCount){
        this.traceCount = traceCount;
    }



    public EradFileHeader(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new EradFileHeader");
        }
        magicNum1 = 0x00;
        magicNum2 = 0x45;
        magicNum3 = 0x41;
        magicNum4 = 0x53;
        magicNum5 = 0x59;
        magicNum6 = 0x52;
        magicNum7 = 0x41;
        magicNum8 = 0x44;

        versionNumber = 2;
        softwareVersionNumber = 2;
        radarType = 0;

        year = 1;
        month = 1;
        day = 1;
        dimension = 1;
        offset = 204; //204th byte (+2 bytes per string)
        timeWindow = 1;
        x = 0;
        y = 0;
        wheelCalibrationSteps = 0;
        dielectric_coefficient = 1;
        operator = "";
        location = "";

    }

    public void write(DataOutputStream dataOutputStream) throws IOException {
        try {
            dataOutputStream.writeByte(magicNum1);
            dataOutputStream.writeByte(magicNum2);
            dataOutputStream.writeByte(magicNum3);
            dataOutputStream.writeByte(magicNum4);
            dataOutputStream.writeByte(magicNum5);
            dataOutputStream.writeByte(magicNum6);
            dataOutputStream.writeByte(magicNum7);
            dataOutputStream.writeByte(magicNum8);

            dataOutputStream.writeByte(versionNumber);
            dataOutputStream.writeByte(softwareVersionNumber);
            dataOutputStream.writeShort(radarType);
            dataOutputStream.writeShort(year);
            dataOutputStream.writeShort(month);
            dataOutputStream.writeShort(day);
            dataOutputStream.writeShort(dimension);
            dataOutputStream.writeShort(offset);
            dataOutputStream.writeFloat(timeWindow);
            dataOutputStream.writeFloat(x);
            dataOutputStream.writeFloat(y);
            dataOutputStream.writeShort(wheelCalibrationSteps);
            dataOutputStream.writeFloat(dielectric_coefficient);
            dataOutputStream.writeUTF(operator);
            for (int i=0; i<getOperatorRemainingBytes(); i++ ){
                dataOutputStream.writeByte(0);
            }
            dataOutputStream.writeUTF(location);
            for (int i=0; i<getLocationRemainingBytes(); i++){
                dataOutputStream.writeByte(0);
            }

        } finally {
            dataOutputStream.flush();

        }

    }

    public void read(RandomAccessFile raf) throws IOException {
        raf.seek(8);
        versionNumber = raf.readByte();
        softwareVersionNumber = raf.readByte();
        radarType = raf.readShort();
        year = raf.readShort();
        month = raf.readShort();
        day = raf.readShort();
        dimension = raf.readShort();
        offset = raf.readShort();
        timeWindow = raf.readFloat();
        x = raf.readFloat();
        y = raf.readFloat();
        wheelCalibrationSteps = raf.readShort();
        dielectric_coefficient = raf.readFloat();
        operator = raf.readUTF();
        raf.seek(102);
        location = raf.readUTF();

    }

    public String getRadarAsTag(){
        String tag ="";
        if (radarType!=0){
            switch (radarType){
                case 1:
                    tag = "100";
                    break;
                case 2:
                    tag = "500";
                    break;
                case 3:
                    tag = "1500";
                    break;
                case 4:
                    tag = "300";
                    break;
            }

        }
        return tag;
    }

    public String getRadarAsString(){
        String radar = "";
        if (radarType!=0){
            switch (radarType){
                case 1:
                    radar = "Scudo 100";
                    break;
                case 2:
                    radar = "Scudo 500";
                    break;
                case 3:
                    radar = "Scudo 1500";
                    break;
                case 4:
                    radar = "Dipole 300";
                    break;
            }
        }

        return radar;
    }

    public String getMediumAsString(){
        String medium = "" + dielectric_coefficient;
        if (dielectric_coefficient== Label.AIR){
            medium = medium.concat(" / AIR");
        } else if (dielectric_coefficient == Label.BASALT){
            medium = medium.concat(" / BASALT");
        } else if (dielectric_coefficient == Label.CLAY_WET){
            medium = medium.concat(" / WET CLAY");
        } else if (dielectric_coefficient == Label.CONCRETE){
            medium = medium.concat(" / CONCRETE");
        } else if (dielectric_coefficient == Label.GRANITE){
            medium = medium.concat(" / GRANITE");
        } else if (dielectric_coefficient == Label.SAND_DRY){
            medium = medium.concat(" / DRY SAND");
        } else if (dielectric_coefficient == Label.SAND_WET){
            medium = medium.concat(" / WET SAND");
        } else if (dielectric_coefficient == Label.WATER){
            medium = medium.concat(" / WATER");
        }
        return medium;
    }

    public String getMediumLabel(){
        String medium = "";
        if (dielectric_coefficient== Label.AIR){
            medium ="Air";
        } else if (dielectric_coefficient == Label.BASALT){
            medium = "Basalt";
        } else if (dielectric_coefficient == Label.CLAY_WET){
            medium = "Wet Clay";
        } else if (dielectric_coefficient == Label.CONCRETE){
            medium = "Concrete";
        } else if (dielectric_coefficient == Label.GRANITE){
            medium = "Granite";
        } else if (dielectric_coefficient == Label.SAND_DRY){
            medium = "Dry sand";
        } else if (dielectric_coefficient == Label.SAND_WET){
            medium = "Wet sand";
        } else if (dielectric_coefficient == Label.WATER){
            medium = "Water";
        }

        return medium;
    }

    public boolean is2d(){
        if (dimension==1){
            return true;
        } else {
            return false;
        }
    }

    public String getSoftwareVersion(){
        String version="";
        if (softwareVersionNumber==1){
            version = "0.1.0";
        }
        return version;
    }

    public String getFileVersion(){
        String version = "";
        if (versionNumber ==1){
            version = "0.1";
        }
        return version;
    }

    private int getOperatorRemainingBytes(){
        return 60 - operator.length();
    }

    private int getLocationRemainingBytes(){
        return 100 - location.length();
    }

}
