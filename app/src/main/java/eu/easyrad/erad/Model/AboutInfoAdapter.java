package eu.easyrad.erad.Model;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.easyrad.erad.R;

/**
 * Created by vanya on 16-8-25.
 */
public class AboutInfoAdapter extends ArrayAdapter<String> {

    public static final String TAG = AboutInfoAdapter.class.getSimpleName();
    private final Context context;
    private final ArrayList<String> strings;

    public AboutInfoAdapter(Context context, ArrayList<String> strings) {
        super(context, R.layout.list_about_layout, strings);
        this.context = context;
        this.strings = strings;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.list_about_layout, parent, false);
        TextView title = (TextView) convertView.findViewById(R.id.about_title);
        TextView subtitle = (TextView) convertView.findViewById(R.id.about_subtitle);

        switch (position) {
            case 0:
                //App version
                title.setText(strings.get(position));
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    subtitle.setText(packageInfo.versionName);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                //Contact us
                title.setText(strings.get(position));
                subtitle.setText(R.string.contact_email);
                break;
            case 2:
                //User guide
                title.setText(strings.get(position));
                subtitle.setText(R.string.user_guide_website);
                break;
            case 3:
                //Visit website
                title.setText(strings.get(position));
                subtitle.setText(R.string.website);
                break;
        }

        return convertView;
    }

}
