package eu.easyrad.erad.Model;

import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by vanya on 15-11-4.
 */
public class Utilities {

    public static int dpToPx(Context context, int dp){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
    }

    public static boolean isLandscape(Context context){
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static String metricToImperial(float meters){
        String label;
        float feet = meters * 3.28f;
        int inches = (int)(feet - (int)feet)*12;
        label = (int)feet + "\'" + inches + "\"";

        return label;
    }


}
