package eu.easyrad.erad.Model;

/**
 * Constants used in SharedPreferences
 */
public class Constants {

    public static final String TAG = Constants.class.getSimpleName();


    /*
    * Name of the SharedPreferences for this application*/
    public static final String PREFERENCES = "EasyRadPrefs";

    /*
    * SETUP_COMPLETE stores BOOLEAN
    * values: {true, false}
    * disambig: {setup complete, setup not complete} //when not complete icon_arrow_forward_green initFrag again
    * default: false
    * */
    public static final String SETUP_COMPLETE = "setup_complete";

    /*
    * RADAR_TYPE stores STRING
    * values: {100, 300, 500, 1500}
    * disambiguation: {SCUDO 100, DIPOLE 300, SCUDO 500, SCUDO 1500}
    * default: 100
    * */
    public static final String RADAR_TYPE = "radar_type_key";

    /*
    * TIME_WINDOW stores BOOLEAN
    * values: {true, false}
    * disambiguation: {LONG, SHORT}
    * signal is {1,7}
    * default: true
    * */
    public static final String TIME_WINDOW = "time_window";

    /*
    * SOUND stores BOOLEAN
    * values: {true, false}
    * disambiguation: {yes sound, no sound}
    * default: true
    * */
    public static final String SOUND = "sound_key";

    /*
    * IMPERIAl stores string
    * values: {imperial, metric}
    * default: false
    * */
    public static final String IMPERIAL = "measurement_system_key";

    /*
    * COLOR_BLIND stores BOOLEAN
    * values: {true, false}
    * disambiguation: {yes, no}
    * default: false
    * */
    public static final String COLOR_BLIND = "color_blind_key";

    /*
    * PALETTE stores INTEGER
    * values: {R.id.mono, R.id.red, R.id.blue, R.id.green, R.id.palette5}
    * disambiguation: {palette1,2,3,4 or 5}
    * to be updated
    * default: R.id.mono
    * */
    public static final String PALETTE = "palette_key";

    /*
    * MEDIUM stores INTEGER
    * values: {R.id.air, R.id.water, R.id.sand_dry, R.id.sand_wet, R.id.clay_wet, R.id.basalt, R.id.granite, R.id.concrete}
    * disambiguation: {that's pretty obvious}
    * default: R.id.air
    * */
    public static final String MEDIUM = "medium_key";

    /*
    * RADAR_POWER_LEVEL stores INTEGER
    * values: {0,1,2,3,4}
    * disambiguation: {2, 3, 4, 5, 6}
    * signal is respectively: {2,3,4,5,6}
    * default: 0
    * */
    public static final String RADAR_POWER_LEVEL = "analogue_filter_key";

    /*
    * FILTER_AREA_1 stores INTEGER
    * values: {1,2,3,4,5,6,7}
    * disambiguation: {0.3,0.6,1,2,4,8,16}
    * default: 3
    * */
    public static final String FILTER_AREA_1 = "filter_area_1";
    public static final String FILTER_AREA_2 = "filter_area_2";
    public static final String FILTER_AREA_3 = "filter_area_3";
    public static final String FILTER_AREA_4 = "filter_area_4";
    public static final String FILTER_AREA_5 = "filter_area_5";


    public static final String LANGUAGE = "language_key";

    /*
    * WHEEL stores BOOLEAN
    * values: {true false}
    * disambig: {supports wheel, does not support wheel}
    * default value: false*/
    public static final String WHEEL = "wheel_key";

    /*
    * WHEEL_CALIBRATION stores INTEGER
    * values: {any}
    * disambig: {the number ot occurrences of signal *56* in signal that denotes a distance of 0.5m
     * default:0*/
    public static final String WHEEL_CALIBRATION = "wheel_calibration_key";

    /*OPERATOR stores String
    * can be empty
    * default empty*/
    public static final String OPERATOR = "operator_key";

    /*LOCATION stores String
    * can be empty
    * default empty*/
    public static final String LOCATION = "location_key";


    /*RECORD_SEGY stores boolean
    * */
    public static final String RECORD_SEGY = "record_segy_key";

    public static final short MODE_SCAN = 1;

    public static final short MODE_READ_2D = 2;

    public static final short MODE_READ_3d = 3;

    public static final short MODE_LANDING_SCREEN = 4;

    public static final short MODE_SETTINGS = 5;





}
