package eu.easyrad.erad.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 16-3-24.
 */
public class PreferenceManager {

    public static final String TAG = PreferenceManager.class.getSimpleName();

    private static PreferenceManager instance = null;
    private SharedPreferences preferences;


    private PreferenceManager(){}

    private PreferenceManager(Context context){
        preferences = context.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
    }

    public static PreferenceManager getInstance(Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }

        if (instance == null){
            instance = new PreferenceManager(context);

        }
        return instance;
    }

    public static PreferenceManager getInstance(){
        if (instance == null){
            Log.i(TAG, "must figure a way to implement this properly");

        }
        return instance;
    }



    public boolean isSetupComplete(){
        boolean complete = preferences.getBoolean(Constants.SETUP_COMPLETE, false);
        if (BuildConfig.DEBUG){
            Log.i(TAG, "isSetUpComplete " + complete);
        }
        return complete;
    }

    public void setSetupComplete(boolean setupComplete){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.SETUP_COMPLETE, setupComplete);
        editor.apply();
    }



    public String getRadar(){
        return preferences.getString(Constants.RADAR_TYPE, "100");
    }

    public void setRadar(String radar){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.RADAR_TYPE, radar);
        editor.apply();
    }



    public boolean isLongTimeWindow(){
        return preferences.getBoolean(Constants.TIME_WINDOW, false);
    }

    public void setLongTimeWindow(boolean longTimeWindow){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.TIME_WINDOW, longTimeWindow);
        editor.apply();
    }



    public boolean soundEnabled(){
        return preferences.getBoolean(Constants.SOUND, false);
    }



    public int getPalette(){
        return preferences.getInt(Constants.PALETTE, R.id.mono);
    }

    public void setPalette(int paletteID){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.PALETTE, paletteID);
        editor.apply();
    }



    public int getMedium(){
        return preferences.getInt(Constants.MEDIUM, R.id.air);
    }

    public void setMedium(int mediumID){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.MEDIUM, mediumID);
        editor.apply();
    }



    public int getRadarPowerLevel(){
        return preferences.getInt(Constants.RADAR_POWER_LEVEL, 3);
    }

    public void setRadarPowerLevel(int powerLevel){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setRadarPowerLevel: " + powerLevel);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.RADAR_POWER_LEVEL, powerLevel);
        editor.apply();
    }





    public int getWaveFilterArea1(){
        return preferences.getInt(Constants.FILTER_AREA_1, 3);
    }

    public int getWaveFilterArea2(){
        return preferences.getInt(Constants.FILTER_AREA_2, 3);
    }

    public int getWaveFilterArea3(){
        return preferences.getInt(Constants.FILTER_AREA_3, 3);
    }

    public int getWaveFilterArea4(){
        return preferences.getInt(Constants.FILTER_AREA_4, 3);
    }

    public int getWaveFilterArea5(){
        return preferences.getInt(Constants.FILTER_AREA_5, 3);
    }

    public void setWaveFilters(int area1, int area2, int area3, int area4, int area5){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.FILTER_AREA_1, area1);
        editor.putInt(Constants.FILTER_AREA_2, area2);
        editor.putInt(Constants.FILTER_AREA_3, area3);
        editor.putInt(Constants.FILTER_AREA_4, area4);
        editor.putInt(Constants.FILTER_AREA_5, area5);
        editor.apply();
    }





    public boolean supportsWheel(){
        return preferences.getBoolean(Constants.WHEEL, false);
    }

    public void setSupportsWheel(boolean supportsWheel){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.WHEEL, supportsWheel);
        editor.apply();
    }

    public int getWheelStepsPerMeter(){
        return preferences.getInt(Constants.WHEEL_CALIBRATION, 0);
    }

    public void setWheelStepsPerMeter(int stepsPerMeter){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.WHEEL_CALIBRATION, stepsPerMeter);
        editor.apply();
    }

    public String getOperator(){
        return preferences.getString(Constants.OPERATOR, "");
    }

    public String getLocation(){
        return preferences.getString(Constants.LOCATION, "");
    }

    public boolean isImperial(){
        String system = preferences.getString(Constants.IMPERIAL, "metric");
        if (system.equals("metric")){
            return false;
        } else {
            return true;
        }
    }


    public void setMeasurementSystem(String system){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.IMPERIAL, system);
        editor.apply();
    }

    public String getMeasurementSystem(){
        return preferences.getString(Constants.IMPERIAL, "metric");
    }

    public boolean recordSegy(){
        return preferences.getBoolean(Constants.RECORD_SEGY, false);
    }


}
