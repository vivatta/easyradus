package eu.easyrad.erad.Model;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by vanya on 15-12-11.
 */
public class SegyBinaryHeader {


    /*Job identification number.*/
    private int job_id_number;


    /*Line number.  For 3D poststack data, this will typically contain the in line number.*/
    private int line_number;


    /*Reel number*/
    private int reel_number;


    /*Number of data traces per ensemble.  Mandatory for prestack data*/
    private short data_traces_per_ensemble;


    /*Number of auxiliary traces per ensemble.  Mandatory for prestack data*/
    private short auxiliary_traces_per_ensemble;


    /*Sample interval. Microseconds (μs) for time data, Hertz (Hz) for frequency data, meters (m) or feet (ft) for depth data. */
    private short sample_interval;


    /*Sample interval of original field recording*/
    private short original_sample_interval;


    /*Number of samples per data trace.
    * Note: The sample interval and number of samples in the Binary File Header should be
    * for the primary set of seismic data traces in the file
    */
    private short samples_per_data_trace;


    /*Number of samples per data trace for original field recording.*/
    private short original_samples_per_data_trace;


    /*Data sample format code.  Mandatory for all data.
        1 = 4-byte IBM floating-point
        2 = 4-byte, two's complement integer
        3 = 2-byte, two's complement integer
        4 = 4-byte fixed-point with gain (obsolete)
        5 = 4-byte IEEE floating-point
        6 = 8-byte IEEE floating-point
        7 = 3-byte two’s complement integer
        8 = 1-byte, two's complement integer
        9 = 8-byte, two's complement integer
        10 = 4-byte, unsigned integer
        11 = 2-byte, unsigned integer
        12 = 8-byte, unsigned integer
        15 = 3-byte, unsigned integer
        16 = 1-byte, unsigned integer*/
    private short data_sample_format;


    /*Ensemble fold — The expected number of data traces per trace ensemble */
    private short ensemble_fold;


    /*Trace sorting code (i.e. type of ensemble) :
        –1 = Other (should be explained in a user Extended Textual File Header stanza
        0 = Unknown
        1 = As recorded (no sorting)
        2 = CDP ensemble
        3 = Single fold continuous profile
        4 = Horizontally stacked
        5 = Common source point
        6 = Common receiver point
        7 = Common offset point
        8 = Common mid-point
        9 = Common conversion point*/
    private short trace_sorting_code;


    /*Vertical sum code:
        1 = no sum,
        2 = two sum,
        ...,
        N = M–1 sum  (M = 2 to 32,767*/
    private short vertical_sum_code;


    /*Sweep frequency at start (Hz).*/
    private short sweep_frequency_start;


    /*Sweep frequency at end (Hz).*/
    private short sweep_frequency_end;


    /*Sweep length (ms).*/
    private short sweep_length;


    /*Sweep type code:
        1 = linear
        2 = parabolic
        3 = exponential
        4 = other*/
    private short sweet_type_code;


    /*Trace number of sweep channel.*/
    private short trace_number_sweep_channel;


    /*Sweep trace taper length in milliseconds at start if tapered (the taper starts at zero time and is effective for this length).*/
    private short sweep_trace_taper_start;


    /*Sweep trace taper length in milliseconds at end (the ending taper starts at sweep length minus the taper length at end).*/
    private short sweep_trace_taper_end;


    /*Taper type:
        1 = linear
        2 = cos^2
        3 = other*/
    private short taper_type;


    /*Correlated data traces:
        1 = no
        2 = yes*/
    private short correlated_traces;


    /*Binary gain recovered:
        1 = yes
        2 = no*/
    private short binary_gain_recovered;


    /*Amplitude recovery method:
        1 = none
        2 = spherical divergence
        3 = AGC
        4 = other*/
    private short amplitude_recovery_method;


    /*Measurement system: If Location Data stanzas are included in the file, this entry must agree with the Location Data stanza.
        If there is a disagreement, the last Location Data stanza is the controlling authority.
        1 = Meters
        2 = Feet*/
    private short measurement_system;


    /*Impulse signal polarity
        1 = Increase in pressure or upward geophone case movement gives negative number on tape.
        2 = Increase in pressure or upward geophone case movement gives positive number on tape*/
    private short impulse_signal_polarity;


    /*Vibratory polarity code:
    *    Seismic signal lags pilot signal by:
    *    1 = 337.5° to   22.5°
    *    2 =   22.5° to   67.5°
    *    3 =   67.5° to 112.5°
    *    4 = 112.5° to 157.5°
    *    5 = 157.5° to 202.5°
    *    6 = 202.5° to 247.5°
    *    7 = 247.5° to 292.5°
    *   8 = 292.5° to 337.5°*/
    private short vibratory_polarity_code;


    /*Extended number of data traces per ensemble. If nonzero, this overrides the number of data traces per ensemble in bytes 3213–3214*/
    private int extended_data_traces;




    /*Extended number of auxiliary traces per ensemble. If nonzero, this overrides the number of auxiliary traces per ensemble in bytes 3215–3216.*/
    private int extended_auxiliary_traces;


    /*Extended sample interval, IEEE double precision (64-bit). If nonzero, this overrides the sample interval in bytes 3217–3218.*/
    private long extended_sample_interval;


    /*Extended sample interval of original field recording, IEEE double precision (64-bit). If nonzero, this overrides the sample interval of original field recording in
    * bytes 3219–3220.*/
    private long extended_sample_interval_original;


    /*Extended number of samples per data trace.  If nonzero, this overrides the number of samples per data trace in bytes 3221–3222.*/
    private int extended_samples_data_trace;


    /*Extended number of samples per data trace in original recording. If nonzero, this overrides the number of samples per data trace in original recording in bytes 3223–3224.*/
    private int extended_samples_data_trace_original;


    /*Extended ensemble fold. If nonzero, this overrides ensemble fold in bytes 3227–3228.*/
    private int extended_ensemble_fold;


    /*The integer constant 0d16909060(0x01020304).  This is used to allow unambiguous detection of the byte ordering to expect for this SEG Y file.  For example,
    if this field reads as 0d67305985(0x04030201) then the bytes in everyBinary File Header, Trace Header and Trace Data field must be reversed as they are read.
    If it reads 0d33620995(0x02010403) then consecutive pairs of bytes need to be swapped in every Binary File Header, Trace Header and Trace Data field.*/
    private int constant;


    /* at byte 3501
    Major SEGY Format Revision Number. This is an 8-bit unsigned value. Thus for SEGY Revision 2.0, as defined in this document,
    this will be recorded as 0x02.This field is mandatory for all versions of SEGY, although a value of zero indicates “traditional”
    SEGY conforming to the 1975 standard*/
    private byte major_revision_number;


    /* at byte 3502
    Minor SEGY Format Revision Number.  This is an 8-bit unsigned value with a radix point between the first and second bytes.  Thus for SEGY Revision 2.0,
        as defined in this document, this will be recorded as 0x00. This field is mandatory for all versions of SEGY.*/
    private byte minor_revision_number;


    /*Fixed length trace flag.  A value of one indicates that all traces in this SEGY file are guaranteed to have the same sample interval and number of samples,
        as specified in Binary File Header bytes 3217–3218 and 3221–3222.  A value of zero indicates that the length of the traces in the file may vary and the
        number of samples in bytes 115–116 of the Standard SEG Trace Header must be examined to determine the actual length of each trace. This field is
        mandatory for all versions of SEGY, although a value of zero indicates “traditional” SEGY conforming to the 1975 standard*/
    private short fixed_lenght_trace_flag;


    /*Number of 3200-byte, Extended Textual File Header records following the Binary Header.  A value of zero indicates there are no Extended Textual File
        Header records (i.e. this file has no Extended Textual File Header(s)).  A value of -1 indicates that there are a variable number of Extended Textual File
        Header records and the end of the Extended Textual File Header is denoted by an ((SEG: EndText)) stanza in the final record.  A positive value indicates that
        there are exactly that many Extended Textual File Header records.  Note that, although the exact number of Extended Textual File Header records may be a
        useful piece of information, it will not always be known at the time the Binary Header is written and it is not mandatory that a positive value be recorded
        here.  This field is mandatory for all versions of SEGY, although a value of zero indicates “traditional” SEGY conforming to the 1975 standard.*/
    private short number_extended_text_header;


    /*Number of additional 240 byte trace headers, ranging between 0 and 65535. A value of zero indicates there are no additional 240 byte trace headers. */
    private short number_additional_trace_header;


    /*Number of traces in this file or stream.  (64-bit unsigned value)  If zero, all bytes in the file or stream are part of this SEG-Y dataset.
     If positive, any bytes after the last trace are ignored and may be used for padding or auxiliary data.*/
    private long number_traces;

    private byte zeros;

    public static final String TAG = SegyBinaryHeader.class.getSimpleName();


    public SegyBinaryHeader() {
        job_id_number = 1;
        line_number = 1;
        reel_number = 4;
        data_traces_per_ensemble = 1; //
        auxiliary_traces_per_ensemble = 0;
        sample_interval = (int)(15*1.66);
        original_sample_interval = 0;
        samples_per_data_trace = 581;
        original_samples_per_data_trace = 0;
        data_sample_format = 3;//3
        ensemble_fold = 1;
        trace_sorting_code = 0;
        vertical_sum_code = 0;
        sweep_frequency_start = 0;
        sweep_frequency_end = 0;
        sweep_length = 0;
        sweet_type_code = 0;
        trace_number_sweep_channel = 0;
        sweep_trace_taper_start = 0;
        sweep_trace_taper_end = 0;
        taper_type = 0;
        correlated_traces = 0;
        binary_gain_recovered = 0;
        amplitude_recovery_method = 0;
        measurement_system = 1;
        impulse_signal_polarity =0;
        vibratory_polarity_code = 0;
        extended_data_traces = 0;
        extended_auxiliary_traces = 0;
        extended_sample_interval =0;
        extended_sample_interval_original = 0;
        extended_samples_data_trace = 0;
        extended_samples_data_trace_original = 0;
        extended_ensemble_fold = 0;
        constant = 0x01020304;
        major_revision_number = 0x02;
        minor_revision_number = 0x00;
        fixed_lenght_trace_flag = 0;
        number_extended_text_header =0;
        number_additional_trace_header =0;
        number_traces = 0;
        zeros = 0;


    }

    public void setJob_id_number(int job_id_number) {
        this.job_id_number = job_id_number;
    }

    public void setLine_number(int line_number) {
        this.line_number = line_number;
    }

    public void setReel_number(int reel_number) {
        this.reel_number = reel_number;
    }

    public void setSample_interval(short sample_interval) {
        this.sample_interval = sample_interval;
    }

    public void setSamples_per_data_trace(short samples_per_data_trace) {
        this.samples_per_data_trace = samples_per_data_trace;
    }

    public void setData_sample_format(short data_sample_format) {
        this.data_sample_format = data_sample_format;
    }

    public void setEnsemble_fold(short ensemble_fold) {
        this.ensemble_fold = ensemble_fold;
    }

    public void setMeasurement_system(short measurement_system) {
        this.measurement_system = measurement_system;
    }


    public void write(DataOutputStream outputStream) throws IOException {
        try {
            outputStream.writeInt(job_id_number);                       //4
            outputStream.writeInt(line_number);                         //4
            outputStream.writeInt(reel_number);                         //4   3*4
            outputStream.writeShort(data_traces_per_ensemble);          //2
            outputStream.writeShort(auxiliary_traces_per_ensemble);     //2
            outputStream.writeShort(sample_interval);                   //2
            outputStream.writeShort(original_sample_interval);          //2
            outputStream.writeShort(samples_per_data_trace);            //2
            outputStream.writeShort(original_samples_per_data_trace);   //2
            outputStream.writeShort(data_sample_format);                //2
            outputStream.writeShort(ensemble_fold);                     //2
            outputStream.writeShort(trace_sorting_code);                //2
            outputStream.writeShort(vertical_sum_code);                 //2
            outputStream.writeShort(sweep_frequency_start);             //2
            outputStream.writeShort(sweep_frequency_end);               //2
            outputStream.writeShort(sweep_length);                      //2
            outputStream.writeShort(sweet_type_code);                   //2
            outputStream.writeShort(trace_number_sweep_channel);        //2
            outputStream.writeShort(sweep_trace_taper_start);           //2
            outputStream.writeShort(sweep_trace_taper_end);             //2
            outputStream.writeShort(taper_type);                        //2  20*2
            outputStream.writeShort(correlated_traces);                 //2
            outputStream.writeShort(binary_gain_recovered);             //2

            outputStream.flush();

            outputStream.writeShort(amplitude_recovery_method);         //2
            outputStream.writeShort(measurement_system);                //2
            outputStream.writeShort(impulse_signal_polarity);           //2
            outputStream.writeShort(vibratory_polarity_code);           //2    4*2
            outputStream.writeInt(extended_data_traces);                //4    2*4
            outputStream.writeInt(extended_auxiliary_traces);           //4
            outputStream.writeLong(extended_sample_interval);           //8    2*8
            outputStream.writeLong(extended_sample_interval_original);  //8
            outputStream.writeInt(extended_samples_data_trace);         //4
            outputStream.writeInt(extended_samples_data_trace_original); //4
            outputStream.writeInt(extended_ensemble_fold);              //4    4*4
            outputStream.writeInt(constant);                            //4
            //write zeros to unassigned
            for (int i =1; i<=200; i++){
                outputStream.writeByte(zeros);
            }

            outputStream.flush();


            outputStream.writeByte(major_revision_number);              //1
            outputStream.writeByte(minor_revision_number);              //1
            outputStream.writeShort(fixed_lenght_trace_flag);           //2
            outputStream.writeShort(number_extended_text_header);       //2
            outputStream.writeShort(number_additional_trace_header);    //2
            outputStream.writeLong(number_traces);                      //8

            //write zeros to unassigned
            for (int i=1; i<=84; i++){
                outputStream.writeByte(zeros);
            }

        } finally {
            outputStream.flush();
        }

    }




}
