package eu.easyrad.erad.Model;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by vanya on 15-12-11.
 */
public class SegyTrace {

    public static final String TAG = SegyTrace.class.getSimpleName();

    public static final int NUM_OF_BYTES_HEADER = 240;


    /*Trace sequence number within line — Numbers continue to increase if the same line continues across multiple SEGY files.*/
    private int trace_sequence_line;

    /*Trace sequence number within SEGY file —Each file starts with trace sequence one.*/
    private int trace_sequence_segy;

    /*Original field record number*/
    private int field_record;

    /*Trace number within the original field record.If supplying multicable data with identical channel numbers on each cable, either supply the cable ID number in
bytes 145–148 of SEG Trace Header Extension 1 or enter (cable–1)*nchan_per_cable+channel_no     here*/
    private int trace_number_field;

    /*Energy source point number — Used when more than one record occurs at the same effective surface location.  It is recommended that the new entry defined in
    Trace Header bytes 197–202 be used for shotpoint number. */
    private int energy_source_point;

    /*Ensemble number (i.e. CDP, CMP, CRP, etc.*/
    private int ensemble_num;

    /*Trace number within the ensemble — Each ensemble starts with trace number one*/
    private int trace_number_ensemble;

    /*Trace identification code:
        –1 = Other
        0 = Unknown
        1 = Time domain seismic data
        2 = Dead
        3 = Dummy
        4 = Time break
        5 = Uphole
        6 = Sweep
        7 = Timing
        8 = Waterbreak
        9 = Near-field gun signature
        10 = Far-field gun signature
        11 = Seismic pressure sensor
        12 = Multicomponent seismic sensor–Vertical component
        13 = Multicomponent seismic sensor–Cross-line component
        14 = Multicomponent seismic sensor–In-line component
        15 = Rotated multicomponent seismic sensor–Vertical component
        16 = Rotated multicomponent seismic sensor–Transverse component
        17 = Rotated multicomponent seismic sensor–Radial component
        18 = Vibrator reaction mass
        19 = Vibrator baseplate
        20 = Vibrator estimated ground force
        21 = Vibrator reference
        22 = Time-velocity pairs
        23 = Time-depth pairs
        24 = Depth-velocity pairs
        25 = Depth domain seismic data
        26 = Gravity potential
        27 = Electric field–Vertical component
        28 = Electric field–Cross-line component
        29 = Electric field–In-line component
        30 = Rotated electric field–Vertical component
        31 = Rotated electric field–Transverse component
        32 = Rotated electric field–Radial component
        33 = Magnetic field–Vertical component
        34 = Magnetic field–Cross-line component
        35 = Magnetic field–In-line component
        36 = Rotated magnetic field–Vertical component
        37 = Rotated magnetic field–Transverse component
        38 = Rotated magnetic field–Radial component
        39 = Rotational sensor–Pitch
        40 = Rotational sensor–Roll
        41 = Rotational sensor–Yaw
        42 ... 64 = Reserved
        65 ... N = optional use,  (maximum N = 32,767) */
    private short trace_id_code;


    /*Number of vertically summed traces yielding this trace.  (1 is one trace, 2 is two summed traces, etc.)*/
    private short traces_vertically_summed;

    /*Number of horizontally stacked traces yielding this trace.  (1 is one trace, 2 is two stacked traces, etc.)*/
    private short traces_horizontally_summed;

    /*Data use:
        1 = Production
        2 = Test*/
    private short data_use;

    /*Distance from center of the source point to the center of the receiver group (negative if opposite to direction in which line is shot).*/
    private int distance_from_source;

    /*Receiver group elevation (all elevations above the Vertical datum are positive and below are negative).*/
    private int receiver_group_elevation;

    /*Surface elevation at source.*/
    private int surface_elevation;

    /*Source depth below surface (a positive number).*/
    private int source_depth;

    /*Datum elevation at receiver group*/
    private int datum_elevation_receiver;

    /*Datum elevation at source*/
    private int datum_elevation_source;

    /*Water depth at source.*/
    private int water_depth_source;

    /*Water depth at group.*/
    private int water_depth_group;

    /*Scalar to be applied to all elevations and depths specified in Trace Header bytes 41–68 to give the real value.  Scalar = 1, ±10, ±100, ±1000, or ±10,000.
        If positive, scalar is used as a multiplier; if negative, scalar is used as a divisor*/
    private short scalar_to_elevations;


    /*Scalar to be applied to all coordinates specified in Trace Header bytes 73–88 and to bytes Trace Header 181–188 to give the real value.  Scalar = 1, ±10,
    ±100, ±1000, or ±10,000.  If positive, scalar is used as a multiplier; if negative, scalar is used as divisor.*/
    private short scalar_to_coordinates;

    /*The coordinate reference system should be identified through an extended header Location
        Data stanza.If the coordinate units are in seconds of arc,
        decimal degrees or DMS, the X values represent longitude and the Y values latitude.  A positive
        value designates east of Greenwich Meridian or north of the equator and a negative value
        designates south or west. */

    /*Source coordinate –X. */
    private int source_x;

    /*Source coordinate –Y. */
    private int source_y;

    /*Group coordinate –X*/
    private int group_x;

    /*Group coordinate –Y*/
    private int group_y;

    /*Coordinate units:
        1 = Length (meters or feet)
        2 = Seconds ofarc
        3 = Decimal degrees
        4 = Degrees, minutes, seconds (DMS)
        Note: To encode ±DDDMMSS bytes 89–90 equal = ±DDD*10^4+ MM*10^2 + SS with bytes 71–72 set to 1;
        To encode ±DDDMMSS.ss bytes 89–90 equal =±DDD*10^6+ MM*10^4+ SS*10^2+ ss with bytes 71–72 set to–100.*/
    private short coordinate_units;

    /*Weathering velocity.  (ft/s or m/s as specified in Binary File Header bytes 3255–3256)*/
    private short weathering_velocity;

    /*Subweathering velocity. (ft/s or m/s as specified in Binary File Header bytes 3255–3256)*/
    private short subweathering_velocity;

    /*Uphole time at source in milliseconds*/
    private short uphole_time_source;

    /*Uphole time at group in milliseconds*/
    private short uphole_time_group;

    /*Source static correction in milliseconds.*/
    private short source_static_correction;

    /*Group static correction in milliseconds.*/
    private short group_static_correction;

    /*Total static applied in milliseconds.  (Zero if no static has been applied,)*/
    private short total_static;

    /*Lag time A — Time in milliseconds between end of 240-byte trace identification header and time break.  The value is
        positive if time break occurs after the end of header; negative if time break occurs before the end of header.  Time break is
        defined as the initiation pulse that may be recorded on an auxiliary trace or as otherwise specified by the recording
        system.*/
    private short lag_time_a;

    /*Lag Time B — Time in milliseconds between time break and the initiation time of the energy source.  May be positive or negative*/
    private short lag_time_b;

    /*Delay recording time — Time in milliseconds between initiation time of energy source and the time when recording
        of data samples begins.  In SEGY rev 0 this entry was intended for deep - water work if data recording does not start at zero time.  The entry can be negative to accommodate
        negative start times (i.e. data recorded before time zero, presumably as a result of static application to the data trace).
        If a non-zero value (negative or positive) is recorded in this entry, a comment to that effect should appear in the Textual File Header.*/
    private short delay_recording;


    /*Mute time — Start time in milliseconds*/
    private short mute_time_start;

    /*Mute time — End time in milliseconds*/
    private short mute_time_end;

    /*Number of samples in this trace.*/
    private short num_of_samples;

    /*Sample interval for this trace. Microseconds (μs) for time data, Hertz (Hz) for frequency data, meters (m) or feet (ft) for depth data.
    The number of bytes in a trace record must be consistent with the number of samples
    written in the Binary File Header and/or the SEG-defined Trace Header(s).
    This is important for all recording media; but it is particularly crucial for
    the correct processing of SEGY data in disk files. If the fixed length trace flag in bytes
    3503–3504 of the Binary File Header is set, the sample interval and number of samples in every
    trace in the SEGY file is assumed to be the same as the values
    recorded in the Binary File Header and these fields are ignored.  If the fixed length trace
    flag is not set, the sample interval and number of samples may vary from trace to trace.Set this to zero if the value in bytes
    97–100 of the optional SEG Trace Header Extension 1 is nonzero
    !!!x1.66*/
    private short sample_interval;

    /*Gain type of field instruments:
        1 = fixed
        2 = binary
        3 = floating
        point
        4 ... N = optional use*/
    private short gain_type;

    /*Instrument gain constant (dB)*/
    private short gain_constant;

    /*Instrument early or initial gain (dB)*/
    private short early_gain;

    /*Correlated:
        1 = no
        2 = yes*/
    private short correlated;

    /*Sweep frequency at start (Hz)*/
    private short sweep_f_start;

    /*Sweep frequency at end (Hz)*/
    private short sweep_f_end;

    /*sweep length in millisecond*/
    private short sweep_length;

    /*Sweep type:
        1 = linear
        2 = parabolic
        3 = exponential
        4 = other*/
    private short sweep_type;

    /*Sweep trace taper length at start in milliseconds*/
    private short sweep_trace_taper_start;

    /*Sweep trace taper length at end in milliseconds*/
    private short sweep_trace_taper_end;

    /*taper type:
        1 = linear
        2 = cos^2
        3 = other*/
    private short taper_type;

    /*Alias filter frequency (Hz), if used*/
    private short alias_fr;

    /*Alias filter slope(dB/octave)*/
    private short alias_slope;

    /*Notch filter frequency (Hz), if used*/
    private short notch_frequency;

    /*Notch filter slope (dB/octave)*/
    private short notch_slope;

    /*Low-cut frequency (Hz), if used.*/
    private short low_freq;

    /*High-cut frequency (Hz), if used*/
    private short high_freq;

    /*Low-cut slope (dB/octave)*/
    private short low_slope;

    /*High-cut slope (dB/octave)*/
    private short high_slope;


    /*Year data recorded—The 1975 standard was unclear as to whether this should be recorded as a 2-digit or a 4-digit year and both have been used.
        For SEGY revisions beyond rev 0, the year should be recorded as the complete 4-digit Gregorian calendar year, e.g., the year 2001 should be recorded as 2001
        (0x7D1)*/
    private short year;

    /*Day of year (Julian day for GMT and UTC time basis)*/
    private short day;

    /*Hour of day (24 hour clock).*/
    private short hour;

    /*Minute of hour.*/
    private short minute;

    /*Second of minute*/
    private short second;

    /*Time basis code:
        1 = Local
        2 = GMT (Greenwich Mean Time)
        3 = Other, should be explained in a user defined stanza in the Extended Textual File Header
        4 = UTC (Coordinated Universal Time*/
    private short time_code;

    /*Trace weighting factor —Defined as 2^–N volts for the least significant bit.  (N = 0, 1, ..., 32767)*/
    private short weight_factor;

    /*Geophone group number of roll switch position one*/
    private short geophone_roll;

    /*Geophone group number of trace number one within original field record*/
    private short geophone_trace;

    /*Geophone group number of last trace within original field record*/
    private short geophone_last_trace;

    /*Gap size (total number of groups dropped).*/
    private short gap_size;

    /*Over travel associated with taper at beginning or end of line:
        1 = down (or behind)
        2 = up (or ahead)*/
    private short over_travel;

    /*X coordinate of ensemble (CDP) position of this trace (scalar in Trace Header bytes 71–72 applies).
    The coordinate reference system should be identified through an extended header Location Data stanza*/
    private int x_coordinate_ensemble;


    /*Y coordinate of ensemble (CDP) position of this trace (scalar in bytes Trace Header 71–72 applies). The coordinate reference system should be identified
    through an extended header Location Data stanza.*/
    private int y_coordinate_ensemble;

    /*For 3-D poststack data, this field should be used for the in-line number. If one in-line per SEGY file is being recorded, this value should be the same for all
    traces in the file and the same value will be recorded in bytes 3205–3208 of the Binary File Header*/
    private int inline_num;

    /*For 3-D poststack data, this field should be used for the cross-line number.  This will typically be the same value as the ensemble (CDP) number in Trace Header
    bytes 21–24, but this does not have to be the case.*/
    private int crossline_num;

    /*Shotpoint number — This is probably only applicable to 2 -D poststack data.  Note that it is assumed that the shotpoint number refers to the source location
    nearest to the ensemble (CDP) location for a particular trace.  If this is not the case, there should be a comment in the Textual File Header explaining what the
    shotpoint number actually refers to.*/
    private int shortpoint_num;

    /*Scalar to be applied to the shotpoint number in Trace Header bytes 197–200 to give the real value.  If positive, scalar is used as a multiplier; if negative as a
    divisor; if zero the shotpoint number is not scaled (i.e. it is an integer.  A typical value will be –10, allowing shotpoint numbers with one decimal digit to the right
    of the decimal point).*/
    private short shotpoint_scalar;


    /*Trace value measurement unit:
        –1 = Other (should be described in Data Sample Measurement Units Stanza)
        0 = Unknown
        1 = Pascal (Pa)
        2 = Volts (v)
        3 = Millivolts (mV)
        4 = Amperes (A)
        5 = Meters (m)
        6 = Meters per second (m/s)
        7 = Meters per second squared (m/s^2)
        8 = Newton (N)
        9 = Watt (W)*/
    private short trace_unit;


    /*205-210
    Transduction Constant—The multiplicative constant used to convert the Data Trace samples to the Transduction Units (specified in Trace Header bytes 211–212).
      The constant is encoded as a four-byte, two's complement integer (bytes 205–208) which is the mantissa and a two-byte, two's complement integer (bytes 209–210)
      which is the power of ten exponent (i.e. Bytes 205–208 * 10**Bytes 209–210). */
    private int mantissa;
    private short power;


    /*Transduction Units— The unit of measurement of the Data Trace samples after they have been multiplied by the Transduction Constant specified in Trace
        Header bytes 205–210.
        –1 = Other (should be described in Data Sample Measurement Unit stanza,
        0 = Unknown
        1 = Pascal (Pa)
        2 = Volts (v)
        3 = Millivolts (mV)
        4 = Amperes (A)
        5 = Meters (m)
        6 = Meters per second (m/s)
        7 = Meter
        s per second squared (m/s^2)
        8 = Newton (N)
        9 = Watt (W)*/
    private short transduction_units;

    /*Device/Trace Identifier—The unit number or id number of the device associated with the Data Trace
    (i.e. 4368 for vibrator serial number 4368 or 20316 for gun 16 on string 3 on vessel 2).  This field allows traces to be associated across
    trace ensembles independently of the trace number (Trace Header bytes 25–28)*/
    private short deviceid;



    /*Scalar to be applied to times specified in Trace Header bytes 95–114 to give the true time value in milliseconds.  Scalar = 1, +10, +100, +1000, or +10,000.
     If positive, scalar is used as a multiplier; if negative, scalar is used as divisor.  A value of zero is assumed to be a scalar value of 1.*/
    private short scalar;


    /*Source Type/Orientation—Defines the type and the orientation of the energy source.  The terms vertical, cross-line and in-line refer to the three axes of an
        orthogonal coordinate system.  The absolute azimuthal orientation of the coordinate system axes can be defined in the Bin Grid Definition Stanza .
        –1 to –n = Other
        0 = Unknown
        1 = Vibratory-Vertical orientation
        2 = Vibratory-Cross-line orientation
        3 = Vibratory-In-line orientation
        4 = Impulsive-Vertical orientation
        5 = Impulsive-Cross-line orientation
        6 = Impulsive-In-line orientation
        7 = Distributed Impulsive-Vertical orientation
        8 = Distributed Impulsive-Cross-line orientation
        9 =Distributed Impulsive-In-line orientation*/
    private short source_type;


    /*Source Energy Direction with respect to the source orientation —The positive orientation direction is defined in Bytes 217–218 of the Trace Header.
    The energy direction is encoded in tenths of degrees (i.e. 347.8º is encoded as 3478). Three 2-byte angles*/
    private short angle1;
    private short angle2;
    private short angle3;

    /*Source Measurement —Describes the source effort used to generate the trace.  The measurement can be simple, qualitative measurements such as the total
weight of explosive used or the peak air gun pressure or the number of vibrators times the sweep duration.  Although these simple measurements are acceptable,
it is preferable to use true measurement units of energy or work. The constant is encoded as a four-byte, two's complement integer (bytes 225–228)
 which is the mantissa and a two-byte, two's complement integer (bytes 209–230) which is the power of ten exponent (i.e. Bytes 225–228 * 10**Bytes 229–230).*/
    private int first_part;
    private short second_part;


    /*Source Measurement Unit—The unit used for the Source Measurement, Trace header bytes 225–230.
    –1 = Other
    0 = Unknown
    1 = Joule (J)
    2 = Kilowatt (kW)
    3 = Pascal (Pa)
    4 = Bar (Bar)
    4 = Bar-meter (Bar-m)
    5 = Newton (N)
    6 = Kilograms (kg)*/
    private short source_measurement_unit;



    /*233–240
    Either binary zeros or the eight character trace header name “SEG00000”.  May
    be ASCII or EBCDIC text*/
    private byte zeros;

    private short[] trace_short;

    public SegyTrace(short[] trace){

        trace_sequence_line =1; //to set
        trace_sequence_segy = 1; //to set
        field_record = 1;       //to set = trace_sequence_line
        trace_number_field = 1;

        energy_source_point = 0;

        ensemble_num = 1; //to set = trace_sequence_line
        trace_number_ensemble = 1;

        trace_id_code = 9; //9

        traces_vertically_summed = 1;
        traces_horizontally_summed = 1;
        data_use = 1;

        distance_from_source = 0;
        receiver_group_elevation = 0;
        surface_elevation = 0;
        source_depth = 0;
        datum_elevation_receiver =0;
        datum_elevation_source = 0;
        water_depth_source = 0;
        water_depth_group =0;

        scalar_to_elevations = 1;
        scalar_to_coordinates = -1;

        source_x = 0;
        source_y = 0;

        group_x = 0; //to set

        group_y = 0;

        coordinate_units = 1;

        weathering_velocity = 0;
        subweathering_velocity =0;
        uphole_time_source = 0;
        uphole_time_group = 0;
        source_static_correction =0;
        group_static_correction = 0;
        total_static = 0;
        lag_time_a = 0;
        lag_time_b =0;
        delay_recording = 0;
        mute_time_start = 0;
        mute_time_end = 0;

        num_of_samples = 581; //to set
        sample_interval = (int)(15*1.66); // to set

        gain_type  =0;
        gain_constant = 0;
        early_gain = 0;
        correlated = 0;
        sweep_f_start = 0;
        sweep_f_end = 0;
        sweep_length =0;
        sweep_type = 0;
        sweep_trace_taper_start = 0;
        sweep_trace_taper_end = 0;
        taper_type = 0;
        alias_fr = 0;
        alias_slope = 0;
        notch_frequency = 0;
        notch_slope = 0;
        low_freq = 0;
        high_freq = 0;
        low_slope = 0;
        high_slope = 0;

        year = 1; //to set
        day = 1; // to set
        hour = 1; // to set
        minute = 1; // to set
        second = 1; // to set
        time_code = 2;

        weight_factor = 0;
        geophone_roll =0;
        geophone_trace =0;
        geophone_last_trace =0;
        gap_size = 0;
        over_travel = 0;
        x_coordinate_ensemble = 0;
        y_coordinate_ensemble = 0;
        inline_num =0;
        crossline_num =0;
        shortpoint_num =0;
        shotpoint_scalar = 0;
        trace_unit = 0;
        mantissa = 0;
        power =0;
        transduction_units = 0;
        deviceid = 0;
        scalar = 0;
        source_type = 0;
        angle1 = 0;
        angle2 = 0;
        angle3 = 0;
        first_part = 0;
        second_part = 0;
        source_measurement_unit = 0;
        zeros = 0;

        this.trace_short = trace;



    }


    public void setTrace_sequence_segy(int trace_sequence_segy) {
        this.trace_sequence_segy = trace_sequence_segy;
    }

    public void setTrace_sequence_line(int trace_sequence_line) {
        this.trace_sequence_line = trace_sequence_line;
    }

    public void setField_record(int field_record) {
        this.field_record = field_record;
    }

    public void setTrace_number_field(int trace_number_field) {
        this.trace_number_field = trace_number_field;
    }

    public void setEnsemble_num(int ensemble_num) {
        this.ensemble_num = ensemble_num;
    }

    public void setTrace_number_ensemble(int trace_number_ensemble) {
        this.trace_number_ensemble = trace_number_ensemble;
    }

    public void setTrace_id_code(short trace_id_code) {
        this.trace_id_code = trace_id_code;
    }

    public void setTraces_vertically_summed(short traces_vertically_summed) {
        this.traces_vertically_summed = traces_vertically_summed;
    }

    public void setTraces_horizontally_summed(short traces_horizontally_summed) {
        this.traces_horizontally_summed = traces_horizontally_summed;
    }

    public void setData_use(short data_use) {
        this.data_use = data_use;
    }

    public void setScalar_to_elevations(short scalar_to_elevations) {
        this.scalar_to_elevations = scalar_to_elevations;
    }

    public void setScalar_to_coordinates(short scalar_to_coordinates) {
        this.scalar_to_coordinates = scalar_to_coordinates;
    }

    public void setCoordinate_units(short coordinate_units) {
        this.coordinate_units = coordinate_units;
    }

    public void setNum_of_samples(short num_of_samples) {
        this.num_of_samples = num_of_samples;
    }

    public void setSample_interval(short sample_interval) {
        this.sample_interval = sample_interval;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public void setDay(short day) {
        this.day = day;
    }

    public void setHour(short hour) {
        this.hour = hour;
    }

    public void setMinute(short minute) {
        this.minute = minute;
    }

    public void setSecond(short second) {
        this.second = second;
    }



    public void write(DataOutputStream outputStream) throws IOException {
        try {
            outputStream.writeInt(trace_sequence_line);         //4
            outputStream.writeInt(trace_sequence_segy);         //4
            outputStream.writeInt(field_record);                //4
            outputStream.writeInt(trace_number_field);          //4     //7*4
            outputStream.writeInt(energy_source_point);         //4
            outputStream.writeInt(ensemble_num);                //4
            outputStream.writeInt(trace_number_ensemble);       //4

            outputStream.writeShort(trace_id_code);             //2
            outputStream.writeShort(traces_vertically_summed);  //2
            outputStream.writeShort(traces_horizontally_summed);//2        //4*2
            outputStream.writeShort(data_use);                  //2

            outputStream.writeInt(distance_from_source);        //4
            outputStream.writeInt(receiver_group_elevation);    //4
            outputStream.writeInt(surface_elevation);           //4
            outputStream.writeInt(source_depth);                //4
            outputStream.writeInt(datum_elevation_receiver);    //4         8*4
            outputStream.writeInt(datum_elevation_source);      //4
            outputStream.writeInt(water_depth_source);          //4
            outputStream.writeInt(water_depth_group);           //4

            outputStream.writeShort(scalar_to_elevations);      //2         2*2
            outputStream.writeShort(scalar_to_coordinates);     //2

            outputStream.writeInt(source_x);                    //4
            outputStream.writeInt(source_y);                    //4
            outputStream.writeInt(group_x);                     //4         4*4
            outputStream.writeInt(group_y);                     //4

            outputStream.writeShort(coordinate_units);          //2         2*2
            outputStream.writeShort(weathering_velocity);       //2

            outputStream.flush();

            outputStream.writeShort(subweathering_velocity);    //2
            outputStream.writeShort(uphole_time_source);        //2
            outputStream.writeShort(uphole_time_group);         //2
            outputStream.writeShort(source_static_correction);  //2
            outputStream.writeShort(group_static_correction);   //2
            outputStream.writeShort(total_static);              //2
            outputStream.writeShort(lag_time_a);                //2
            outputStream.writeShort(lag_time_b);                //2
            outputStream.writeShort(delay_recording);           //2
            outputStream.writeShort(mute_time_start);           //2     18*2
            outputStream.writeShort(mute_time_end);             //2
            outputStream.writeShort(num_of_samples);            //2
            outputStream.writeShort(sample_interval);           //2
            outputStream.writeShort(gain_type);                 //2
            outputStream.writeShort(gain_constant);             //2
            outputStream.writeShort(early_gain);                //2
            outputStream.writeShort(correlated);                //2
            outputStream.writeShort(sweep_f_start);             //2

            outputStream.writeShort(sweep_f_end);               //2
            outputStream.writeShort(sweep_length);              //2
            outputStream.writeShort(sweep_type);                //2
            outputStream.writeShort(sweep_trace_taper_start);   //2
            outputStream.writeShort(sweep_trace_taper_end);     //2
            outputStream.writeShort(taper_type);                //2
            outputStream.writeShort(alias_fr);                  //2
            outputStream.writeShort(alias_slope);               //2
            outputStream.writeShort(notch_frequency);           //2     17*2
            outputStream.writeShort(notch_slope);               //2
            outputStream.writeShort(low_freq);                  //2
            outputStream.writeShort(high_freq);                 //2
            outputStream.writeShort(low_slope);                 //2
            outputStream.writeShort(high_slope);                //2
            outputStream.writeShort(year);                      //2
            outputStream.writeShort(day);                       //2
            outputStream.writeShort(hour);                      //2

            outputStream.writeShort(minute);                    //2
            outputStream.writeShort(second);                    //2
            outputStream.writeShort(time_code);                 //2
            outputStream.writeShort(weight_factor);             //2
            outputStream.writeShort(geophone_roll);             //2     9*2
            outputStream.writeShort(geophone_trace);            //2
            outputStream.writeShort(geophone_last_trace);       //2
            outputStream.writeShort(gap_size);                  //2
            outputStream.writeShort(over_travel);               //2

            outputStream.writeInt(x_coordinate_ensemble);       //4     2*4
            outputStream.writeInt(y_coordinate_ensemble);       //4


            outputStream.flush();

            outputStream.writeInt(inline_num);                  //4
            outputStream.writeInt(crossline_num);               //4     3*4
            outputStream.writeInt(shortpoint_num);              //4

            outputStream.writeShort(shotpoint_scalar);          //2
            outputStream.writeShort(trace_unit);                //2     2*2

            outputStream.writeInt(mantissa);                    //4     1*4

            outputStream.writeShort(power);                     //2
            outputStream.writeShort(transduction_units);        //2
            outputStream.writeShort(deviceid);                  //2
            outputStream.writeShort(scalar);                    //2     8*2
            outputStream.writeShort(source_type);               //2
            outputStream.writeShort(angle1);                    //2
            outputStream.writeShort(angle2);                    //2
            outputStream.writeShort(angle3);                    //2

            outputStream.writeInt(first_part);                  //4     1*4

            outputStream.writeShort(second_part);               //2     2*2
            outputStream.writeShort(source_measurement_unit);   //2

//            empty_bytes = NUM_OF_BYTES_HEADER - outputStream.size();

            for (int i =1; i<=8; i++){
                outputStream.writeByte(zeros);
            }
            outputStream.flush();

            for (int i=0; i<trace_short.length; i++){
                outputStream.writeShort(trace_short[i]);
            }
        }
        finally {
            outputStream.flush();
        }

    }

}
