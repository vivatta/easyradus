package eu.easyrad.erad.Model;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by vanya on 15-12-22.
 */
public class EradTraceHeader {

    public static final String TAG = EradTraceHeader.class.getSimpleName();



    /*Trace index in file*/
    long index;

    /*Number of samples in trace*/
    short samples;

    /*Hour*/
    short hour;

    /*Minute*/
    short minute;

    /*Second*/
    short second;

    /*Millisecond*/
    int millisecond;

    /*Slice number
    * 0 if not 3d*/
    int sliceNumber;

    /*Trace number in slice
        * 0 if not 3d*/
    int traceNumberSlice;

    public void setTraceNumberSlice(int traceNumberSlice) {
        this.traceNumberSlice = traceNumberSlice;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public void setSamples(short samples) {
        this.samples = samples;
    }

    public void setHour(short hour) {
        this.hour = hour;
    }

    public void setMinute(short minute) {
        this.minute = minute;
    }

    public void setSecond(short second) {
        this.second = second;
    }

    public void setMillisecond(int millisecond) {
        this.millisecond = millisecond;
    }

    public void setSliceNumber(int sliceNumber) {
        this.sliceNumber = sliceNumber;
    }

    public long getIndex() {
        return index;
    }

    public short getSamples() {
        return samples;
    }

    public short getHour() {
        return hour;
    }

    public short getMinute() {
        return minute;
    }

    public short getSecond() {
        return second;
    }

    public int getMillisecond() {
        return millisecond;
    }

    public int getSliceNumber() {
        return sliceNumber;
    }

    public int getTraceNumberSlice() {
        return traceNumberSlice;
    }


    public EradTraceHeader(){
        index = 0;
        samples = 581;
        hour = 0;
        minute = 0;
        second = 0;
        millisecond = 0;
        sliceNumber = 0;
        traceNumberSlice = 0;

    }

    public void write(DataOutputStream dataOutputStream) throws IOException{
        try {
            dataOutputStream.writeLong(index);
            dataOutputStream.writeShort(samples);
            dataOutputStream.writeShort(hour);
            dataOutputStream.writeShort(minute);
            dataOutputStream.writeShort(second);
            dataOutputStream.writeInt(millisecond);
            dataOutputStream.writeInt(sliceNumber);
            dataOutputStream.writeInt(traceNumberSlice);

        } finally {
            dataOutputStream.flush();

        }
    }

    public void read(RandomAccessFile randomAccessFile, long position) throws IOException{

        randomAccessFile.seek(position);

        index = randomAccessFile.readLong();
        samples = randomAccessFile.readShort();
        hour = randomAccessFile.readShort();
        minute = randomAccessFile.readShort();
        second = randomAccessFile.readShort();
        millisecond = randomAccessFile.readInt();
        sliceNumber = randomAccessFile.readInt();
        traceNumberSlice = randomAccessFile.readInt();

    }
}
