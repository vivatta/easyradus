package eu.easyrad.erad.Model;

import android.graphics.Color;

/**
 * Created by vanya on 15-9-30.
 */
public class Gradient {

    public static final String TAG = Gradient.class.getSimpleName();

    private int[] colors;
    private int[] limits;

    private float[] y;
    private float[] x;

    public Gradient(){

    }



    //must supply arrays of equal length
    //limits[0] must be 0; limits[limits.length] must be 255;
    // color values b/n 0, 255
    public Gradient(int[] limits, int[] colors){
        this.limits = limits;
        this.colors = colors;

    }

    public Gradient(float[] x, float[] y){
        this.x = x;
        this.y = y;
    }

    public void setColors(int[] colors){
        this.colors = colors;
    }

    public void setLimits(int[] limits){
        this.limits = limits;
    }

    public int[] getColors(){
        return colors;
    }

    public int[] getLimits(){
        return limits;
    }


    public int getColorForPoint(int point){

        int minX = limits[0];
        int maxX = limits[1];
        int minY = colors[0];
        int maxY = colors[1];


        for (int i=0; i<limits.length; i++){
            if (point > limits[i]){


                minX = limits[i];
                maxX = limits[i+1];


                minY = colors[i];
                maxY = colors[i+1];


            }
        }

        int red = getYValue(Color.red(minY), Color.red(maxY), minX, maxX, point);
        int green = getYValue(Color.green(minY), Color.green(maxY), minX, maxX, point);
        int blue = getYValue(Color.blue(minY), Color.blue(maxY), minX, maxX, point);

        return Color.rgb(red, green, blue);

    }

    public float getYvalue(float point){
        float minX = x[0];
        float maxX = x[1];
        float minY = y[0];
        float maxY = y[1];

        for (int i=0; i<x.length; i++){
            if (point > x[i]){

                minX = x[i];
                maxX = x[i+1];

                minY = y[i];
                maxY = y[i+1];


            }
        }

        return getYValueFloat(minY, maxY, minX, maxX, point);

    }


    private int getYValue(int minY, int maxY, int minX, int maxX, int point){

        float gradient = calculateGradient(minY, maxY, minX, maxX);

        return (int) (minY + (gradient * Math.abs(point - minX)));

    }

    private float getYValueFloat(float minY, float maxY, float minX, float maxX, float point){

        float gradient = calculateGradient(minY, maxY, minX, maxX);

        return minY + (gradient * Math.abs(point - minX));

    }


    public float calculateGradient(int minY, int maxY, int minX, int maxX){
        return (maxY - minY)/Math.abs(maxX - minX);
    }

    public float calculateGradient(float minY, float maxY, float minX, float maxX){
        return (maxY - minY)/Math.abs(maxX - minX);
    }



}
