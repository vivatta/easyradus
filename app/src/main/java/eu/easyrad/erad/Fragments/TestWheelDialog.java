package eu.easyrad.erad.Fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 16-5-31.
 */
public class TestWheelDialog extends android.support.v4.app.DialogFragment {

    public static final String TAG = TestWheelDialog.class.getSimpleName();

    WheelTestingListener listener;
    ProgressBar progressBar;
    Button start;
    Button restart;

    public TestWheelDialog(){}

    public static TestWheelDialog newInstance(String measurementSystem, int steps) {

        Bundle args = new Bundle();
        args.putString("measurementSystem", measurementSystem);
        args.putInt("steps", steps);

        TestWheelDialog fragment = new TestWheelDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            listener = (WheelTestingListener)activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implement listener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreateDialog(savedInstanceState);
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateDialog");
        }

        String measurementSystem = getArguments().getString("measurementSystem");
        int steps = getArguments().getInt("steps");

        String message;
        if (measurementSystem.equals("metric")){
            message = getResources().getString(R.string.test_wheel_instructions_metric);
        } else {
            message = getResources().getString(R.string.test_wheel_instructions_imperial);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        RelativeLayout parent = (RelativeLayout)inflater.inflate(R.layout.test_wheel_dialog, null);

        TextView instructions = (TextView)parent.findViewById(R.id.test_instructions);
        instructions.setText(message);

        progressBar = (ProgressBar)parent.findViewById(R.id.test_progress);
        progressBar.setMax(steps);

        start = (Button)parent.findViewById(R.id.start_test);
        restart = (Button)parent.findViewById(R.id.restart_test);

        start.setOnClickListener(buttonListener);
        restart.setOnClickListener(buttonListener);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.test_wheel_title);
        builder.setView(parent);
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onWheelTestingEnd();
                dialog.dismiss();
            }
        });

/*        builder.setNeutralButton(R.string.recalibrate_wheel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO show the calibration dialog
            }
        });*/

        builder.setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onWheelTestingEnd();
                dialog.dismiss();

            }
        });

        return builder.create();

    }

    Button.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.start_test:
                    restart.setEnabled(true);
                    listener.onWheelTestingRestart();
                    break;

                case R.id.restart_test:
                    progressBar.setProgress(0);
                    listener.onWheelTestingRestart();

                    break;
            }
        }
    };

    public void setProgress(int progress){
        if(BuildConfig.DEBUG){
            Log.i(TAG, "setProgress");
        }
        progressBar.setProgress(progress);
    }

    public interface WheelTestingListener{
        void onWheelTestingRestart();
        void onWheelTestingEnd(); //stop threads and all

    }

}
