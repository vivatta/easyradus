package eu.easyrad.erad.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import eu.easyrad.erad.BuildConfig;

/**
 * Created by vanya on 16-1-8.
 */
public class RecordDialog extends DialogFragment {

    public static final String TAG = RecordDialog.class.getSimpleName();

    RecordDialogInterface dialogInterface;
    public interface RecordDialogInterface{
        void onRecordPositiveClick(boolean is2d, float x, float y, String operator, String location);
        void onRecordNegativeClick();

    }

    public RecordDialog(){}

    public static RecordDialog newInstance(boolean is2d){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        RecordDialog dialog = new RecordDialog();
        Bundle args = new Bundle();
        args.putBoolean("is2d", is2d);
        dialog.setArguments(args);
        return  dialog;
    }

    @Override
    public void onAttach(Activity activity){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(activity);
        try {
            dialogInterface = (RecordDialogInterface) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement listener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        final Boolean is2d = getArguments().getBoolean("is2d");

        AlertDialog.Builder builder  = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(eu.easyrad.erad.R.layout.record_file_dialog, null);
        builder.setView(view);
        final EditText x = (EditText)view.findViewById(eu.easyrad.erad.R.id.edit_x);
        final EditText y = (EditText)view.findViewById(eu.easyrad.erad.R.id.edit_y);
        final EditText operator = (EditText)view.findViewById(eu.easyrad.erad.R.id.edit_operator);
        final EditText location = (EditText)view.findViewById(eu.easyrad.erad.R.id.edit_location);


        builder.setPositiveButton("RECORD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                dialogInterface.onRecordPositiveClick(getArguments().getBoolean("is2d"));
                if (is2d){
                    dialogInterface.onRecordPositiveClick(true,
                            0,
                            0,
                            operator.getText().toString(),
                            location.getText().toString());

                } else {
                    dialogInterface.onRecordPositiveClick(false,
                            Float.parseFloat(x.getText().toString()),
                            Float.parseFloat(y.getText().toString()),
                            operator.getText().toString(),
                            location.getText().toString());
                }


            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogInterface.onRecordNegativeClick();
            }
        });

        LinearLayout xyedit = (LinearLayout)view.findViewById(eu.easyrad.erad.R.id.x_y_container);
        if (getArguments().getBoolean("is2d")){
            xyedit.setVisibility(View.GONE);
        }


        return builder.create();

    }

}
