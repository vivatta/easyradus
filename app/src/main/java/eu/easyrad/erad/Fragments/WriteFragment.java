package eu.easyrad.erad.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.Calendar;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.Threads.WriteEradThread;
import eu.easyrad.erad.Threads.WriteSegyThread;
import eu.easyrad.erad.View.Label;

/**
 * A simple {@link Fragment} subclass.
 */
public class WriteFragment extends android.support.v4.app.Fragment {

    public static final String TAG = WriteFragment.class.getSimpleName();

    private Context context;
    WriteSegyThread writeSegyThread;
    WriteEradThread writeEradThread;
    RecordingFinishedCallback callback;

    public static final short SEGY = 1;
    public static final short ERAD_2D = 2;
    public static final short ERAD_3D = 3;

    private Bundle params;
    String filename;


    public WriteFragment() {
        // Required empty public constructor
    }

    public static WriteFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new WriteFragment();
    }

    public interface RecordingFinishedCallback{
        void recordingFinished(String filename);
    }

    public void setParams(Bundle params){
        this.params = null;
        this.params = params;
    }

    private short getRadar(){
        short radarCode = 0;
        if (params.getString("radar") !=null){
            switch (params.getString("radar")){
                case "100":
                    radarCode =1;
                    break;
                case "500":
                    radarCode = 2;
                    break;
                case "1500":
                    radarCode = 3;
                    break;
                case "300":
                    radarCode = 4;
                    break;
            }
        } else {
            radarCode = 0;
        }
        return radarCode;
    }

    private boolean getIs2d(){
        return params.getBoolean("is2d");
    }

    private float getTimeWindow(){
        float timeWindow = 0;
        switch (getRadar()){
            case 1:
                if (params.getBoolean("timeWindow")){
                    timeWindow = 150;
                } else {
                    timeWindow = 75;
                }
                break;
            case 2:
                if (params.getBoolean("timeWindow")){
                    timeWindow = 100;
                } else {
                    timeWindow = 50;
                }
                break;
            case 3:
                if (params.getBoolean("timeWindow")){
                    timeWindow = 15;
                } else {
                    timeWindow = (float)7.5;
                }
                break;
            case 4:
                if (params.getBoolean("timeWindow")){
                    timeWindow = 150;
                } else  {
                    timeWindow = 75;
                }
                break;
        }
        return timeWindow;
    }

    private float getX(){
        return params.getFloat("x");
    }

    private float getY() {
        return params.getFloat("y");
    }

    private short getSteps(){
        return (short)params.getInt("steps");
    }

    private float getMedium(){
        float medium = 1;
        switch (params.getInt("medium")){
            case eu.easyrad.erad.R.id.air:
                medium = Label.AIR;
                break;
            case eu.easyrad.erad.R.id.water:
                medium = Label.WATER;
                break;
            case eu.easyrad.erad.R.id.sand_dry:
                medium = Label.SAND_DRY;
                break;
            case eu.easyrad.erad.R.id.sand_wet:
                medium = Label.SAND_WET;
                break;
            case eu.easyrad.erad.R.id.clay_wet:
                medium = Label.CLAY_WET;
                break;
            case eu.easyrad.erad.R.id.granite:
                medium = Label.GRANITE;
                break;
            case eu.easyrad.erad.R.id.basalt:
                medium = Label.BASALT;
                break;
            case eu.easyrad.erad.R.id.concrete:
                medium = Label.CONCRETE;
                break;
        }
        return medium;
    }

    private String getOperator(){
        return params.getString("operator");
    }

    private String getLocation(){
        return params.getString("location");
    }

    public String generateFileName(short fileFormat){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "generateFileName");
        }
        Calendar now = Calendar.getInstance();
        filename = "";

        switch (fileFormat){
            case SEGY:
                File directorySEGY = new File(Environment.getExternalStorageDirectory()+File.separator+"EasyRad"+File.separator+"SEG-Y");
                directorySEGY.mkdirs();
                filename = directorySEGY.getAbsolutePath()+File.separator+now.get(Calendar.YEAR)+"_"+(now.get(Calendar.MONTH)+1)+"_"+now.get(Calendar.DAY_OF_MONTH)+"_"+now.get(Calendar.HOUR_OF_DAY)+now.get(Calendar.MINUTE)+".SGY";
                break;

            case ERAD_2D:
                File directory2D = new File(Environment.getExternalStorageDirectory()+File.separator+"EasyRad"+File.separator+"ERAD 2D");
                directory2D.mkdirs();
                filename = directory2D.getAbsolutePath()+File.separator+now.get(Calendar.YEAR)+"_"+(now.get(Calendar.MONTH)+1)+"_"+now.get(Calendar.DAY_OF_MONTH)+"_"+now.get(Calendar.HOUR_OF_DAY)+now.get(Calendar.MINUTE)+".erad";
                break;

            case ERAD_3D:
                File directory3D = new File(Environment.getExternalStorageDirectory()+File.separator+"EasyRad"+File.separator+"ERAD 3D");
                directory3D.mkdirs();
                filename = directory3D.getAbsolutePath()+File.separator+now.get(Calendar.YEAR)+"_"+(now.get(Calendar.MONTH)+1)+"_"+now.get(Calendar.DAY_OF_MONTH)+"_"+now.get(Calendar.HOUR_OF_DAY)+now.get(Calendar.MINUTE)+".erad";
                break;
        }


        return filename;
    }

    public String getFilename(){
        return filename;
    }

    public void writeSegy(){
        writeSegyThread = new WriteSegyThread(generateFileName(SEGY));
        writeSegyThread.setRunning(true);
        writeSegyThread.start();

    }

    public void stopWritingSegy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onFIleNOtOk");
        }
        if (writeSegyThread !=null){
            writeSegyThread.setRunning(false);
            WriteSegyThread.mHandler.getLooper().quit();
            WriteSegyThread.mHandler = null;
        }
        String filename = Environment.getExternalStorageDirectory()+File.separator+"EasyRad"+File.separator+"SEG-Y";
        MediaScannerConnection.scanFile(context, new String[]{filename}, null, null);
    }

    public void writeErad(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "writeErad");
        }
        if (getIs2d()) {
            writeEradThread = new WriteEradThread(generateFileName(ERAD_2D), true);
            writeEradThread.setDimension((short) 1);

        } else {
            writeEradThread = new WriteEradThread(generateFileName(ERAD_3D), false);
            writeEradThread.setDimension((short) 2);
        }
        writeEradThread.setRadar(getRadar());
        writeEradThread.setTimeWindow(getTimeWindow());
        writeEradThread.setX(getX());
        writeEradThread.setY(getY());
        writeEradThread.setSteps(getSteps());
        writeEradThread.setDielectric_coefficient(getMedium());
        writeEradThread.setOperator(getOperator());
        writeEradThread.setLocation(getLocation());
        writeEradThread.setRunning(true);
        writeEradThread.start();
    }



    public void stopWritingErad(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopWritingErad");
        }
        if (writeEradThread!=null){
            writeEradThread.setRunning(false);
            if (WriteEradThread.mHandler!=null) {
                WriteEradThread.mHandler.getLooper().quit();
                WriteEradThread.mHandler = null;
            }
        }
        String filename = Environment.getExternalStorageDirectory()+File.separator+"EasyRad";
        MediaScannerConnection.scanFile(context, new String[]{filename}, null, null);
        boolean finished = false;
        while(!finished){
            Log.i(TAG, "in While");
            finished = writeEradThread.isFinished();
        }
        if (this.filename!=null) {
            callback.recordingFinished(this.filename);
        }
        this.filename = null;
    }

    public boolean isWriting(){
        boolean isWriting = false;
        if (writeEradThread!=null && writeEradThread.isAlive()){
            isWriting = true;
        }
        return isWriting;
    }

    @Override
    public void onAttach(Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(context);
        this.context = context;
        try {
            callback = (RecordingFinishedCallback)context;
        } catch (ClassCastException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        params = new Bundle();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onActivityCreated");
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();

    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();

    }

    @Override
    public void onStop(){

        super.onStop();

    }

    @Override
    public void onDestroyView(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroyView");
        }
        super.onDestroyView();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void onDestroy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();

    }

    @Override
    public void onDetach(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();
    }









}
