package eu.easyrad.erad.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 15-11-20.
 */
public class SetupFragment extends Fragment {

    public static final String TAG = SetupFragment.class.getSimpleName();

    SetUpListener setUpListener;


//    SharedPreferences preferences;
    Context context;

    //UI
    RadioGroup radarChoices_container;
    RadioGroup measSysChoices_container;


    SwitchCompat wheelSupport;
    Button calibrateWheel;
    Button testWheel;
    Button save;

    String radar;
    String measurementSystem;

    public interface SetUpListener {
        void prepareForCalibration();
        void prepareForTesting();
        void onSetupComplete();
    }


    public SetupFragment(){}

    public static SetupFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new SetupFragment();
    }

    @Override
    public void onAttach(Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(context);
        this.context = context;

        try {
            setUpListener = (SetUpListener)context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement setUpCompleteListener");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateView");
        }
        View v;
        v = inflater.inflate(R.layout.fragment_setup, container, false);

        radarChoices_container = (RadioGroup)v.findViewById(R.id.radar_choices);
        radarChoices_container.setOnCheckedChangeListener(radarOptionListener);

        measSysChoices_container = (RadioGroup)v.findViewById(R.id.measurement_system_choices);
        measSysChoices_container.setOnCheckedChangeListener(measSystemOptionListener);


        calibrateWheel = (Button)v.findViewById(R.id.calibrate_button);
        calibrateWheel.setOnClickListener(buttonListener);

        wheelSupport = (SwitchCompat) v.findViewById(R.id.wheel_switch);
        wheelSupport.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    calibrateWheel.setEnabled(true);

                } else {
                    calibrateWheel.setEnabled(false);
                }
            }
        });


        save = (Button)v.findViewById(R.id.save);
        save.setOnClickListener(buttonListener);

        testWheel = (Button)v.findViewById(R.id.test_wheel_button);
        testWheel.setOnClickListener(buttonListener);
        
        if (savedInstanceState!=null){

            radar = savedInstanceState.getString("radar");
            radarChoices_container.clearCheck();
            radarChoices_container.check(savedInstanceState.getInt("selectedRadar"));

            wheelSupport.setChecked(savedInstanceState.getBoolean("wheelSupport"));
        }

        return v;

    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();
    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("radar", radar);
        savedInstanceState.putBoolean("wheelSupport", wheelSupport.isChecked());
        savedInstanceState.putInt("selectedRadar", radarChoices_container.getCheckedRadioButtonId());


    }

    @Override
    public void onDestroy() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();
        context = null;
        setUpListener = null;

    }

    @Override
    public void onDetach() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();
    }



    RadioGroup.OnCheckedChangeListener radarOptionListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

            switch (checkedId){
                case R.id.scudo_100:
                    radar = "100";
                    break;

                case R.id.scudo_500:
                    radar = "500";
                    break;

                case R.id.scudo_1500:
                    radar = "1500";
                    break;

                case R.id.dipole_300:
                    radar = "300";
                    break;
            }

            if (!(measSysChoices_container.getCheckedRadioButtonId()==-1)){
                save.setEnabled(true);
            }
        }
    };

    RadioGroup.OnCheckedChangeListener measSystemOptionListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            switch (checkedId){
                case R.id.metric:
                    measurementSystem = "metric";
                    break;

                case R.id.imperial:
                    measurementSystem = "imperial";
                    break;
            }

            if (!(radarChoices_container.getCheckedRadioButtonId()==-1)){
                save.setEnabled(true);
            }
        }

    };

    Button.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()){

                case R.id.calibrate_button:
                    if (radarChoices_container.getCheckedRadioButtonId()==-1 && measSysChoices_container.getCheckedRadioButtonId()==-1){
                        String text = getResources().getString(R.string.no_radar_measurement_sys_selected);
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();

                    } else if (radarChoices_container.getCheckedRadioButtonId()==-1){
                        String text = getResources().getString(R.string.no_radar_selected);
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();

                    } else if (measSysChoices_container.getCheckedRadioButtonId()==-1){
                        String text = getResources().getString(R.string.no_measurement_sys_selected);
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();

                    } else {
                        //check for radar
                        setUpListener.prepareForCalibration();

                    }

                    break;

                case R.id.test_wheel_button:
                    Log.i(TAG, "test Button");
                    setUpListener.prepareForTesting();
                    break;

                case R.id.save:
                    save();
                    break;
            }
        }
    };


    private void save(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "save");
        }

        PreferenceManager.getInstance().setSetupComplete(true);
        PreferenceManager.getInstance().setSupportsWheel(wheelSupport.isChecked());
        PreferenceManager.getInstance().setRadar(radar);
        PreferenceManager.getInstance().setMeasurementSystem(measurementSystem);

        setUpListener.onSetupComplete();
        Toast.makeText(context, R.string.preferences_saved, Toast.LENGTH_LONG).show();
    }

    public String getMeasurementSystem(){
        return measurementSystem;
    }

    public void enableTestButton(){
        testWheel.setEnabled(true);
    }




}
