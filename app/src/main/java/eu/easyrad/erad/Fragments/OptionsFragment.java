package eu.easyrad.erad.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Model.EradFileHeader;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.Model.Utilities;
import eu.easyrad.erad.R;

/**
 *
 */
public class OptionsFragment extends android.support.v4.app.Fragment implements CompoundButton.OnCheckedChangeListener{

    public static final String TAG = OptionsFragment.class.getSimpleName();



    //Listeners
    private onOptionSelectedListener optionSelectedListener;
    private onMediumSelectedListener mediumSelectedListener;
    private onPaletteSelectedListener paletteSelectedListener;
    private onRecordOptionSelectedListener recordOptionSelectedListener;

    Context context;

    //Parent layout
    private LinearLayout parentLayout;
    private RelativeLayout infoLayout;

    //Options toggles
    public ToggleButton connectionToggle;
    private ToggleButton waveToggle;
    private ToggleButton timeWindowToggle;
    private ToggleButton fftToggle;
    private ToggleButton spectroUpdateModeToggle;


    //Palettes
    private Button palette;
    private RadioGroup paletteGroup;

    //Medium radio buttons
    private Button medium;
    private RadioGroup mediumGroup;

    private Button record;
    private RadioGroup recordGroup;

    private EradFileHeader headerInfo;

    private short workMode;




    //Option selection listener
    public interface onOptionSelectedListener{
        void onOptionSelected(int id, boolean on);
    }

    //Medium selection listener
    public interface onMediumSelectedListener{
        void onMediumSelected(int id);
    }

    //Medium selection listener
    public interface onPaletteSelectedListener{
        void onPaletteSelected(int id);
    }

    //Record type selection listener
    public interface onRecordOptionSelectedListener{
        void onRecordOptionSelected(int id);
    }




    public OptionsFragment() {}

    public static OptionsFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new OptionsFragment();
    }


    @Override
    public void onAttach(Context myActivity) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(myActivity);
        context = myActivity;

        try {
            optionSelectedListener = (onOptionSelectedListener)myActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement onOptionSelectedListener");
        }

        try {
            mediumSelectedListener = (onMediumSelectedListener)myActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement onMediumSelectedListener");
        }

        try {
            paletteSelectedListener = (onPaletteSelectedListener)myActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement onPaletteSelectedListener");
        }

        try {
            recordOptionSelectedListener = (onRecordOptionSelectedListener)myActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement onRecordOptionsSelectedListener");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateView");
        }
        View v = inflater.inflate(R.layout.fragment_options, container, false);
        parentLayout = (LinearLayout)v.findViewById(R.id.buttons_parent);

        waveToggle = (ToggleButton)v.findViewById(R.id.wave_toggle_btn);
        timeWindowToggle = (ToggleButton)v.findViewById(R.id.time_window_btn);
        fftToggle = (ToggleButton)v.findViewById(R.id.fft_btn);
        palette = (Button)v.findViewById(R.id.palette);
        paletteGroup = (RadioGroup)v.findViewById(R.id.palette_radio_group);
        medium = (Button)v.findViewById(R.id.medium);
        Button snapshot = (Button) v.findViewById(R.id.snapshot);
        snapshot.setOnClickListener(groupListener);

        palette.setOnClickListener(groupListener);
        paletteGroup.setOnCheckedChangeListener(radioGroupListener);

        paletteGroup.check(PreferenceManager.getInstance().getPalette());


        fftToggle.setOnCheckedChangeListener(this);

        if (savedInstanceState != null){
            workMode = savedInstanceState.getShort("workMode");
        }


        //if SCAN
        if (isScan()){
            connectionToggle = (ToggleButton)v.findViewById(R.id.connection_toggle_btn);
            connectionToggle.setVisibility(View.VISIBLE);
            View view = v.findViewById(R.id.connection_toggle_helper_view);
            view.setVisibility(View.VISIBLE);
            spectroUpdateModeToggle = (ToggleButton)v.findViewById(R.id.spectro_update_toggle);

            if (waveToggle!=null) {
                waveToggle.setVisibility(View.VISIBLE);
                waveToggle.setOnCheckedChangeListener(this);
            }

            medium.setOnClickListener(groupListener);
            medium.setEnabled(true);
            mediumGroup = (RadioGroup)v.findViewById(R.id.medium_radio_group);
            mediumGroup.setOnCheckedChangeListener(radioGroupListener);

            record = (Button)v.findViewById(R.id.record);
            record.setVisibility(View.VISIBLE);
            record.setOnClickListener(groupListener);
            recordGroup = (RadioGroup)v.findViewById(R.id.record_radio_group);
            recordGroup.setOnCheckedChangeListener(radioGroupListener);
            RadioButton record3d = (RadioButton) v.findViewById(R.id.record_3d);
            record3d.setOnClickListener(groupListener);
//            record3d.setVisibility(View.GONE);

            connectionToggle.setOnCheckedChangeListener(this);
            timeWindowToggle.setOnCheckedChangeListener(this);
            timeWindowToggle.setEnabled(true);

            //Init the elements
            mediumGroup.check(PreferenceManager.getInstance().getMedium());
            setTimeWindowToggle(PreferenceManager.getInstance().getRadar());
            timeWindowToggle.setChecked(PreferenceManager.getInstance().isLongTimeWindow());

            if (PreferenceManager.getInstance().supportsWheel()){
                spectroUpdateModeToggle.setVisibility(View.VISIBLE);
                View vat = v.findViewById(R.id.spectro_update_toggle_helper);
                vat.setVisibility(View.VISIBLE);
                spectroUpdateModeToggle.setOnCheckedChangeListener(this);
                spectroUpdateModeToggle.setChecked(false);
                record3d.setVisibility(View.VISIBLE);
            }

            enableOptions();


        } //if READ
        else {
            if (waveToggle!=null) {
                waveToggle.setVisibility(View.GONE);
            }
            medium.setClickable(false);
            medium.setEnabled(false);
            timeWindowToggle.setClickable(false);
            timeWindowToggle.setEnabled(false);

            infoLayout = (RelativeLayout) v.findViewById(R.id.file_info_group);

            Button openFile = (Button)v.findViewById(R.id.open_file);
            openFile.setVisibility(View.VISIBLE);
            openFile.setOnClickListener(groupListener);
            View openFileHelper = v.findViewById(R.id.open_file_helper_view);
            openFileHelper.setVisibility(View.VISIBLE);

            ToggleButton info = (ToggleButton)v.findViewById(R.id.info_toggle);
            info.setVisibility(View.VISIBLE);
            info.setOnCheckedChangeListener(this);
            View infoHelper = v.findViewById(R.id.info_toggle_helper);
            infoHelper.setVisibility(View.VISIBLE);

            Button exportToSegy = (Button)v.findViewById(R.id.export_to_segy);
            exportToSegy.setVisibility(View.VISIBLE);
            exportToSegy.setOnClickListener(groupListener);



            if (headerInfo!= null){

                timeWindowToggle.setTextOff((int)headerInfo.getTimeWindow()+"ns");
                timeWindowToggle.setChecked(false);
                medium.setText(headerInfo.getMediumLabel());

                TextView textView = (TextView)v.findViewById(R.id.operator_info);
                textView.setText(headerInfo.getOperator());

                textView = (TextView)v.findViewById(R.id.location_info);
                textView.setText(headerInfo.getLocation());

                textView = (TextView)v.findViewById(R.id.date_info);
                textView.setText(headerInfo.getDay() + "/" + headerInfo.getMonth() + "/" + headerInfo.getYear());

                textView = (TextView)v.findViewById(R.id.radar_info);
                textView.setText(headerInfo.getRadarAsString());

                textView = (TextView)v.findViewById(R.id.time_window_info);
                textView.setText("" + headerInfo.getTimeWindow() + "ns");

                textView = (TextView)v.findViewById(R.id.medium_info);
                textView.setText(headerInfo.getMediumAsString());

                textView = (TextView)v.findViewById(R.id.trace_count_info);
                textView.setText("" + headerInfo.getTraceCount());

                String dimension;

                if (!headerInfo.is2d()) {
                    v.findViewById(R.id.x_info_title).setVisibility(View.VISIBLE);
                    textView = (TextView) v.findViewById(R.id.x_info);
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(headerInfo.getX() + "m");


                    v.findViewById(R.id.y_info_title).setVisibility(View.VISIBLE);
                    textView = (TextView) v.findViewById(R.id.y_info);
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(headerInfo.getY() + "m");

                    v.findViewById(R.id.separator_line).setVisibility(View.VISIBLE);

                    dimension = "3D";
                }else {
                    dimension = "2D";
                }
                textView = (TextView)v.findViewById(R.id.file_format_info);
                String fileInfo =
                        dimension + " "
                        + getResources().getString(R.string.erad) + " "
                        + headerInfo.getFileVersion() + " "
                        + getResources().getString(R.string.app_name) + " "
                        + headerInfo.getSoftwareVersion();
                textView.setText(fileInfo);
            }


        }

        return v;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewCreated");
        }
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {

            if (isScan()) {

                paletteGroup.check(savedInstanceState.getInt("selectedPalette"));
                mediumGroup.check(savedInstanceState.getInt("selectedMedium"));
                recordGroup.check(savedInstanceState.getInt("selectedRecord"));
                timeWindowToggle.setChecked(savedInstanceState.getBoolean("timeWindowToggle"));
                if (spectroUpdateModeToggle.getVisibility() == View.VISIBLE) {
                    spectroUpdateModeToggle.setChecked(false);
                }


            }
        }

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewStateRestored");
        }
        super.onViewStateRestored(savedInstanceState);
        if (spectroUpdateModeToggle!=null) {
            if (spectroUpdateModeToggle.getVisibility() == View.VISIBLE) {
                spectroUpdateModeToggle.setChecked(false);
            }
        }

    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();

        if (isScan()) {
            connectionToggle.setChecked(false);

        }
    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();
        if (isScan()) {

            PreferenceManager.getInstance().setPalette(paletteGroup.getCheckedRadioButtonId());
            PreferenceManager.getInstance().setMedium(mediumGroup.getCheckedRadioButtonId());
            PreferenceManager.getInstance().setLongTimeWindow(timeWindowToggle.isChecked());
            connectionToggle.setChecked(false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onFIleNOtOk");
        }
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("selectedPalette", paletteGroup.getCheckedRadioButtonId());
        savedInstanceState.putShort("workMode", workMode);
        if (isScan()) {
            savedInstanceState.putInt("selectedMedium", mediumGroup.getCheckedRadioButtonId());
            savedInstanceState.putInt("selectedRecord", recordGroup.getCheckedRadioButtonId());
            savedInstanceState.putBoolean("timeWindowToggle", timeWindowToggle.isChecked());
/*            if (spectroUpdateModeToggle.getVisibility() == View.VISIBLE) {
                savedInstanceState.putBoolean("updateMode", spectroUpdateModeToggle.isChecked());
            }*/
        }
    }

    @Override
    public void onStop(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStop");
        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroyView");
        }
        super.onDestroyView();

    }

    @Override
    public void onDetach() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();
        optionSelectedListener = null;
        mediumSelectedListener = null;
        paletteSelectedListener = null;
        recordOptionSelectedListener = null;
        parentLayout = null;

        //Options toggles
        connectionToggle = null;
        waveToggle = null;
        timeWindowToggle = null;
        fftToggle = null;


    }

    @Override
    public void onDestroy() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();
        context = null;
    }


    /*
    * checks whether fragment should be inflated in read or scan mode*/
    private boolean isScan(){

        boolean isScan = true;

        switch (workMode) {
            case Constants.MODE_SCAN:
                isScan = true;
                break;
            case Constants.MODE_READ_2D:
                isScan = false;
                break;
            case Constants.MODE_READ_3d:
                isScan = false;
                break;
        }

        if (BuildConfig.DEBUG){
            Log.i(TAG, "isScan: " + isScan);
        }
        return isScan;
    }

    public Boolean isLandscape() {
        return Utilities.isLandscape(context);
    }




    public void setHeaderInfo(EradFileHeader header){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setHeaderInfo");
        }
        headerInfo = header;
        if (timeWindowToggle!=null) {
            timeWindowToggle.setTextOff((int) headerInfo.getTimeWindow() + "ns");
            timeWindowToggle.setChecked(false);
            timeWindowToggle.invalidate();
        }
        if (medium != null) {
            medium.setText(headerInfo.getMediumLabel());
        }
        if (infoLayout!=null) {
            //Operator
            TextView view = (TextView) infoLayout.findViewById(R.id.operator_info);
            view.setText(headerInfo.getOperator());
            //location
            view = (TextView) infoLayout.findViewById(R.id.location_info);
            view.setText(headerInfo.getLocation());
            //Date of record
            view = (TextView) infoLayout.findViewById(R.id.date_info);
            view.setText(headerInfo.getDay() + "/" + header.getMonth() + "/" + header.getYear());
            //radar
            view = (TextView) infoLayout.findViewById(R.id.radar_info);
            view.setText("" + headerInfo.getRadarAsString());
            //timeWindow
            view = (TextView)infoLayout.findViewById(R.id.time_window_info);
            view.setText("" + headerInfo.getTimeWindow() + "ns");
            //dielectric coefficient
            view = (TextView)infoLayout.findViewById(R.id.medium_info);
            view.setText(headerInfo.getMediumAsString());
            //trace count
            view = (TextView)infoLayout.findViewById(R.id.trace_count_info);
            view.setText("" + headerInfo.getTraceCount());
        }
    }

    public void setWorkMode(short workMode){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setWorkMode: " + workMode);
        }
        this.workMode = workMode;
    }

    private void setTimeWindowToggle(String radar_type){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setTImeWindowToggle");
        }
        if (timeWindowToggle!= null){
            switch (radar_type){
                case "100":
                    timeWindowToggle.setTextOff("75ns");
                    timeWindowToggle.setTextOn("150ns");

                    break;
                case "500":
                    timeWindowToggle.setTextOff("50ns");
                    timeWindowToggle.setTextOn("100ns");

                    break;
                case "1500":
                    timeWindowToggle.setTextOff("7.5ns");
                    timeWindowToggle.setTextOn("15ns");

                    break;
                case "300":
                    timeWindowToggle.setTextOff("75ns");
                    timeWindowToggle.setTextOn("150ns");

                    break;
            }
        }
    }



    public int getMedium(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getMedium");
        }
        return mediumGroup.getCheckedRadioButtonId();
    }

    public int getPalette(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getPalette");
        }
        return paletteGroup.getCheckedRadioButtonId();
    }

    public boolean getTimeWindow(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getTimeWindow");
        }
        return timeWindowToggle.isChecked();
    }

    public boolean getUpdateMode(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getUpdateMode");
        }
        boolean isChecked = false;
        if (spectroUpdateModeToggle!=null) {
            isChecked = spectroUpdateModeToggle.isChecked();
        }
        //returns true if scanning with wheel
        return isChecked;
    }

    private String getRadarTag(short radarCode){
        String tag  = "";
        switch (radarCode){
            case 1:
                tag = "100";
                break;
            case 2:
                tag = "500";
                break;
            case 3:
                tag = "1500";
                break;
            case 4:
                tag = "300";
                break;
        }
        return tag;
    }



    public void showGroup(final ViewGroup group) {


        final boolean isHorizontal = isLandscape();

        int expansion_amount =0;
        switch (group.getId()){
            case R.id.palette_radio_group:
                expansion_amount= Utilities.dpToPx(context, 68);
                break;
            case R.id.medium_radio_group:
                if (isHorizontal) {
                    expansion_amount = Utilities.dpToPx(context, 90);
                } else {
                    expansion_amount = Utilities.dpToPx(context, 60);
                }
                break;
            case R.id.record_radio_group:
                expansion_amount = Utilities.dpToPx(context, 68);
                break;
            case R.id.file_info_group:
                if (isHorizontal){
                    expansion_amount = Utilities.dpToPx(context, 125);
                } else {
                    expansion_amount = Utilities.dpToPx(context, 280);
                }
                break;
        }
        final ValueAnimator animator = ValueAnimator.ofInt(0, expansion_amount);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = group.getLayoutParams();
                if (isHorizontal) {
                    layoutParams.width = val;
                } else {
                    layoutParams.height = val;
                }
                group.setLayoutParams(layoutParams);

            }
        });
        animator.setDuration(200);
        animator.start();
    }

    private void hideGroup(final ViewGroup group){


        final boolean isLandscape = isLandscape();

        int expansion_amount =0;
        switch (group.getId()){
            case R.id.palette_radio_group:
                expansion_amount= Utilities.dpToPx(context, 68);
                break;
            case R.id.medium_radio_group:
                if (isLandscape) {
                    expansion_amount = Utilities.dpToPx(context, 90);
                } else {
                    expansion_amount = Utilities.dpToPx(context, 60);
                }
                break;
            case R.id.record_radio_group:
                expansion_amount = Utilities.dpToPx(context, 68);
                break;
            case R.id.file_info_group:
                expansion_amount = Utilities.dpToPx(context, 125);
                break;
        }
        ValueAnimator animator = ValueAnimator.ofInt(expansion_amount, 0);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = group.getLayoutParams();
                if (isLandscape) {
                    layoutParams.width = val;
                } else {
                    layoutParams.height = val;
                }
                group.setLayoutParams(layoutParams);

            }
        });

        animator.setDuration(200);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                group.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();

    }



    public void uncheckRecording(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "uncheckRecording");
        }
        if (isRecording){
            recordGroup.setOnCheckedChangeListener(null);
            recordGroup.clearCheck();
            recordGroup.setOnCheckedChangeListener(radioGroupListener);
            isRecording = false;
            record.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_selector_record));
        }
    }

    public void disableOptions(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "disableOptions");
        }
        if (isRecording){
            timeWindowToggle.setEnabled(false);
            medium.setEnabled(false);
        }
    }

    public void enableOptions(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "enableOptions");
        }

        timeWindowToggle.setEnabled(true);
        medium.setEnabled(true);
    }


    boolean openPalette = false;
    boolean openMedium = false;
    boolean openRecord = false;
    boolean isRecording = false;

    View.OnClickListener groupListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            switch (view.getId()){
                case R.id.palette:
                    if (!openPalette) {
                        paletteGroup.setVisibility(View.VISIBLE);

                        showGroup(paletteGroup);
                        openPalette = true;
                        if (openMedium){
                            hideGroup(mediumGroup);
                            openMedium = false;
                        }
                        if (openRecord) {
                            hideGroup(recordGroup);
                            openRecord = false;
                        }
                        ToggleButton infoBtn = (ToggleButton) parentLayout.findViewById(R.id.info_toggle);
                        if (infoBtn.getVisibility()==View.VISIBLE){
                            infoBtn.setChecked(false);
                        }
                    } else {
                        hideGroup(paletteGroup);
                        openPalette =false;
                    }
                    break;


                case R.id.medium:
                    if (!openMedium) {
                        mediumGroup.setVisibility(View.VISIBLE);
                        showGroup(mediumGroup);
                        openMedium = true;
                        if (openPalette){
                            hideGroup(paletteGroup);
                            openPalette = false;
                        }
                        if (openRecord){
                            hideGroup(recordGroup);
                            openRecord = false;
                        }
                    } else  {
                        hideGroup(mediumGroup);
                        openMedium = false;
                    }
                    break;

                case R.id.snapshot:
                    recordOptionSelectedListener.onRecordOptionSelected(R.id.snapshot);
                    break;

                case R.id.record:
                    if (isRecording){
                        recordOptionSelectedListener.onRecordOptionSelected(1);
                        recordGroup.setOnCheckedChangeListener(null);
                        recordGroup.clearCheck();
                        recordGroup.setOnCheckedChangeListener(radioGroupListener);
                        isRecording = false;
                        record.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_selector_record));


                    } else {
                        if (!openRecord) {
                            recordGroup.setVisibility(View.VISIBLE);
                            showGroup(recordGroup);
                            openRecord = true;
                            if (openPalette) {
                                hideGroup(paletteGroup);
                                openPalette = false;
                            }
                            if (openMedium) {
                                hideGroup(mediumGroup);
                                openMedium = false;
                            }
                        } else {
                            hideGroup(recordGroup);
                            openRecord = false;
                        }
//                        isRecording = true;
                    }

                    break;

                case R.id.open_file:
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    DialogFragment dialogFragment = ChooseFileDialog.newInstance();
                    dialogFragment.show(manager, "chooseFileDialog");
                    break;

                case R.id.export_to_segy:
                    recordOptionSelectedListener.onRecordOptionSelected(view.getId());

                    break;

            }

        }
    };

    RadioGroup.OnCheckedChangeListener radioGroupListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            switch (radioGroup.getId()){
                case R.id.palette_radio_group:

                    paletteSelectedListener.onPaletteSelected(checkedId);
                    switch (checkedId){
                        case R.id.mono:
                            palette.setBackground(ContextCompat.getDrawable(context, R.drawable.palette_mono));
                            break;
                        case R.id.red:
                            palette.setBackground(ContextCompat.getDrawable(context, R.drawable.palette_red));
                            break;
                        case R.id.blue:
                            palette.setBackground(ContextCompat.getDrawable(context, R.drawable.palette_blue));
                            break;
                        case R.id.palette5:
                            palette.setBackground(ContextCompat.getDrawable(context, R.drawable.palette_5));
                            break;
                        case R.id.green:
                            palette.setBackground(ContextCompat.getDrawable(context, R.drawable.palette_green));
                            break;
                        case R.id.palette_rainbow:
                            palette.setBackground(ContextCompat.getDrawable(context, R.drawable.palette_rainbow));
                            break;
                    }

                    openPalette = false;
                    break;

                case R.id.medium_radio_group:
                    mediumSelectedListener.onMediumSelected(checkedId);
                    for (int i=0; i<radioGroup.getChildCount(); i++){
                        RadioButton child = (RadioButton)radioGroup.getChildAt(i);
                        if (child.getId() == checkedId){
                            CharSequence selected = child.getText();
                            medium.setText(selected);
                        }
                    }

                    openMedium = false;
                    break;

                case R.id.record_radio_group:

                    recordOptionSelectedListener.onRecordOptionSelected(checkedId);
                    isRecording = true;
                    switch(checkedId) {
                        case R.id.record_2d:
                            record.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_record_2d_green));
                            break;
                        case R.id.record_3d:
                            record.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_record_3d_green));
                            break;
                    }

                    openRecord = false;
                    break;
            }
            hideGroup(radioGroup);
        }
    };


    //Listens to which toggle is on/off
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        optionSelectedListener.onOptionSelected(buttonView.getId(), isChecked);

        if (buttonView.getId() == R.id.info_toggle){

            if (isChecked){
                infoLayout.setVisibility(View.VISIBLE);
                showGroup(infoLayout);

                if (openPalette){
                    hideGroup(paletteGroup);
                    openPalette = false;
                }
            } else {
                hideGroup(infoLayout);
            }
        }
    }



}
