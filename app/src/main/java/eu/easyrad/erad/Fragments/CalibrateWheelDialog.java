package eu.easyrad.erad.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 16-5-19.
 */
public class CalibrateWheelDialog extends android.support.v4.app.DialogFragment{

    public static final String TAG = CalibrateWheelDialog.class.getSimpleName();

    WheelCalibrationListener calibrationListener;
    ImageView movementNotifier;
    ImageView movementNotifier_shifted;
    Button start;
    Button stop;

    public CalibrateWheelDialog(){}

    public static CalibrateWheelDialog newInstance(String measurementSystem) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        CalibrateWheelDialog calibrateWheelDialog = new CalibrateWheelDialog();
        Bundle args = new Bundle();
        args.putString("system", measurementSystem);
        calibrateWheelDialog.setArguments(args);
        return calibrateWheelDialog;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            calibrationListener = (WheelCalibrationListener)activity;
        } catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "must implement listener");
        }
    }

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateDialog");
        }
        String measuringSystem = getArguments().getString("system");
        String message;
        if (measuringSystem.equals("metric")){
            message = getResources().getString(R.string.calibrate_wheel_instructions_metric);
        } else {
            message = getResources().getString(R.string.calibrate_wheel_instructions_imperial);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        RelativeLayout parent = (RelativeLayout)inflater.inflate(R.layout.calibrate_wheel_dialog, null);
        TextView instructions = (TextView)parent.findViewById(R.id.calibration_instructions);
        movementNotifier = (ImageView)parent.findViewById(R.id.movement_notifier);
        movementNotifier_shifted = (ImageView)parent.findViewById(R.id.movement_notifier_shifted);
        instructions.setText(message);

        start  = (Button)parent.findViewById(R.id.start_calibration);
        stop = (Button)parent.findViewById(R.id.stop_calibration);

        start.setOnClickListener(buttonListener);
        stop.setOnClickListener(buttonListener);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.calibrate_wheel);
        builder.setView(parent);
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                calibrationListener.onWheelCalibrationCancelled();
                dialog.dismiss();
            }
        });

        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                calibrationListener.onWheelCalibrationEnd();
            }
        });

        return builder.create();

    }

    Button.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.start_calibration:
                    calibrationListener.onWheelCalibrationRestart();
                    stop.setEnabled(true);
                    start.setText(getResources().getString(R.string.restart));

                    break;

                case R.id.stop_calibration:
                    calibrationListener.onWheelCalibrationEnd();
                    CalibrateWheelDialog.this.getDialog().cancel();
                    break;
            }
        }
    };



    public void toggleView(){
        if (movementNotifier.getVisibility()==View.VISIBLE){
            movementNotifier.setVisibility(View.GONE);
            movementNotifier_shifted.setVisibility(View.VISIBLE);
        } else if (movementNotifier_shifted.getVisibility()==View.VISIBLE){
            movementNotifier_shifted.setVisibility(View.GONE);
            movementNotifier.setVisibility(View.VISIBLE);
        }


    }

    public interface WheelCalibrationListener{
        void onWheelCalibrationRestart();
        void onWheelCalibrationEnd();
        void onWheelCalibrationCancelled();
    }
}
