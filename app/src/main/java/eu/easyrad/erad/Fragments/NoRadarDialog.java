package eu.easyrad.erad.Fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import eu.easyrad.erad.BuildConfig;

/**
 * Created by vanya on 15-8-28.
 */
public class NoRadarDialog extends DialogFragment {

    public static final String TAG = NoRadarDialog.class.getSimpleName();

    public NoRadarDialog(){
        //empty constructor
    }

    public static NoRadarDialog newInstance(String message){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        NoRadarDialog dialog = new NoRadarDialog();
        Bundle args = new Bundle();
        args.putString("message", message);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState){

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        String message = getArguments().getString("message");
        builder.setMessage(message);
        builder.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //something something
                //retry radarconnectionfrag.getRadar();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                RadarConnectionFragment fragment = (RadarConnectionFragment) manager.findFragmentByTag("radarConnectionFrag");
                fragment.checkUsbDevice();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //close the dialog
                FragmentManager manager = getActivity().getSupportFragmentManager();
                RadarConnectionFragment fragment = (RadarConnectionFragment)manager.findFragmentByTag("radarConnectionFrag");
                fragment.onDestroy();
                NoRadarDialog.this.getDialog().cancel();

            }
        });
        return builder.create();
    }
}
