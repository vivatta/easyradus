package eu.easyrad.erad.Fragments;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.Model.Utilities;
import eu.easyrad.erad.R;
import eu.easyrad.erad.Threads.Erad2DrawThread;
import eu.easyrad.erad.View.Label;
import eu.easyrad.erad.View.SpectroSurfaceView;
import eu.easyrad.erad.View.VerticalProgressBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class SpectrogramFragment extends android.support.v4.app.Fragment {

    public static final String TAG = SpectrogramFragment.class.getSimpleName();

    Label yAxis;
    LinearLayout xAxis;
    SpectroSurfaceView spectroView;
    RelativeLayout locationTracker;
    FrameLayout ssv_parent;

    int lastFetchedForth;
    int lastFetchedBack;
    short workMode;
    float window;
    float medium;
    int palette;
    int brushSize;
    short stepsPerMeter;
    boolean imperial;


    Context context;
    FileNavigationListener fileNavigationListener;

    private int mActivePointerId = MotionEvent.INVALID_POINTER_ID;
    private float mLastTouchX;
    int traceCount;
    boolean recordedWithWheel;

    ArrayList<Integer> stepIndices;



    public interface FileNavigationListener{
        void onNavigateBack(int index);
        void onNavigateForth(int index);
    }



    public SpectrogramFragment() {
        // Required empty public constructor
    }

    public static SpectrogramFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new SpectrogramFragment();
    }

    @Override
    public void onAttach(Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(context);
        try {
            fileNavigationListener = (FileNavigationListener)context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement listener");
        }
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
        imperial = PreferenceManager.getInstance().isImperial();
        if (savedInstanceState!=null){
            workMode = savedInstanceState.getShort("workMode");
            if (workMode == Constants.MODE_READ_2D || workMode == Constants.MODE_READ_3d){
                traceCount = savedInstanceState.getInt("traceCount");
                recordedWithWheel = savedInstanceState.getBoolean("recordedWithWheel");
                stepsPerMeter = savedInstanceState.getShort("stepsPerMeter");
            }
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateVIew");
        }
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_spectrogram, container, false);

        spectroView = (SpectroSurfaceView)v.findViewById(R.id.spectrogram_surface_view);
        locationTracker = (RelativeLayout)v.findViewById(R.id.location_tracker_container);
        xAxis = (LinearLayout)v.findViewById(R.id.xAxis);
        initPalette(PreferenceManager.getInstance().getPalette());

        spectroView.setWorkMode(workMode);
        spectroView.setPxPerDp(Utilities.dpToPx(context, 1));
        spectroView.setImperial(imperial);

        yAxis = (Label)v.findViewById(R.id.yAxis);
        yAxis.setImperial(imperial);

        ssv_parent = (FrameLayout) v.findViewById(R.id.cherga_container);

        if (savedInstanceState!=null){
            medium = savedInstanceState.getFloat("medium");
            window = savedInstanceState.getFloat("timeWindow");
            palette = savedInstanceState.getInt("palette");
            brushSize = savedInstanceState.getInt("brushSize");
            spectroView.setBrushSize(brushSize);


        } else {
            medium = initMedium(PreferenceManager.getInstance().getMedium());
            window = initTimeWin(PreferenceManager.getInstance().getRadar(), PreferenceManager.getInstance().isLongTimeWindow());
            palette = initPalette(PreferenceManager.getInstance().getPalette());
            spectroView.setBrushSize(getBrushSize(PreferenceManager.getInstance().getRadar()));
        }

        changeMedium(medium);
        setTimeWindow(window);
        changePalette(palette);

        if (workMode==Constants.MODE_READ_2D || workMode==Constants.MODE_READ_3d){

            spectroView.setRecordedWithWheel(recordedWithWheel);
            spectroView.setTraceCount(traceCount);
            if (recordedWithWheel){
                xAxis.setVisibility(View.VISIBLE);
            }
            startSpectroSurface();
            spectroView.setOnTouchListener(touchListener);

        }

        return v;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewCreated");
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onActivityCreated");
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewStateRestored");
        }
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();
        if (ssv_parent!=null) {
            ssv_parent.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();

    }

    @Override
    public void onStop(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStop");
        }
        super.onStop();

    }

    @Override
    public void onDestroyView(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroyView");
        }
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putShort("workMode", workMode);
        savedInstanceState.putFloat("medium", medium);
        savedInstanceState.putFloat("timeWindow", window);
        savedInstanceState.putInt("palette", palette);
        savedInstanceState.putInt("brushSize", brushSize);

        if (workMode == Constants.MODE_READ_2D){
            savedInstanceState.putInt("traceCount", traceCount);
            savedInstanceState.putBoolean("recordedWithWheel", recordedWithWheel);
            savedInstanceState.putShort("stepsPerMeter", stepsPerMeter);
        }

    }

    @Override
    public void onDestroy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();
        if (spectroView!=null) {
            spectroView.stopSpectroThread();
        }
        if (ssv_parent!=null) {
            ssv_parent.setVisibility(View.GONE);
        }
        spectroView = null;

    }

    @Override
    public void onDetach(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();

    }




    public void updateLabels(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "updateLabels");
        }

        if (xAxis.getVisibility()==View.VISIBLE){
            int lastDrawn = spectroView.getLastDrawnTrace();
            int firstDrawn = spectroView.getFirstDrawnTrace();

            for (int i=0; i<xAxis.getChildCount(); i+=3){
                View view = xAxis.getChildAt(i);

                int traceIndex = (int)(view.getX()/ Utilities.dpToPx(context, 2)) + firstDrawn;
                if (traceIndex>lastDrawn){
                    return;
                }

                int previous=1; //step index
                int next=2; //step index
                int index =0;
                String distance;

                for (int j=0; j<stepIndices.size()-1; j++){

                    if (stepIndices.get(j)==traceIndex){
                        previous = traceIndex;
                        index = j;
                        break;

                    } else if (stepIndices.get(j)<traceIndex && stepIndices.get(j+1)>traceIndex){
                        previous = stepIndices.get(j);
                        next = stepIndices.get(j + 1);
                        index =j;

                        break;
                    }
                }


                float distanceInCm = ((100 / stepsPerMeter) / (next - previous)) * (traceIndex - previous) + index * (100 / stepsPerMeter);

                if (imperial){
                    distance = Utilities.metricToImperial(distanceInCm/100);
                } else {
                    distance = String.valueOf(distanceInCm / 100);
                }


                TextView textView = (TextView)xAxis.getChildAt(i+1);
                textView.setText(distance);

            }


        }
    }

    private float initTimeWin(String radar, boolean checked){
        float timeWindow = 50;


        switch (radar){
            case "100":
                if (checked){
                    timeWindow = 150;
                } else { timeWindow = 75;}
                break;

            case "500":
                if (checked){
                    timeWindow = 100;
                } else { timeWindow = 50;}
                break;

            case "1500":
                if (checked){
                    timeWindow = 15;
                } else  { timeWindow = (float)7.5;}
                break;

            case "300":
                if (checked){
                    timeWindow = 150;
                } else {
                    timeWindow = 75;
                }
                break;
        }
        return timeWindow;
    }

    private float initMedium(int selected){

        float medium =1;
        switch (selected){
            case R.id.air:
                medium = Label.AIR;
                break;

            case R.id.water:
                medium = Label.WATER;
                break;

            case R.id.sand_dry:
                medium = Label.SAND_DRY;
                break;

            case R.id.sand_wet:
                medium = Label.SAND_WET;
                break;

            case R.id.clay_wet:
                medium = Label.CLAY_WET;
                break;

            case R.id.granite:
                medium = Label.GRANITE;
                break;

            case R.id.basalt:
                medium = Label.BASALT;
                break;

            case R.id.concrete:
                medium = Label.CONCRETE;
                break;
        }
        return medium;
    }

    private int initPalette(int selected){
        int palette = 1;

        switch (selected) {

            case R.id.mono:
                palette = 1;
                break;

            case R.id.red:
                palette = 2;
                break;

            case R.id.blue:
                palette = 3;
                break;

            case R.id.green:
                palette = 4;
                break;

            case R.id.palette5:
                palette = 5;
                break;

            case R.id.palette_rainbow:
                palette = 6;
                break;
        }

        return palette;
    }





    public void setWorkMode(short workMode){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setWorkMode: " + workMode);
        }
        this.workMode = workMode;
    }

    public void setRecordedWithWheel(boolean recordedWithWheel){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setRecordedWithWheel " + recordedWithWheel);
        }
        this.recordedWithWheel = recordedWithWheel;
        if (spectroView!=null){
            spectroView.setRecordedWithWheel(recordedWithWheel);
        }
        if (xAxis!=null){
            if (recordedWithWheel){
                xAxis.setVisibility(View.VISIBLE);
            } else {
                xAxis.setVisibility(View.GONE);
            }
        }


    }

    public void setStepIndices(ArrayList<Integer> stepIndices){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setStepIndices");
        }
        this.stepIndices = stepIndices;
    }

    public void setTraceCount(int traceCount){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setTraceCount: " + traceCount);
        }
        this.traceCount = traceCount;
        if (spectroView!=null){
            spectroView.setTraceCount(traceCount);
        }
    }

    public void setStepsPerMeter(short steps){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setStepsPerMeter " + steps);
        }
        this.stepsPerMeter = steps;

    }

    public void setMedium(float medium){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setMedium: " + medium);
        }
        this.medium = medium;
    }

    public void setWindow(float window){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setWindow " + window);
        }
        this.window = window;
    }

    public void setTimeWindow(float window){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setTimeWindow: " + window);
        }
        if (yAxis != null){
            yAxis.setTimeWindow(window);
        }
    }

    public void setUpdatesWithWheel(boolean isSpatial){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setUpdatesWtihWheel: " + isSpatial);
        }
        if (spectroView!= null && spectroView.getVisibility() == View.VISIBLE){
            spectroView.setUpdateMode(isSpatial);
        }
    }

    public void setUpdateSteps(int steps){
        if (spectroView !=null && spectroView.getVisibility() == View.VISIBLE){
            spectroView.setSpatialUpdateSteps(steps);
        }
    }

    public void setRecording3D(boolean recording3D){
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "setRecording3d: " + recording3D);
        }

        if (recording3D){
            showLocationTrackerLayout();
        } else {
            hideLocationTrackerLayout();
        }


    }

    int x;
    int y;
    //in cm
    public void setX(int x){
        this.x = x;

    }

    //in cm
    public void setY(int y){
        this.y = y;
    }



    private void showLocationTrackerLayout() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "showLocationTrackerLayout");
        }
        int numberOfSlices;
        if (PreferenceManager.getInstance().getRadar().equals("1500")){

            numberOfSlices = (x/10);
            Log.i(TAG, "numb of slices: " + numberOfSlices + " x: " + x+ " x/10=" + (x/10));
        } else {
            numberOfSlices = x/50;
        }


        //populate the layout with progressbars
        LinearLayout verticalProgressBarContainer = (LinearLayout)locationTracker.findViewById(R.id.vertical_progressbar_container);
        LinearLayout.LayoutParams params  = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.weight=1;

        for (int i=0; i<numberOfSlices; i++) {
            ProgressBar bar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
            Drawable drawable;
            if ((i & 1) == 0) {
                drawable = getResources().getDrawable(R.drawable.progressbar_up);
            } else {
                drawable = getResources().getDrawable(R.drawable.progressbar_down);
            }
            bar.setProgressDrawable(drawable);
            bar.setMax(100);
            bar.setProgress(10);
            verticalProgressBarContainer.addView(bar, params);

        }

        locationTracker.setVisibility(View.VISIBLE);
        ValueAnimator animator1 = ValueAnimator.ofInt(0,(ssv_parent.getHeight()/2));
//        animator1.setDuration(300);
        animator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                locationTracker.getLayoutParams().height = value.intValue();
                locationTracker.requestLayout();
            }
        });

        ValueAnimator animator2 = ValueAnimator.ofInt(ssv_parent.getHeight(), (ssv_parent.getHeight()/2));
//        animator2.setDuration(300);
        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                ssv_parent.getLayoutParams().height = value.intValue();
                ssv_parent.requestLayout();
            }
        });


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(1000);
        animatorSet.playTogether(animator1, animator2);
        spectroView.resetSpectro();

//        animatorSet.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                int max = (y / stepsPerMeter);
//                for (int i = 0; i< max; i++) {
//                    VerticalProgressBar bar = (VerticalProgressBar) (((LinearLayout) locationTracker.findViewById(R.id.vertical_progressbar_container)).getChildAt(1));
//                    bar.updateProgress();
//                }
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });

        animatorSet.start();


    }

    private void hideLocationTrackerLayout(){}



    public synchronized void incrementLocationProgress(){
        Log.i(TAG, "incrementLocationProgress");
        LinearLayout container = (LinearLayout) locationTracker.findViewById(R.id.vertical_progressbar_container);
//        VerticalProgressBar verticalProgressBar = (VerticalProgressBar)container.getChildAt(0);
//        verticalProgressBar.incrementProgress();
        ProgressBar bar = (ProgressBar) container.getChildAt(0);
        int progres = bar.getProgress();
        progres++;
        bar.setProgress(progres);
    }

    public synchronized void decrementLocationProgress(){
        Log.i(TAG, "decrementLocationProgress");
        LinearLayout container = (LinearLayout) locationTracker.findViewById(R.id.vertical_progressbar_container);
//        VerticalProgressBar verticalProgressBar = (VerticalProgressBar)container.getChildAt(0);
//        verticalProgressBar.decrementProgress();
        ProgressBar bar = (ProgressBar) container.getChildAt(0);
        int progress = bar.getProgress();
        progress--;
        bar.setProgress(progress);
    }







    public void changePalette(int palette){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "changePalette; " + palette);
        }
        this.palette = palette;
        if (spectroView != null) {
            spectroView.setPalette(palette);
        }
    }

    public void changeMedium(float medium){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "changeMedium: " + medium);
        }
        if (yAxis !=null) {
            yAxis.setMedium(medium);
        }

    }




    public int getBrushSize(String radar){
        switch (radar){
            case "100":
                brushSize = 3;
                break;

            case "300":
                brushSize = 3;
                break;

            case "500":
                brushSize = 3;
                break;

            case "1500":
                brushSize = 3;
                break;
        }

        return brushSize;
    }

    public boolean getRecordedWithWheel(){
        return recordedWithWheel;
    }

    public Bitmap getBitmap(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getBitmap");
        }
        Bitmap spectro = null;
        Bitmap composite;

        if (spectroView!=null){
            spectro = spectroView.getBitmap();
        }
        if ((workMode == Constants.MODE_READ_2D || workMode== Constants.MODE_READ_3d) && xAxis.getVisibility()==View.VISIBLE){
            xAxis.setDrawingCacheEnabled(true);
            Bitmap xAxisB = Bitmap.createBitmap(xAxis.getDrawingCache());
            xAxis.setDrawingCacheEnabled(false);
            composite = Bitmap.createBitmap(spectro.getWidth(), spectro.getHeight(), Bitmap.Config.RGB_565);

            Canvas canvas = new Canvas();
            canvas.setBitmap(composite);

            canvas.drawBitmap(spectro, 0, 0, null);
            canvas.drawBitmap(xAxisB, 0, composite.getHeight()-Utilities.dpToPx(context, 40), null);

            return composite;
        } else {
            return spectro;
        }
    }






    public void startSpectroSurface(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "startSPectroSurface");
        }

        if (spectroView!=null) {
            spectroView.setVisibility(View.VISIBLE);
            spectroView.setSpatialUpdateSteps(PreferenceManager.getInstance().getWheelStepsPerMeter());

        }
    }

    public boolean spectroSurfaceIsReady(){
        boolean ready = false;
        if (spectroView!=null){
            if (spectroView.getVisibility() == View.VISIBLE){
                ready = true;
            }
        }

        if (BuildConfig.DEBUG){
            Log.i(TAG, "spectroSurfaceReady: " + ready);
        }
        return ready;
    }

    public void pauseSpectroThread(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "pauseSpectroTHread");
        }
        if (spectroView!=null) {
            spectroView.onPauseSpectroSV();
        }
    }

    public void resetCanvas(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "resetCanvas");
        }
        if (spectroView!=null && spectroView.getVisibility() == View.VISIBLE){
            spectroView.resetErad();
        }
    }





    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int action = MotionEventCompat.getActionMasked(event);

            switch (action){
                case MotionEvent.ACTION_DOWN:
                {
                    final int pointerIndex = MotionEventCompat.getActionIndex(event);
                    final float x = MotionEventCompat.getX(event, pointerIndex);

                    if (Erad2DrawThread.mHandler!=null){
                        Erad2DrawThread.mHandler.removeMessages(1);
                    }

                    //get drawn traces
                    lastFetchedBack = spectroView.getFirstDrawnTrace();
                    lastFetchedForth = spectroView.getLastDrawnTrace();

                    //Remember where we started
                    mLastTouchX = x;
                    //Save the id for this pointer
                    mActivePointerId = MotionEventCompat.getPointerId(event, 0);
                    break;
                }

                case MotionEvent.ACTION_MOVE:
                {
                    //find the index of the active pointer and fetch its position
                    final int pointerIndex = MotionEventCompat.findPointerIndex(event, mActivePointerId);
                    final float x = MotionEventCompat.getX(event, pointerIndex);

                    //Calculate the distance moved
                    final float dx = x - mLastTouchX;
                    int direction = (int)Math.signum(dx);
                    if(Math.abs(dx)>45) {
                        switch (direction) {
                            case 1:
                                fileNavigationListener.onNavigateBack(lastFetchedBack - 1);
                                lastFetchedBack--;
                                lastFetchedForth--;
                                break;
                            case -1:
                                spectroView.test(); //sets counter to end of screen
                                fileNavigationListener.onNavigateForth(lastFetchedForth + 1);
                                lastFetchedBack++;
                                lastFetchedForth++;
                                break;

                        }
                    }

                    break;

                }

                case MotionEvent.ACTION_UP:
                {
                    mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                    if (Erad2DrawThread.mHandler!=null){
                        Erad2DrawThread.mHandler.removeMessages(1);
                    }
                    lastFetchedBack = spectroView.getFirstDrawnTrace();
                    lastFetchedForth = spectroView.getLastDrawnTrace();

                    updateLabels();
                    break;
                }

                case MotionEvent.ACTION_CANCEL:
                {
                    mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                    if (Erad2DrawThread.mHandler!=null){
                        Erad2DrawThread.mHandler.removeMessages(1);
                    }
                    lastFetchedBack = spectroView.getFirstDrawnTrace();
                    lastFetchedForth = spectroView.getLastDrawnTrace();

                    updateLabels();
                    break;
                }

                case MotionEvent.ACTION_POINTER_UP:
                {
                    if (Erad2DrawThread.mHandler!=null){
                        Erad2DrawThread.mHandler.removeMessages(1);
                    }
                    lastFetchedBack = spectroView.getFirstDrawnTrace();
                    lastFetchedForth = spectroView.getLastDrawnTrace();
                    final int pointerIndex = MotionEventCompat.getActionIndex(event);
                    final int pointerId = MotionEventCompat.getPointerId(event, pointerIndex);

                    if (pointerId == mActivePointerId){
                        //This was the active pointer going up. choose a new pointer and adjust
                        final int newPointerIndex = pointerIndex == 0 ? 1:0;
                        mLastTouchX = MotionEventCompat.getX(event, newPointerIndex);
                        mActivePointerId = MotionEventCompat.getPointerId(event, newPointerIndex);
                    }
                    updateLabels();
                    break;
                }
            }

            return true;
        }
    };




}
