package eu.easyrad.erad.Fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.Model.Utilities;
import eu.easyrad.erad.R;
import eu.easyrad.erad.View.VerticalSeekBar;
import eu.easyrad.erad.View.WaveControl;
import eu.easyrad.erad.View.WaveSurfaceView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WaveFragment extends android.support.v4.app.Fragment{

    public static final String TAG = WaveFragment.class.getSimpleName();

    public static final float STEP_1 = (float)0.3;
    public static final float STEP_2 = (float)0.6;
    public static final float STEP_3 = (float)1;
    public static final float STEP_4 = (float)2;
    public static final float STEP_5 = (float)4;
    public static final float STEP_6 = (float)8;
    public static final float STEP_7 = (float)16;

    private onFilterAppliedListener listener;
    private onRemoteFilterAppliedListener remoteFilterAppliedListener;


    //UI
    LinearLayout topContainer;
    WaveControl waveControl;
    WaveControl field1;
    WaveControl field2;
    WaveControl field3;
    WaveControl field4;
    WaveControl field5;

    WaveSurfaceView waveSurfaceView;

    Context context;

    SeekBar seekBar;
    VerticalSeekBar verticalSeekBar;


    //Constructor
    public WaveFragment() {
        // Required empty public constructor
    }

    public static WaveFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new WaveFragment();
    }

    @Override
    public void onAttach(Context myActivity) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(myActivity);

        context = myActivity;

        try {
            listener = (onFilterAppliedListener)myActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement onFilterAppliedListener");
        }

        try {
            remoteFilterAppliedListener = (onRemoteFilterAppliedListener)myActivity;
        }catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement onRemoteFilterAppliedListener");
        }
    }

    @Override
    public void onCreate(Bundle myBundle){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(myBundle);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateView");
        }

        View v;
        v = inflater.inflate(eu.easyrad.erad.R.layout.fragment_wave, container, false);
        topContainer = (LinearLayout)v.findViewById(eu.easyrad.erad.R.id.controls_container);
        waveSurfaceView = (WaveSurfaceView)v.findViewById(eu.easyrad.erad.R.id.wave_surface_view);

        field1 = (WaveControl)v.findViewById(eu.easyrad.erad.R.id.field_1);
        field2 = (WaveControl)v.findViewById(eu.easyrad.erad.R.id.field_2);
        field3 = (WaveControl)v.findViewById(eu.easyrad.erad.R.id.field_3);
        field4 = (WaveControl)v.findViewById(eu.easyrad.erad.R.id.field_4);
        field5 = (WaveControl)v.findViewById(eu.easyrad.erad.R.id.field_5);

        field1.setCounter(PreferenceManager.getInstance().getWaveFilterArea1());
        field2.setCounter(PreferenceManager.getInstance().getWaveFilterArea2());
        field3.setCounter(PreferenceManager.getInstance().getWaveFilterArea3());
        field4.setCounter(PreferenceManager.getInstance().getWaveFilterArea4());
        field5.setCounter(PreferenceManager.getInstance().getWaveFilterArea5());

        field1.plus.setOnClickListener(field1listener);
        field1.minus.setOnClickListener(field1listener);

        field2.plus.setOnClickListener(field2listener);
        field2.minus.setOnClickListener(field2listener);

        field3.plus.setOnClickListener(field3listener);
        field3.minus.setOnClickListener(field3listener);

        field4.plus.setOnClickListener(field4listener);
        field4.minus.setOnClickListener(field4listener);

        field5.plus.setOnClickListener(field5listener);
        field5.minus.setOnClickListener(field5listener);

        if (Utilities.isLandscape(context)) {
            seekBar = (SeekBar) v.findViewById(eu.easyrad.erad.R.id.testSeekBar);
            seekBar.setMax(4);
            seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
            seekBar.setProgress(PreferenceManager.getInstance().getRadarPowerLevel());

        } else {
            verticalSeekBar = (VerticalSeekBar)v.findViewById(R.id.testSeekBar);
            verticalSeekBar.setMax(4);
            verticalSeekBar.setOnSeekBarChangeListener(seekBarChangeListener);
            verticalSeekBar.setProgress(PreferenceManager.getInstance().getRadarPowerLevel());

        }



        for (int i=0; i<topContainer.getChildCount()-1;i++){
            WaveControl child = (WaveControl)topContainer.getChildAt(i);
            applyFilter(child);
        }

        return v;

    }

    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            switch (progress) {
                case 0:
                    remoteFilterAppliedListener.onRemoteFilterApplied(2);
                    break;
                case 1:
                    remoteFilterAppliedListener.onRemoteFilterApplied(3);
                    break;
                case 2:
                    remoteFilterAppliedListener.onRemoteFilterApplied(4);
                    break;
                case 3:
                    remoteFilterAppliedListener.onRemoteFilterApplied(5);
                    break;
                case 4:
                    remoteFilterAppliedListener.onRemoteFilterApplied(6);
                    break;
            }

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    View.OnClickListener field1listener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case eu.easyrad.erad.R.id.plus:
                    field1.incrementCounter();

                    break;
                case eu.easyrad.erad.R.id.minus:
                    field1.decrementCounter();

            }
            applyFilter(field1);
        }
    };

    View.OnClickListener field2listener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()){
                case eu.easyrad.erad.R.id.plus:
                    field2.incrementCounter();
                    break;
                case eu.easyrad.erad.R.id.minus:
                    field2.decrementCounter();
            }
            applyFilter(field2);
        }
    };


    View.OnClickListener field3listener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()){
                case eu.easyrad.erad.R.id.plus:
                    field3.incrementCounter();
                    break;
                case eu.easyrad.erad.R.id.minus:
                    field3.decrementCounter();
            }
            applyFilter(field3);
        }
    };

    View.OnClickListener field4listener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()){
                case eu.easyrad.erad.R.id.plus:
                    field4.incrementCounter();
                    break;
                case eu.easyrad.erad.R.id.minus:
                    field4.decrementCounter();
            }
            applyFilter(field4);
        }
    };


    View.OnClickListener field5listener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            switch (v.getId()){
                case eu.easyrad.erad.R.id.plus:
                    field5.incrementCounter();
                    break;
                case eu.easyrad.erad.R.id.minus:
                    field5.decrementCounter();
            }
            applyFilter(field5);
        }
    };

    private synchronized void applyFilter(WaveControl parent){

        switch (parent.counter){
            case 1:
                listener.onFilterApplied(STEP_1, parent.getId());
                break;
            case 2:
                listener.onFilterApplied(STEP_2, parent.getId());
                break;
            case 3:
                listener.onFilterApplied(STEP_3, parent.getId());
                break;
            case 4:
                listener.onFilterApplied(STEP_4, parent.getId());
                break;
            case 5:
                listener.onFilterApplied(STEP_5, parent.getId());
                break;
            case 6:
                listener.onFilterApplied(STEP_6, parent.getId());
                break;
            case 7:
                listener.onFilterApplied(STEP_7, parent.getId());
                break;
        }
    }

    public interface onFilterAppliedListener{
        void onFilterApplied(float step, int partition);
    }

    public interface onRemoteFilterAppliedListener{
        void onRemoteFilterApplied(int filter);
    }

    public int getPowerLevel(){
        int progress = 1;
        if (seekBar !=null){
            progress = seekBar.getProgress();
        } else if (verticalSeekBar !=null){
            progress = verticalSeekBar.getProgress();
        }
        return progress;
    }

    public int[] getLocalFilters(){
        int[] filters = new int[5];
        filters[0] = field1.getCounter();
        filters[1] = field2.getCounter();
        filters[2] = field3.getCounter();
        filters[3] = field4.getCounter();
        filters[4] = field5.getCounter();
        return filters;

    }

    /*
    * Method to fade controls when expanding the size of the wave fragment*/
    public void fadeInControls(){

        for (int i=0; i<topContainer.getChildCount() -1; i++){

            waveControl = (WaveControl)topContainer.getChildAt(i);

            ObjectAnimator fadeIn = ObjectAnimator.ofFloat(waveControl, "alpha", 0f, 1f);
            fadeIn.setDuration(300);

            fadeIn.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator){
                    changeVisibility();
                }

                @Override
                public void onAnimationEnd(Animator animator){

                }

                @Override
                public void onAnimationCancel(Animator animator){

                }

                @Override
                public void onAnimationRepeat(Animator animator){

                }
            });
            fadeIn.start();
        }
        ObjectAnimator fade = ObjectAnimator.ofFloat(seekBar, "alpha", 0f, 1f);
        fade.setDuration(300);
        fade.start();
    }
    /*
    * Method to fade controls when shrinking the size of the wave fragment*/
    public void fadeOutControls(){
        for (int i=0; i<topContainer.getChildCount() -1; i++){

            waveControl = (WaveControl)topContainer.getChildAt(i);

            ObjectAnimator fadeOut = ObjectAnimator.ofFloat(waveControl, "alpha", 1f, 0f);
            fadeOut.setDuration(300);
            fadeOut.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator){
                }

                @Override
                public void onAnimationEnd(Animator animator){
                    changeVisibility();
                }

                @Override
                public void onAnimationCancel(Animator animator){

                }

                @Override
                public void onAnimationRepeat(Animator animator){

                }
            });
            fadeOut.start();
        }
        ObjectAnimator fade = ObjectAnimator.ofFloat(seekBar, "alpha", 1f,0f);
        fade.setDuration(300);
        fade.start();
    }

    /*
    * Changes the visibility of the wave control elements upon shrinking/expanding of the sise of wave fragment */
    private synchronized void changeVisibility(){
        for (int i=0; i<topContainer.getChildCount()-1; i++){

            waveControl = (WaveControl)topContainer.getChildAt(i);

            if (waveControl.getVisibility() == View.GONE ){
                waveControl.setVisibility(View.VISIBLE);
                seekBar.setVisibility(View.VISIBLE);

            } else if (waveControl.getVisibility() == View.VISIBLE){
                waveControl.setVisibility(View.GONE);
                seekBar.setVisibility(View.GONE);
            }
        }
    }

    /*
    * Sets the visibility of waveSurfaceView to true; initialises the drawing surface and thread */
    public void startWave(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "startWave");
        }
        if (waveSurfaceView != null) {
            waveSurfaceView.setVisibility(View.VISIBLE);
        }
    }

    public void pauseSurfaceThread(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "pauseSurfaceThread");
        }
        if (waveSurfaceView != null) {
            waveSurfaceView.pauseWaveSurfaceView();

        }
    }

    public void stopSurfaceThread(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopSurfaceThread");
        }
        if (waveSurfaceView != null){
            waveSurfaceView.stopWaveThread();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewCreated");
        }
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onActivityCreated");
        }
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewStateRestored");
        }
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null){
            field1.setCounter(savedInstanceState.getInt("field1counter"));
            field2.setCounter(savedInstanceState.getInt("field2counter"));
            field3.setCounter(savedInstanceState.getInt("field3counter"));
            field4.setCounter(savedInstanceState.getInt("field4counter"));
            field5.setCounter(savedInstanceState.getInt("field5counter"));
            if (Utilities.isLandscape(context)) {
                seekBar.setProgress(savedInstanceState.getInt("powerLevel"));
            } else {
                verticalSeekBar.setProgress(savedInstanceState.getInt("powerLevel"));
            }
        }

    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("field1counter", field1.getCounter());
        savedInstanceState.putInt("field2counter", field2.getCounter());
        savedInstanceState.putInt("field3counter", field3.getCounter());
        savedInstanceState.putInt("field4counter", field4.getCounter());
        savedInstanceState.putInt("field5counter", field5.getCounter());
        if (seekBar!=null) {
            savedInstanceState.putInt("powerLevel", seekBar.getProgress());
        } else if (verticalSeekBar !=null){
            savedInstanceState.putInt("powerLevel", verticalSeekBar.getProgress());
        }
    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();

        PreferenceManager.getInstance().setWaveFilters(
                field1.getCounter(),
                field2.getCounter(),
                field3.getCounter(),
                field4.getCounter(),
                field5.getCounter());
        if (seekBar!=null) {
            PreferenceManager.getInstance().setRadarPowerLevel(seekBar.getProgress());

        } else if (verticalSeekBar!=null)  {
            PreferenceManager.getInstance().setRadarPowerLevel(verticalSeekBar.getProgress());

        }
    }

    @Override
    public void onStop(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStop");
        }
        super.onStop();

    }

    @Override
    public void onDestroyView() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroyView");
        }
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();

    }

    @Override
    public void onDetach(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();

        stopSurfaceThread();

        topContainer = null;
        waveControl = null;
        waveSurfaceView = null;
        listener = null;
        context = null;

    }


}
