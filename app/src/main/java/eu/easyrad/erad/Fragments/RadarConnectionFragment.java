package eu.easyrad.erad.Fragments;


import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import eu.easyrad.erad.Activities.MainActivity;
import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.AdaptiveFilter;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.R;
import eu.easyrad.erad.Threads.SpectroThread;
import eu.easyrad.erad.Threads.WaveThread;
import eu.easyrad.erad.Threads.WriteEradThread;
import eu.easyrad.mylibrary.usbSerialForAndroid.src.main.java.com.hoho.android.usbserial.driver.Cp21xxSerialDriver;
import eu.easyrad.mylibrary.usbSerialForAndroid.src.main.java.com.hoho.android.usbserial.driver.UsbSerialPort;
import eu.easyrad.mylibrary.usbSerialForAndroid.src.main.java.com.hoho.android.usbserial.util.SerialInputOutputManager;

/**
 * A Fragment that connects to a radar via USB
 * Starts a new thread for the connection
 */
public class RadarConnectionFragment extends Fragment {

    public static final String TAG = RadarConnectionFragment.class.getSimpleName();
    private static final String ACTION_USB_PERMISSION ="com.android.example.USB_PERMISSION";

    PendingIntent mPermissionIntent;

    UsbManager manager;
    UsbDevice radar;
    UsbSerialPort usbPort;
    UsbDeviceConnection connection;

    RadarConnectionListener radarConnectedListener;
    Context context;

    private SerialInputOutputManager mSerialManager;
    private ExecutorService mExecutor;

    boolean isCalibrating;
    boolean comingFromCableFall;
    boolean isForward;
    boolean withFFT;
    boolean withWheel;
    boolean isTesting;
    boolean isRecording3d;

    int lastTimeWindow;
    int lastPowerLevel;
    int wheelCalibrationSteps;
    int testSteps;
    int backwardsSteps;
    int previous;


    private float filterPart1;
    private float filterPart2;
    private float filterPart3;
    private float filterPart4;
    private float filterPart5;

    private float gradient1;
    private float gradient2;
    private float gradient3;
    private float gradient4;
    private float gradient5;

    private AdaptiveFilter filter;

    public interface RadarConnectionListener{
        void onRadarStarted(boolean isCalibrating); //true for isCalibrating false for isTesting
        void onRadarConnected();
        void onRadarDisconnected();
    }




    public static RadarConnectionFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new RadarConnectionFragment();
    }

    @Override
    public void onAttach(Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(context);
        this.context = context;

        try {
            radarConnectedListener = (RadarConnectionListener)context;
        } catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "must implement listener");
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        this.context.registerReceiver(detachReceiver, filter);

        IntentFilter filter2 = new IntentFilter(ACTION_USB_PERMISSION);
        this.context.registerReceiver(mUsbReceiver, filter2);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mExecutor = Executors.newSingleThreadExecutor();
        checkUsbDevice();


        lastTimeWindow = translateTimeWindowSignal(PreferenceManager.getInstance().isLongTimeWindow());
        lastPowerLevel = translateAnalogFilterSignal(PreferenceManager.getInstance().getRadarPowerLevel());

        previous = 48;
        withWheel = false;
        isForward = true;
        withFFT = false;
        filter = new AdaptiveFilter();

        filterPart1 =1;
        filterPart2 =1;
        filterPart3 =1;
        filterPart4 =1;
        filterPart5 =1;
        calculateGradients();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
            context.registerReceiver(detachReceiver, filter);

            IntentFilter filter2 = new IntentFilter(ACTION_USB_PERMISSION);
            context.registerReceiver(mUsbReceiver, filter2);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();
//        pauseConnection();
    }

    @Override
    public void onStop(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStop");
        }
        super.onStop();
        try {
            Log.i(TAG, "context null: " + (context==null));
            context.unregisterReceiver(mUsbReceiver);
            context.unregisterReceiver(detachReceiver);
        } catch (IllegalArgumentException e ){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();
//        context = null;
        mExecutor.shutdown();
    }

    @Override
    public void onDetach(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();
    }


    public void checkUsbDevice(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "checkUsbDevice");
        }
        manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();

        if (!deviceList.isEmpty()){
            List<UsbDevice> devices = new ArrayList<>(deviceList.values());
            if (isValidDevice(devices.get(0))) {
                radar = devices.get(0);
                requestPermission(radar);

            } else {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                DialogFragment dialogFragment = NoRadarDialog.newInstance(getResources().getString(R.string.device_not_compatible));
                dialogFragment.show(manager, "noRadarDialog");
            }
        } else {

            FragmentManager manager = getActivity().getSupportFragmentManager();
            DialogFragment dialogFragment;
            short mode = ((MainActivity)getActivity()).getWorkMode();
            if (mode ==Constants.MODE_SCAN || mode == Constants.MODE_LANDING_SCREEN){
                dialogFragment = NoRadarDialog.newInstance(getResources().getString(R.string.no_radar_scan));
            } else {
                dialogFragment = NoRadarDialog.newInstance(getResources().getString(R.string.no_radar_setup));
            }
            dialogFragment.show(manager, "noRadarDialog");
        }

    }

    private boolean isValidDevice(UsbDevice device){

        int productId = device.getProductId();
        boolean deviceOk = false;
        switch (productId){
            case 0x8A9F:
                deviceOk = true;
                PreferenceManager.getInstance().setRadar("500");
                break;

            case 0x8AA0:
                deviceOk = true;
                PreferenceManager.getInstance().setRadar("300");
                break;

            case 0x8AA1:
                deviceOk = true;
                PreferenceManager.getInstance().setRadar("1500");
                break;

            case 0x8AA2:
                deviceOk = true;
                PreferenceManager.getInstance().setRadar("100");
                break;

            case 0xEA60:
                deviceOk = true;
                break;
        }
        if (BuildConfig.DEBUG){
            Log.i(TAG, "isValidDevice " + deviceOk);
        }

        return deviceOk;
    }

    public void requestPermission(UsbDevice pluggedRadar){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "requestPermission");
        }

        mPermissionIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
        manager.requestPermission(pluggedRadar, mPermissionIntent);

    }

    public void findDriver(UsbDevice device){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "findDriver");
        }

        Cp21xxSerialDriver driver = new Cp21xxSerialDriver(device);
        List<UsbSerialPort> ports = driver.getPorts();

        usbPort = ports.get(0);
        Log.i(TAG, "port is null: " + (usbPort == null));
        connection = manager.openDevice(usbPort.getDriver().getDevice());

        if (connection == null){
            return;
        }

        try {
            usbPort.open(connection);
            usbPort.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);

        } catch (IOException e) {

            try {
                usbPort.close();
            }catch (IOException e2){

            }
            usbPort = null;
            return;
        }
    }

    /*
    * returns the signal that has to be sent to set the time window
    * @window the signal*/
    private int translateTimeWindowSignal(boolean timeWindow){

        int window;
        if (timeWindow){
            window = 1;
        } else {
            window =7;
        }
        return window;
    }

    /*
   * Returns the corresponding signal for sending to the radar from the sharedPreferences
   * @local the signal*/
    private int translateAnalogFilterSignal(int analogue){

        int local =0;
        switch (analogue){
            case 0:
                local = 2;
                break;
            case 1:
                local =3;
                break;
            case 2:
                local = 4;
                break;
            case 3:
                local = 5;
                break;
            case 4:
                local = 6;
                break;
        }
        return local;
    }


    private void onDeviceStateChange(){
        stopIoManager();
        startIoManager();
    }

    Future future;
    private synchronized void stopIoManager(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopIOManager");
        }
        if (mSerialManager != null){

            mSerialManager.stop();
            mSerialManager = null;
            if (future!=null){
                future.cancel(true);
            }
            future = null;

        }


    }

    private synchronized void startIoManager(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "startIOManager");
        }

        if (usbPort != null){
            if (mSerialManager== null) {
                if (isCalibrating){
                    mSerialManager = new SerialInputOutputManager(usbPort, calibrateListener);
                } else if (isTesting){

                    mSerialManager = new SerialInputOutputManager(usbPort, testListener);
                } else {
                    mSerialManager = new SerialInputOutputManager(usbPort, mListener);
                }
            }
            future = mExecutor.submit(mSerialManager);
            sendSignal(lastPowerLevel);
            sendSignal(lastTimeWindow);
            sendSignal(lastTimeWindow);
            sendSignal(lastPowerLevel);

        }
    }



    private void sendSignal(int signal){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "sendSignal: " + signal);
        }

        byte[] foe = new byte[2];
        switch (signal){
            case 1:
                foe[0] = (byte)49;
                break;
            case 2:
                foe[0] = (byte)50;
                break;
            case 3:
                foe[0] = (byte)51;
                break;
            case 4:
                foe[0] = (byte)52;
                break;
            case 5:
                foe[0] = (byte)53;
                break;
            case 6:
                foe[0] = (byte)54;
                break;
            case 7:
                foe[0] = (byte)55;
                break;
        }


        if (mSerialManager != null) {
            mSerialManager.writeAsync(foe);
        }
    }



    Runnable updateCalibrateWheelDialog = new Runnable() {
        @Override
        public void run() {
            MainActivity activity = (MainActivity)getActivity();
            activity.notifyCalibrateWheelDialog();

        }
    };

    Runnable updateTestWheelDialog = new Runnable() {
        @Override
        public void run() {
            MainActivity activity = (MainActivity)getActivity();
            activity.updateTestWheelDialog();

        }
    };

    Runnable goingForwardNotification = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "goingForwardNotification run");
            MainActivity activity = (MainActivity)getActivity();
            activity.incrementLocationTracker();
        }
    };

    Runnable goingBackwardNotification = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "goingBacwardNotification run");
            MainActivity activity = (MainActivity)getActivity();
            activity.decrementLocationTracker();
        }
    };





    private final SerialInputOutputManager.Listener testListener = new SerialInputOutputManager.Listener() {
        private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper());;
        int previousSignal = 48;

        @Override
        public void onNewData(byte[] data) {
            if (data.length >= 572) {

                int unsigned = data[573] & 0xFF;
                if (unsigned == 70 && unsigned!=previousSignal) {

                    testSteps++;
                    mHandler.post(updateTestWheelDialog);

                }
                previousSignal = unsigned;
            }
        }

        @Override
        public void onRunError(Exception e) {

        }
    };


    private final SerialInputOutputManager.Listener calibrateListener = new SerialInputOutputManager.Listener() {
        private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper());
        int previousSignal = 48;

        @Override
        public void onNewData(byte[] data) {
            if (data.length >= 572) {

                int unsigned = data[573] & 0xFF;
                if (unsigned == 70 && unsigned!=previousSignal) {
                    wheelCalibrationSteps++;
                    mHandler.post(updateCalibrateWheelDialog);

                }
                previousSignal = unsigned;
            }
        }

        @Override
        public void onRunError(Exception e) {

        }
    };

    private final SerialInputOutputManager.Listener mListener = new SerialInputOutputManager.Listener() {

        @Override
        public void onNewData(final byte[] data) {

            float[] points = new float[data.length];

            for (int i = 0; i < data.length; i++) {

                int unsignedInt = data[i] & 0xFF;

                if (i >= 0 && i <= 115) {
                    points[i] = applyFilter(unsignedInt, gradient1, i, 1);
                } else if (i > 115 && i <= 231) {
                    points[i] = applyFilter(unsignedInt, gradient2, i, filterPart1);
                } else if (i > 231 && i <= 347) {
                    points[i] = applyFilter(unsignedInt, gradient3, i, filterPart2);
                } else if (i > 347 && i <= 463) {
                    points[i] = applyFilter(unsignedInt, gradient4, i, filterPart3);
                } else if (i > 463 && i <= 560) {
                    points[i] = applyFilter(unsignedInt, gradient5, i, filterPart4);
                } else if (i > 560 && i <= 580) {
                    points[i] = (float) unsignedInt;
                }
            }

            if (withFFT) {
                if (points.length >= 581 ) {
                    filter.feed(points);
                    points = filter.apply(points);
                }

            }

            Message waveData = Message.obtain();
            Bundle bundle = new Bundle();
            bundle.putFloatArray("data", points);
            waveData.setData(bundle);
            if (WaveThread.mHandler!=null) {
                WaveThread.mHandler.sendMessage(waveData);
            }

            Message spectroData = Message.obtain();
            Bundle other = new Bundle();
            other.putFloatArray("spectroData", points);
            spectroData.setData(other);
            if (SpectroThread.mHandler != null) {
                SpectroThread.mHandler.sendMessage(spectroData);
            }

            if (isRecording3d) {
                int signalNow = data[573] & 0xFF;
                if (signalNow == 70 && signalNow!=previous){
                    mHandler = new android.os.Handler(Looper.getMainLooper());
                    mHandler.post(goingForwardNotification);
                } else if (signalNow == 56 && signalNow!=previous){
                    mHandler = new android.os.Handler(Looper.getMainLooper());
                    mHandler.post(goingBackwardNotification);
                }
                previous = signalNow;
            }



            if (WriteEradThread.mHandler != null) {

                if (withWheel) {

                    int signalNow = data[573] & 0xFF;

                    if (signalNow == 70){
                        isForward = true;

                        if (backwardsSteps > 0) {

                            if (signalNow != previous){
                                backwardsSteps --;
                                //notify going forward

                                if (backwardsSteps <0){
                                    backwardsSteps = 0;
                                }
                            }
                        }
                    }

                    if (signalNow == 56){
                        isForward = false;

                        if (signalNow != previous){
                            backwardsSteps ++;
                            //notify going backward
                        }
                    }

                    if (backwardsSteps == 0 && isForward){
                        Message writeData = Message.obtain();
                        Bundle write = new Bundle();
                        write.putByteArray("writeData", data);
                        writeData.setData(write);
                        WriteEradThread.mHandler.sendMessage(writeData);
                    }

                    previous = signalNow;

                } else {
                    Message writeData = Message.obtain();
                    Bundle write = new Bundle();
                    write.putByteArray("writeData", data);
                    writeData.setData(write);
                    WriteEradThread.mHandler.sendMessage(writeData);
                }
            }
        }

        @Override
        public void onRunError(Exception e) {


        }
    };


    public final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){

                            if (isCalibrating){
                                findDriver(device);
                                radarConnectedListener.onRadarStarted(true);

                            } else if (isTesting){
                                findDriver(device);
                                radarConnectedListener.onRadarStarted(false);

                            } else {
                                if (comingFromCableFall) {
                                    findDriver(device);

                                } else {
                                    radarConnectedListener.onRadarConnected();
                                }


                            }
                        }
                    }
                    else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    };



    private final BroadcastReceiver detachReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED))
                radarConnectedListener.onRadarDisconnected();
            radar = null;
        }
    };


    public void calculateGradients(){
        gradient1 = (filterPart1 - 1)/116;
        gradient2 = (filterPart2-filterPart1)/116;
        gradient3 = (filterPart3-filterPart2)/116;
        gradient4 = (filterPart4 - filterPart3)/116;
        gradient5 = (filterPart5-filterPart4)/116;
    }

    private float applyFilter(int unsigned, float gradient,int position, float minValue){

        int step = position/116;
        int minX = step * 116;

        float point = 0;
        float filter = ((position-minX) * gradient)+minValue;


        if (unsigned < 128){
            point = 128 - ((128 - (float) unsigned) * filter);


            if (point <0){
                point = 0;
            }

        } else if (unsigned >127){
            point = (((float)unsigned - 128)*filter) + 128;


            if (point >255){
                point = 255;
            }
        }

        return point;
    }





    public void pauseConnection(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "pauseConnection");
        }
        stopIoManager();

    }

    public void resumeConnection(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "resumeConnection");
        }
        if (radar == null){
            checkUsbDevice();
        }
        if (usbPort == null){
            findDriver(radar);
            onDeviceStateChange();

//            sendSignal(translateTimeWindowSignal(PreferenceManager.getInstance().isLongTimeWindow()));
//            sendSignal(translateAnalogFilterSignal(PreferenceManager.getInstance().getRadarPowerLevel()));
        } else {

            startIoManager();
        }
    }

    public void stopConnectionThread(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopConnectionThread");
        }
        //for now it is here
        stopIoManager();

    }




    public void setComingFromCableFall(boolean comingFromCableFall){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setComingFromCableFall");
        }
        this.comingFromCableFall = comingFromCableFall;
    }

    public void setRecordingWithWheel(boolean withWheel){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setRecordingWithWheel " + withWheel);
        }
        this.withWheel = withWheel;

    }

    android.os.Handler mHandler;
    public void setRecording3d(boolean recording3d) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "setRecording3d: " + recording3d);
        }
        this.isRecording3d = recording3d;
        if (isRecording3d) {
            mHandler = new android.os.Handler(Looper.getMainLooper());
        } else {
            mHandler = null;
        }
    }





    public void setIsCalibrating(boolean isCalibrating){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setIsCalibrating " + isCalibrating);
        }
        this.isCalibrating = isCalibrating;

    }

    public int getWheelCalibrationSteps(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getWheelCalibrationSteps "  + wheelCalibrationSteps);
        }
        return wheelCalibrationSteps;
    }

    public void resetWheelCalibration(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "resetWheelCalibration");
        }
        wheelCalibrationSteps = 0;

    }



    public void setTesting(boolean isTesting){
        if(BuildConfig.DEBUG){
            Log.i(TAG, "setTesting: " + isTesting);
        }
        this.isTesting = isTesting;
    }

    public int getTestSteps(){
        return testSteps;
    }

    public void resetTestingSteps(){
        if(BuildConfig.DEBUG){
            Log.i(TAG, "resetTestingSteps");
        }
        testSteps = 0;
    }






    public void changeTimeWindow(int timeWindow){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "changeTimeWindow");
        }
        lastTimeWindow = timeWindow;
        sendSignal(timeWindow);

    }

    public void applyFFT(boolean withFFT){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "applyFFT "  + withFFT);
        }
        this.withFFT = withFFT;

    }

    public void applySignalFilter(float state, int area){
        switch (area){
            case R.id.field_1:
                filterPart1 = state;
                break;

            case R.id.field_2:
                filterPart2 = state;
                break;

            case R.id.field_3:
                filterPart3 = state;
                break;

            case R.id.field_4:
                filterPart4 = state;
                break;

            case R.id.field_5:
                filterPart5 = state;
                break;
        }
        calculateGradients();

    }

    public void changePowerLevel(int filter){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "changePowerLevel");
        }
        lastPowerLevel = filter;
        sendSignal(filter);

    }














}