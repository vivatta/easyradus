package eu.easyrad.erad.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.FileNameAdapter;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 16-1-11.
 */
public class ChooseFileDialog extends DialogFragment {

    public static final String TAG = ChooseFileDialog.class.getSimpleName();

    Context context;
    SelectedFileListener fileListener;

    ArrayList<File> files;

    ListView listView;
    File currentFile;


    public ChooseFileDialog(){}

    public static ChooseFileDialog newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new ChooseFileDialog();

    }

    @Override
    public void onAttach(Activity activity){

        super.onAttach(activity);
        context = activity;
        try {
            fileListener = (SelectedFileListener) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "must implement listener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        currentFile = new File(Environment.getExternalStorageDirectory()+File.separator+"EasyRad");
        if (!currentFile.exists()){
            currentFile = Environment.getExternalStorageDirectory();
        }
        files = generateFileList(currentFile);

        final FileNameAdapter adapter = new FileNameAdapter(context, files);

        listView = new ListView(getActivity());
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setVerticalFadingEdgeEnabled(true);
        listView.setScrollbarFadingEnabled(false);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                } else {
                    currentFile = (File) listView.getItemAtPosition(position);
                    if (currentFile.isDirectory()) {
                        files = generateFileList(currentFile);
                        adapter.clear();
                        adapter.addAll(files);
                        adapter.notifyDataSetChanged();

                    } else {
                        view.setSelected(true);
                    }
                }
            }
        });


        final AlertDialog.Builder builder  = new AlertDialog.Builder(getActivity());
        builder.setTitle(eu.easyrad.erad.R.string.choose_file);
        builder.setNegativeButton(R.string.btn_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.setPositiveButton(R.string.btn_open_file,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fileListener.onFileSelected(currentFile.getPath());
                    }
                });
        builder.setView(listView);
        return builder.create();
    }


    private ArrayList<File> generateFileList(final File currentDirectory){

        ArrayList<File> files = new ArrayList<>();
        files.add(currentDirectory);
        //if current directory is not the sdcard
        if (!currentDirectory.getAbsolutePath().equals(Environment.getExternalStorageDirectory().getAbsolutePath())){
            files.add(currentDirectory.getParentFile());
        }
        if (currentDirectory.list()== null){
            return files;

        } else {
            for (int i =0; i<currentDirectory.listFiles().length; i++){
                File file = currentDirectory.listFiles()[i];
                if (file.isDirectory()){
                    files.add(file);
                }
                int periodIndex = file.toString().lastIndexOf(".");
                if (periodIndex != -1){
                    String extension = file.toString().substring(periodIndex);
                    if (extension.equals(".ERAD") || extension.equals(".erad")){
                        files.add(file);
                    }
                }
            }
        }


        return files;
    }

     public interface SelectedFileListener{
        void onFileSelected(String path);
    }


}
