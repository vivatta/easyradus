package eu.easyrad.erad.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.AboutInfoAdapter;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 16-8-24.
 */
public class AboutDialog extends DialogFragment {

    public static final String TAG = AboutDialog.class.getSimpleName();
    Context context;

    public AboutDialog(){}

    public static AboutDialog newInstance() {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "newInstance");
        }

        return new AboutDialog();
    }

    @Override
    public void onAttach(Activity activity){

        super.onAttach(activity);
        context = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateDialog");
        }
        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout parent = (LinearLayout) inflater.inflate(R.layout.about_dialog, null);

        ArrayList<String> strings = new ArrayList<>();
        strings.add("Application version");
        strings.add("Contact us");
        strings.add("User guide");
        strings.add("Visit website");
        final AboutInfoAdapter adapter = new AboutInfoAdapter(getActivity(), strings);
        ListView listView = (ListView) parent.findViewById(R.id.about_list);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setVerticalFadingEdgeEnabled(true);
        listView.setScrollbarFadingEnabled(false);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        Intent send = new Intent(Intent.ACTION_SENDTO);
                        String uriText = "mailto:" + Uri.encode(context.getResources().getString(R.string.contact_email));
                        Uri uri = Uri.parse(uriText);
                        send.setData(uri);
                        startActivity(Intent.createChooser(send, "Send mail"));
                        break;
                    case 2:
                        String website = context.getResources().getString(R.string.user_guide_website);
                        Intent viewGuide = new Intent(Intent.ACTION_VIEW);
                        viewGuide.setData(Uri.parse(website));
                        startActivity(viewGuide);
                        break;
                    case 3:
                        String site = context.getResources().getString(R.string.website);
                        Intent viewSite = new Intent(Intent.ACTION_VIEW);
                        viewSite.setData(Uri.parse(site));
                        startActivity(viewSite);
                        break;
                }
            }
        });

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.about);
        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setView(parent);
        return builder.create();
    }
}
