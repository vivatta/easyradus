package eu.easyrad.erad.Fragments;


import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.Log;
import android.widget.Toast;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    public static final String TAG  = SettingsFragment.class.getSimpleName();


    SetupFragment.SetUpListener setUpListener;



    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new SettingsFragment();
    }


    @Override
    public void onAttach(Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(context);

        try {

            setUpListener = (SetupFragment.SetUpListener)context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
//        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreatePreference");
        }
        getPreferenceManager().setSharedPreferencesName(Constants.PREFERENCES);
        addPreferencesFromResource(eu.easyrad.erad.R.xml.preferences);


        final Preference calibrate_wheel = findPreference("calibrate_wheel");
        final Preference test_wheel = findPreference("test_wheel");
        final Preference about = findPreference("about");

        if (PreferenceManager.getInstance().supportsWheel()){
            calibrate_wheel.setEnabled(true);
            test_wheel.setEnabled(true);
        } else {
            calibrate_wheel.setEnabled(false);
            test_wheel.setEnabled(false);
        }

        calibrate_wheel.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setUpListener.prepareForCalibration();
                return false;
            }
        });

        test_wheel.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setUpListener.prepareForTesting();
                return false;
            }
        });

        about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                DialogFragment aboutDialog = AboutDialog.newInstance();
                aboutDialog.show(manager, "aboutDialog");
                return false;
            }
        });


        final SwitchPreferenceCompat supportsWheel = (SwitchPreferenceCompat) findPreference("wheel_key");
        supportsWheel.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                SwitchPreferenceCompat preferenceCompat = (SwitchPreferenceCompat)preference;
                if (preferenceCompat.isChecked()){
                    calibrate_wheel.setEnabled(false);
                    test_wheel.setEnabled(false);
                } else {
                    calibrate_wheel.setEnabled(true);
                    test_wheel.setEnabled(true);
                }

                return true;
            }
        });

        final ListPreference measurementSystem = (ListPreference)findPreference("measurement_system_key");
        measurementSystem.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if (PreferenceManager.getInstance().supportsWheel()) {
                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.recalibrate_after_change), Toast.LENGTH_LONG);
                    toast.show();
                }
                return true;
            }
        });

    }


    @Override
    public void onDestroy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();
    }

}
