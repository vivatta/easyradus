package eu.easyrad.erad.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.R;

/**
 *  subclass.
 */
public class LandingScreenFragment extends android.support.v4.app.Fragment implements View.OnClickListener {

    public static final String TAG = LandingScreenFragment.class.getSimpleName();

    //UI
    Button connectToRadar;
    Button openFile;
    Button settings;

    //Option selection Listener
    onOptionSelectedListener optionSelectedListener;

    public LandingScreenFragment() {
        // Required empty public constructor
    }

    public static LandingScreenFragment newInstance(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "newInstance");
        }
        return new LandingScreenFragment();
    }


    @Override
    public void onAttach(Context myActivity){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(myActivity);

        try {
            optionSelectedListener = (onOptionSelectedListener)myActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(myActivity.toString() + "must implement " + TAG + " onOptionSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreateView");
        }
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_landing_screen, container, false);
        connectToRadar = (Button)v.findViewById(R.id.connect_to_radar_button);
        openFile = (Button)v.findViewById(R.id.open_file_button);
        settings = (Button)v.findViewById(R.id.settings_button);
        connectToRadar.setOnClickListener(this);
        openFile.setOnClickListener(this);
        settings.setOnClickListener(this);

        return v;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.connect_to_radar_button:
                optionSelectedListener.onOptionSelected(v.getId());
                break;

            case R.id.open_file_button:
                optionSelectedListener.onOptionSelected(v.getId());
                break;

            case R.id.settings_button:
                optionSelectedListener.onOptionSelected(v.getId());
                break;

        }
    }

/*Listens to which option is selected*/
    public interface onOptionSelectedListener{
        void onOptionSelected(int id);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewCreated");
        }
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onActivityCreated");
        }
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onViewStateRestored");
        }
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();

    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();

    }

    @Override
    public void onStop(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStop");
        }
        super.onStop();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);

    }


    @Override
    public void onDestroyView(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroyView");
        }
        super.onDestroyView();

    }

    @Override
    public void onDestroy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();

    }

    @Override
    public void onDetach(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();
        optionSelectedListener = null;

    }


}
