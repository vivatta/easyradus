package eu.easyrad.erad.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.AdaptiveFilter;
import eu.easyrad.erad.Model.EradFileHeader;
import eu.easyrad.erad.Model.EradTraceHeader;
import eu.easyrad.erad.Model.PreferenceManager;
import eu.easyrad.erad.Model.SegyBinaryHeader;
import eu.easyrad.erad.Model.SegyTextHeader;
import eu.easyrad.erad.Model.SegyTrace;
import eu.easyrad.erad.R;
import eu.easyrad.erad.Threads.Erad2DrawThread;

/**
 * Created by vanya on 16-1-14.
 */
public class ReadFragment extends Fragment {

    public static final String TAG = ReadFragment.class.getSimpleName();
    private File file;
    long traceCount;
    FileCheckerListener fileCheckerListener;
    RandomAccessFile raf;
    ThreadPoolExecutor executor;
    boolean inflatedUI;
    boolean withFFT;
    boolean justRecorded;

    AdaptiveFilter filter;

    public ReadFragment(){}

     public static ReadFragment newInstance(){
         if (BuildConfig.DEBUG){
             Log.i(TAG, "newInstance");
         }
         return new ReadFragment();

    }

    public void checkFile(File file) throws IOException {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "checkFile");
        }
        this.file = file ;
        raf = new RandomAccessFile(this.file, "r");

        if (checkMagicWord(raf) && checkFileSize(raf)){
            if (justRecorded){
                String text = file.getAbsolutePath() + " " + getResources().getString(R.string.recorded_successfully);
                Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_LONG);
                toast.show();
                if (PreferenceManager.getInstance().recordSegy()){
                    Log.i(TAG, "export to segy begin");
                    copyToSegy();
                }
            } else {
                fileCheckerListener.onFileOk(getHeader(raf));
            }

        } else {
            if (justRecorded){
                String text = getResources().getString(R.string.recorded_unsuccessfuly);
                Toast toast = Toast.makeText(getContext(), text, Toast.LENGTH_LONG);
                toast.show();

            } else {
                fileCheckerListener.onFileNotOk();
            }
        }
        justRecorded = false;

    }

    public void setFile(String filepath){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setFile " + filepath);
        }
        file = new File(filepath);
    }


    public void setJustRecorded(boolean justRecorded){
        this.justRecorded = justRecorded;
    }

    public void setWithFFT(boolean withFFT){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setWithFFt" + withFFT);
        }
        this.withFFT = withFFT ;
    }

    private boolean checkMagicWord(RandomAccessFile raf){


        boolean isOK;
        byte one =0;
        byte two = 0;
        byte three = 0;
        byte four = 0;
        byte five = 0;
        byte six = 0;
        byte seven = 0;
        byte eight = 0;

        try {
            raf.seek(0);
            one = raf.readByte();
            two = raf.readByte();
            three = raf.readByte();
            four = raf.readByte();
            five = raf.readByte();
            six = raf.readByte();
            seven = raf.readByte();
            eight = raf.readByte();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (one==0x00 && two==0x45 && three==0x41 && four==0x53 && five==0x59 && six==0x52 && seven==0x41 && eight==0x44){
                isOK = true;
            } else {
                isOK = false;
            }
        }
        if (BuildConfig.DEBUG){
            Log.i(TAG, "magicWordOk: " + isOK);
        }
        return isOK;

    }

    private boolean checkFileSize(RandomAccessFile raf){

        boolean isOk;
        long supposedSize = 0;

        try {
            raf.seek(file.length()-8);
            traceCount = raf.readLong();

            //File_header_size + number_of_traces*(trace_header_size + sample_size) + long_number_of_traces = 204 + numOfTraces*(28+581)+8
            supposedSize = 204 + 8 + traceCount*(609);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (supposedSize == file.length()){
                isOk = true;
            } else {
                isOk = false;
            }
        }

        if (BuildConfig.DEBUG){
            Log.i(TAG, "FileSizeOK: " + isOk);
        }

        return isOk;

    }


    private EradFileHeader getHeader(RandomAccessFile raf) throws IOException {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getHeader with raf");
        }
        EradFileHeader header = new EradFileHeader();
        header.read(raf);
        header.setTraceCount(traceCount);

        return header;

    }

    public EradFileHeader getHeader() throws IOException{
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getHeader");
        }
        EradFileHeader fileHeader = null;
        if (raf !=null){
            fileHeader = new EradFileHeader();
            fileHeader.read(raf);
            fileHeader.setTraceCount(traceCount);
        }
        return fileHeader;
    }



    public synchronized void getFileToTrace(int index){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getFileToTrace; " + index);
        }
        final int traceIndex;
        if (index>traceCount){
            traceIndex = (int)traceCount;
        } else {
            traceIndex = index;
        }

        filter = new AdaptiveFilter();
        if (raf!=null){

            executor.execute(new Runnable() {
                @Override
                public void run() {

                    if (!inflatedUI) {
                        try {
                            Thread.sleep(1100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    for (int i=0, j=232; i<traceIndex; i++, j+=609){
                        float[] toSend = new float[582];
                        try {
                            raf.seek(j-28); //trace index
                            toSend[0] = (float)raf.readLong();
                            raf.seek(j); //trace data
                            for (int f=1; f<582; f++){
                                toSend[f] = raf.readByte() & 0xFF;
                            }



                            if (withFFT){
                                filter.feed(toSend);
                                toSend = filter.applyForRead(toSend);
                            }

                            Message message1 = Message.obtain();
                            message1.what = 1;
                            Bundle spectroData = new Bundle();
                            spectroData.putFloatArray("spectroData", toSend);
                            message1.setData(spectroData);

                            if (Erad2DrawThread.mHandler != null) {
                                try {
                                    Erad2DrawThread.mHandler.sendMessage(message1);

                                } catch (RuntimeException e){
                                    e.printStackTrace();
                                }

                            } else {

                            }

                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }


    public void getWholeFile() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getWholeFIle");
        }
        filter = new AdaptiveFilter();
        if (raf!=null){
            executor.execute(new Runnable() {
                @Override
                public void run() {


                    if (!inflatedUI) {
                        try {
                            Thread.sleep(1100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    for (int i = 232; i < file.length(); i += 609) {
                        float[] toSend = new float[582];//581

                        try {
                            raf.seek(i-28); //trace index
                            toSend[0] = raf.readLong();

                            raf.seek(i);
                            for (int j = 1; j < 582; j++) { //j=0
                                toSend[j] = raf.readByte() & 0xFF;
                            }

                            if (withFFT){
                                filter.feed(toSend);
                                toSend = filter.applyForRead(toSend);
                            }


                            Message message1 = Message.obtain();
                            message1.what=1;
                            Bundle spectroData = new Bundle();
                            spectroData.putFloatArray("spectroData", toSend);
                            message1.setData(spectroData);

                            if (Erad2DrawThread.mHandler != null) {
                                Erad2DrawThread.mHandler.sendMessage(message1);
                            } else {

                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }
                }
            });
        }
    }


    public void fetchTrace(final int index, final boolean isForward){

        if (index <= 0 || index > traceCount){
            return;
        }

        if (raf!=null){
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    int indexInFile = (203 + index*28 + ((index-1)*581) ) + 1;

                    if (isForward){
                        for (int i=0, j=indexInFile; i<1; i++, j+=609){
                            float[] toSend = new float[582];
                            try {
                                //get trace index
                                raf.seek(j-28);
                                toSend[0] = (float)raf.readLong();
                                //trace data
                                raf.seek(j);
                                for (int f=1; f<582; f++){
                                    toSend[f] = raf.readByte() & 0xFF;
                                }

                                if (withFFT){

                                    filter.feed(toSend);
                                    toSend  = filter.applyForRead(toSend);
                                }

                                Message message1 = Message.obtain();
                                message1.what = 1;
                                Bundle spectroData = new Bundle();
                                spectroData.putFloatArray("spectroData", toSend);
                                message1.setData(spectroData);

                                if (Erad2DrawThread.mHandler != null) {
                                    Erad2DrawThread.mHandler.sendMessage(message1);
                                } else {

                                }

                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }

                    } else {
                        for (int i=0, j=indexInFile; i<1; i++, j-=609){ //i<10
                            float[] toSend = new float[582];
                            try {
                                //get trace index
                                raf.seek(j-28);
                                toSend[0] = (float)raf.readLong();
                                //trace data
                                raf.seek(j);
                                for (int f=1; f<582; f++){
                                    toSend[f] = raf.readByte() & 0xFF;
                                }


                                if (withFFT){

                                    filter.feed(toSend);
                                    toSend = filter.applyForRead(toSend);
                                }

                                Message message1 = Message.obtain();
                                message1.what =1;
                                Bundle spectroData = new Bundle();
                                spectroData.putFloatArray("spectroData", toSend);
                                message1.setData(spectroData);

                                if (Erad2DrawThread.mHandler != null) {
                                    Erad2DrawThread.mHandler.sendMessage(message1);
                                } else {

                                }

                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }


    }


    public void fetchStepIndices(){

        StepIndexGenerator generator = new StepIndexGenerator(file.length(), fileCheckerListener);
        generator.executeOnExecutor(executor, raf);
    }

    public void copyToSegy(){

        EradToSegyConverter converter = new EradToSegyConverter(getContext(), file.length());
        converter.executeOnExecutor(executor, raf);
    }

    public interface FileCheckerListener{
        void onFileOk(EradFileHeader header);
        void onFileNotOk();
        void onStepIndicesReady(ArrayList<Integer> listOfSteps);
    }


    public void setInflatedUI(boolean inflatedUI){

        this.inflatedUI = inflatedUI;
    }



        private static class StepIndexGenerator extends AsyncTask<RandomAccessFile, Void, ArrayList<Integer>>{
            long fileLength;
            FileCheckerListener listener;

            public StepIndexGenerator(long fileLength, FileCheckerListener listener){
                this.fileLength = fileLength;
                this.listener = listener;
            }

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
            }

            @Override
            protected ArrayList<Integer> doInBackground(RandomAccessFile...params){
                ArrayList<Integer> steps = new ArrayList<>();
                RandomAccessFile raf = params[0];
                if (raf!=null){
                    try {
                        raf.seek(204);
                        steps.add((int) raf.readLong());
                        raf.seek(805);
                        int previous = raf.readByte() & 0xFF;

                        for (int i=1414; i<fileLength; i+=609) {
                            raf.seek(i);
                            int signalNow = raf.readByte() & 0xFF;
                            if (signalNow != previous){
                                raf.seek(i-601);  //TODO test if this is the real num
                                steps.add((int) raf.readLong()); // adds the index of the trace if it is a step
                                previous = signalNow; // this is the new previous signal
                            }
                        }

                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
                return steps;
            }

            @Override
            protected void onPostExecute(ArrayList<Integer> steps){
                super.onPostExecute(steps);
                if (steps!=null){
                    listener.onStepIndicesReady(steps);
                }
            }

        }


    private static class EradToSegyConverter extends AsyncTask<RandomAccessFile, Void, Void>{

        String filenameDestination;
        long fileLength;
        Context context;

        public EradToSegyConverter(Context context, long fileLength){
            Calendar now = Calendar.getInstance();
            File directorySEGY = new File(Environment.getExternalStorageDirectory()+File.separator+"EasyRad"+File.separator+"SEG-Y");
            directorySEGY.mkdirs();
            filenameDestination = directorySEGY.getAbsolutePath()+
                    File.separator+now.get(Calendar.YEAR)+"_"+
                    (now.get(Calendar.MONTH)+1)+"_"+
                    now.get(Calendar.DAY_OF_MONTH)+"_"+
                    now.get(Calendar.HOUR_OF_DAY)+
                    now.get(Calendar.MINUTE)+".SGY";

            this.fileLength = fileLength;
            this.context = context;

        }

        @Override
        protected Void doInBackground(RandomAccessFile... params) {
            RandomAccessFile raf = params[0];

            SegyTextHeader segyTextHeader = new SegyTextHeader();
            SegyBinaryHeader segyBinaryHeader = new SegyBinaryHeader();

            short day;
            short year;

            if (raf!=null){

                try {
                    EradFileHeader header = new EradFileHeader();
                    header.read(raf);

                    segyTextHeader.setOperator(header.getOperator());
                    segyTextHeader.setLocation(header.getLocation());
                    segyTextHeader.setRadarType(header.getRadarAsString());
                    segyTextHeader.setMedium(header.getMediumAsString());
                    segyTextHeader.setTimeWindow(header.getTimeWindow());

                    year = header.getYear();
                    Calendar calendar = Calendar.getInstance();
                    calendar.set((int)header.getYear(), (int)header.getMonth(), (int)header.getDay());
                    day = (short)calendar.get(Calendar.DAY_OF_YEAR);

                    FileOutputStream fos = new FileOutputStream(filenameDestination);
                    final DataOutputStream dos = new DataOutputStream(fos);

                    segyTextHeader.write(dos);
                    segyBinaryHeader.write(dos);

                    for (int i = 204; i < fileLength-8; i += 609) {

                        EradTraceHeader eradTraceHeader = new EradTraceHeader();
                        eradTraceHeader.read(raf, i);

                        short[] data = new short[581];
                        for (int j=0; j<581; j++){
                            data[j] = (short) ((raf.readByte() &0xFF) *100);
                        }

                        SegyTrace segyTrace = new SegyTrace(data);
                        segyTrace.setTrace_sequence_line((int)eradTraceHeader.getIndex());
                        segyTrace.setField_record((int)eradTraceHeader.getIndex());
                        segyTrace.setEnsemble_num((int)eradTraceHeader.getIndex());
                        segyTrace.setTrace_sequence_segy((int)eradTraceHeader.getIndex());
                        segyTrace.setNum_of_samples(eradTraceHeader.getSamples());
                        segyTrace.setHour(eradTraceHeader.getHour());
                        segyTrace.setMinute(eradTraceHeader.getMinute());
                        segyTrace.setSecond(eradTraceHeader.getSecond());
                        segyTrace.setYear(year);
                        segyTrace.setDay(day);


                        segyTrace.write(dos);
                        dos.flush();
                    }

                    dos.flush();
                    dos.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }



            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);
            String text = filenameDestination + " " + context.getResources().getString(R.string.recorded_successfully);
            Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }



    @Override
    public void onAttach(Context activity){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onAttach");
        }
        super.onAttach(activity);
        try {
            fileCheckerListener = (FileCheckerListener) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "must implement fileCheckerListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onCreate");
        }
        super.onCreate(savedInstanceState);

        executor = new ThreadPoolExecutor(1, 2, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());

        setRetainInstance(true);
        inflatedUI = false;

        if (file!=null){
            try {
                checkFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return null;
    }


    @Override
    public void onStart(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStart");
        }
        super.onStart();

    }

    @Override
    public void onPause(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPause");
        }
        super.onPause();

    }

    @Override
    public void onResume(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResume");
        }
        super.onResume();

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onActivityCreated");
        }
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onSaveInstanceState");
        }
        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void onStop(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onStop");
        }
        super.onStop();

    }

    @Override
    public void onDestroy(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDestroy");
        }
        super.onDestroy();
        executor.shutdown();

    }

    @Override
    public void onDetach() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onDetach");
        }
        super.onDetach();


    }


}
