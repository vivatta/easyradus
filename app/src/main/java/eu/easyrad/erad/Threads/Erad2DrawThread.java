package eu.easyrad.erad.Threads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Gradient;

/**
 * Created by vanya on 16-2-3.
 */
public class Erad2DrawThread extends Thread {

    public static final String TAG = Erad2DrawThread.class.getSimpleName();

    private SurfaceHolder holder;
    public static Handler mHandler;


    float[] data;

    private boolean running = false;


    public float canvasHeight;
    public int canvasWidth;

    private float coefficientY;
    private float coefficientX;

    private Canvas canvas;
    public Paint paint;
    private Paint labelPaint;
    private Paint navBarPaint;

    //buffered canvas 1
    private Canvas bufferedCanvas;
    private Bitmap bufferedBitmap;

    //buffered canvas 2
    private Canvas offloadCanvas;
    private Bitmap offloadBitmap;

    private Matrix matrix;

    //point coordinates
    private float x;
    private float y;

    int colorValue;

    //helper recs for scrolling
    private Rect source;
    private Rect destination;
    private Rect sourceBack;
    private Rect destinationBack;

    //palette modes
    public static final int MONO =1;
    public static final int RED = 2;
    public static final int BLUE = 3;
    public static final int GREEN = 4;
    public static final int PALETTE_5 = 5;
    public static final int PALETTE_RAINBOW = 6;

    public int paletteFlag;

    int labelCounter;
    int someCounter;
    int pxPerDp;

    int traceCount;


    public Erad2DrawThread(SurfaceHolder holder, int pxPerDp){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new Erad2DrawThread");
        }
        this.holder = holder;
        this.pxPerDp = pxPerDp;

        paint = new Paint();
        paint.setStrokeWidth(dpToPx(2)); //3
        paint.setColor(Color.argb(255, 255, colorValue, 255)); // default value

        labelPaint = new Paint();
        labelPaint.setTextSize(dpToPx(15));
        labelPaint.setColor(Color.WHITE);

        navBarPaint = new Paint();

        paletteFlag = MONO;

        bufferedCanvas = new Canvas();
        offloadCanvas = new Canvas();

        matrix = new Matrix();
        someCounter = dpToPx(5); //100
        firstDrawn = 1;
        lastDrawn = 0;


    }

    public void setCanvasHeight(int newHeight){
        this.canvasHeight = (float) newHeight;
        coefficientY = (canvasHeight-dpToPx(5))/560; //8

    }


    public void setCanvasWidth(int newWidth){
        this.canvasWidth = newWidth;
        coefficientX = (float) canvasWidth/traceCount;


    }

    public void setBufferedBitmaps(int width, int height){
        bufferedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        bufferedCanvas.setBitmap(bufferedBitmap);

        navBarPaint.setARGB(255,255,255,255);
        bufferedCanvas.drawRect(0, canvasHeight - dpToPx(5), canvasWidth, canvasHeight, navBarPaint);

        offloadBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        offloadCanvas.setBitmap(offloadBitmap);

        source = new Rect(dpToPx(2),0,offloadBitmap.getWidth(), offloadBitmap.getHeight()); //leaving 2px room for new line  //4
        destination = new Rect(0, 0, bufferedBitmap.getWidth()-dpToPx(2), bufferedBitmap.getHeight());

        sourceBack = new Rect(0,0,bufferedBitmap.getWidth()-dpToPx(2),bufferedBitmap.getHeight());
        destinationBack = new Rect(dpToPx(2),0, offloadBitmap.getWidth(), offloadBitmap.getHeight());

    }

    public void setTraceCount(int traceCount) {
        this.traceCount = traceCount;
        coefficientX = (float) canvasWidth/traceCount;
    }

    public void setPaletteFlag(int flag){
        this.paletteFlag = flag;
    }

    //0-black; 100- black; 120 - gray; 255 - white;
   private Gradient mono = new Gradient(
            new int[]{0, 100,120,140, 255},
            new int[]{Color.rgb(0,0,0),
                    Color.rgb(0,0,0),
                    Color.rgb(153,153,153),
                    Color.rgb(255,255,255),
                    Color.rgb(255,255,255)});

    //0-deep red; 120-ERred; 131-ERgreen; 255-ERgreen
    private Gradient green = new Gradient(
            new int[]{0,120,131,255},
            new int[]{Color.rgb(141,16,21),
                    Color.rgb(236,28,36),
                    Color.rgb(132,194,37),
                    Color.rgb(132,194,37)}
    );

    //0-yellow; 115-red; 128-green; 135-blue;255-black
    private Gradient red = new Gradient(
            new int[]{0,115,128,135,255},
            new int[]{Color.rgb(255,255,0),
                    Color.rgb(255,0,0),
                    Color.rgb(0,255,0),
                    Color.rgb(0,0,255),
                    Color.rgb(0,0,0)}
    );

    //0-blue; 120-black, 131-yellow, 255-yellow
    private Gradient blue = new Gradient(
            new int[]{0,120,131,255},
            new int[]{Color.rgb(0, 0, 255),
                    Color.rgb(0,0,0),
                    Color.rgb(255,255,0),
                    Color.rgb(255,255,0)}
    );

    //0-dark red; 115-red; 128-yellow; 140-blue;255-dark blue;
    private Gradient palette5 = new Gradient(
            new int[]{0,115,128,140,255},
            new int[]{Color.rgb(102,0,0),
                    Color.rgb(255,0,0),
                    Color.rgb(255,255,0),
                    Color.rgb(0,0,255),
                    Color.rgb(102,0,0)}
    );

    private Gradient rainbow = new Gradient(
            new int[]{0,17,34,51,68,85,102,119,136,153,170,187,204,221,238,255},
            new int[]{Color.rgb(187,170,188),
                    Color.rgb(136,25,130),
                    Color.rgb(18, 9, 136),
                    Color.rgb(39,113,2),
                    Color.rgb(254, 22, 0),
                    Color.rgb(105, 36, 29),
                    Color.rgb(60,58,61),
                    Color.rgb(60, 58, 61),
                    Color.rgb(130, 121, 122),
                    Color.rgb(145,11,2),
                    Color.rgb(253,33,0),
                    Color.rgb(251,238,2),
                    Color.rgb(5, 215, 2),
                    Color.rgb(2,20,250),
                    Color.rgb(253,58,250),
                    Color.rgb(255,255,255)}
    );



    public void setPaintColor(int pointValue){

        switch (paletteFlag){
            case MONO:
                paint.setColor(mono.getColorForPoint(pointValue));
                break;

            case GREEN:
                paint.setColor(green.getColorForPoint(pointValue));

                break;

            case RED:
                paint.setColor(red.getColorForPoint(pointValue));

                break;
            case BLUE:
                paint.setColor(blue.getColorForPoint(pointValue));
                break;

            case PALETTE_5:
                paint.setColor(palette5.getColorForPoint(pointValue));
                break;

            case PALETTE_RAINBOW:
                paint.setColor(rainbow.getColorForPoint(pointValue));
                break;
        }
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void setRecordedWithWheel(boolean withWheel){
        this.withWheel = withWheel;
        if (withWheel){
            coefficientY =  (canvasHeight-dpToPx(26))/560;
        } else {
            coefficientY =  (canvasHeight-dpToPx(5))/560;
        }
    }

    boolean withWheel;
    int previous;
    int lastDrawn;
    int firstDrawn;
    int lastCounter;

    @Override
    public void run(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "run");
        }
        while (running){
            previous = 48;
            labelCounter= 0;

            Looper.prepare();

            canvas = null;
            mHandler = new Handler(){
                @Override
                public void handleMessage(Message message) {
                    super.handleMessage(message);

                    data = message.getData().getFloatArray("spectroData");

                    if (data[0]==lastDrawn+1){
                        lastDrawn = (int)data[0];

                        if (someCounter >= canvasWidth) {
                            bufferedCanvas.drawBitmap(offloadBitmap, source, destination, null);
                            firstDrawn = lastDrawn - (canvasWidth/dpToPx(2)) + 1;

                            someCounter = lastCounter;
                        }

                        if (someCounter < canvasWidth) { //canvasWidth
                            someCounter += dpToPx(2);
                            lastCounter = someCounter;
                        }

                        navBarPaint.setARGB(255,255,255,255);
                        bufferedCanvas.drawRect(0, canvasHeight - dpToPx(5), canvasWidth, canvasHeight, navBarPaint);

                        navBarPaint.setARGB(255, 132, 194, 37);
                        bufferedCanvas.drawRect(firstDrawn*coefficientX, canvasHeight-dpToPx(5), lastDrawn*coefficientX, canvasHeight, navBarPaint);


                        for (int i = 1; i < data.length - 21; i++) { //i=0

                            colorValue = (int) data[i];
                            setPaintColor(colorValue);
                            x = (float) someCounter;
                            y = (float) i * coefficientY;
                            paint.setStrokeWidth(dpToPx(2));
                            bufferedCanvas.drawPoint(x, y, paint);

                        }


                        try {
                            canvas = holder.lockCanvas();

                            synchronized (holder) {
                                if (canvas != null) {

                                    canvas.drawBitmap(bufferedBitmap, matrix, null); //drawing to the back buffer from bufferedBitmap
                                    offloadCanvas.drawBitmap(bufferedBitmap, matrix, null); //
                                } else {
                                    return;
                                }
                            }
                        } finally {
                            if (canvas != null) {
                                holder.unlockCanvasAndPost(canvas); //switch back buffer with front buffer
                            }
                        }


                    } else if (data[0]==firstDrawn-1){

                        firstDrawn = (int)data[0];
                        bufferedCanvas.drawBitmap(offloadBitmap, sourceBack, destinationBack, null);
                        someCounter = dpToPx(5); //0

                        for (int i = 1; i < data.length - 21; i++) { //i=0

                            colorValue = (int) data[i];
                            setPaintColor(colorValue);
                            x = (float) someCounter;
                            y = (float) i * coefficientY;
                            bufferedCanvas.drawPoint(x, y, paint);

                        }
                        lastDrawn = firstDrawn + (canvasWidth / dpToPx(2)-1);

                        navBarPaint.setARGB(255,255,255,255);
                        bufferedCanvas.drawRect(0, canvasHeight - dpToPx(5), canvasWidth, canvasHeight, navBarPaint);

                        navBarPaint.setARGB(255, 132,194,37);
                        bufferedCanvas.drawRect(firstDrawn*coefficientX, canvasHeight-dpToPx(5), lastDrawn*coefficientX, canvasHeight, navBarPaint);



                        try {
                            canvas = holder.lockCanvas();

                            synchronized (holder) {
                                if (canvas != null) {

                                    canvas.drawBitmap(bufferedBitmap, matrix, null); //drawing to the back buffer from bufferedBitmap
                                    offloadCanvas.drawBitmap(bufferedBitmap, matrix, null); //
                                } else {
                                    return;
                                }
                            }
                        } finally {
                            if (canvas != null) {
                                holder.unlockCanvasAndPost(canvas); //switch back buffer with front buffer
                            }
                        }

                    }


                }
            };

            Looper.loop();
        }
    }

    public void reset(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "reset");
        }
        mHandler.removeMessages(1);
        someCounter=dpToPx(5); //0
        firstDrawn = 1;
        lastDrawn = 0;
        bufferedCanvas.drawColor(Color.BLACK);
    }


    private int dpToPx(int dp){
        return dp*pxPerDp;
    }

    public void recycleBitmaps(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "recycleBitmaps");
        }
        bufferedBitmap.recycle();
        offloadBitmap.recycle();
    }

    public int getFirstDrawn(){
        return firstDrawn;
    }

    public int getLastDrawn(){
        return lastDrawn;
    }

    public void setSomeCounterForward(){
        someCounter = lastCounter ;
    }

    public Bitmap getBufferedBitmap(){
        return bufferedBitmap;
    }

}
