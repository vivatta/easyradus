package eu.easyrad.erad.Threads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceHolder;

import eu.easyrad.erad.BuildConfig;


/**
 * Created by vanya on 15-8-20.
 */
public class WaveThread extends Thread{

    public static final String TAG = WaveThread.class.getSimpleName();

    private SurfaceHolder holder;
    public static Handler mHandler;

    float[] data;

    private boolean running = false;

    public float canvasWidth;
    public float canvasHeight;

    private Canvas canvas;
    public Paint paint;

    private Path path;
    private Context context;

    public WaveThread(SurfaceHolder holder, Context context){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new WaveThread");
        }
        this.context = context;
        this.holder = holder;
        paint = new Paint();
    }

    public void setCanvasWidth(int newWidth){
        canvasWidth = (float)newWidth;
    }

    public void setCanvasHeight(int newHeight){
        canvasHeight = (float)newHeight;
    }

    @Override
    public void run(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "run");
        }
        while (running){
            Looper.prepare();

            canvas = null;
            mHandler = new Handler(){
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    mHandler.obtainMessage();
                    data = msg.getData().getFloatArray("data");

                    try {
                        canvas = holder.lockCanvas();
                        synchronized (holder) {
                            if (canvas !=null) {
                                canvas.drawColor(Color.parseColor("#cacaca")); //#cacaca waveComponentGreen in colors.xml
                                paint.setColor(Color.BLACK);
                                paint.setStyle(Paint.Style.STROKE);
                                paint.setStrokeWidth(dpToPx(1));
                                if (data!=null) {
                                    canvas.drawPath(producePath(data), paint);
                                }
                            } else {
                                return;
                            }
                        }
                    } finally {
                        if (canvas != null) {
                            holder.unlockCanvasAndPost(canvas);
                        }
                    }

                }
            };
            Looper.loop();
        }
    }

    public void setRunning(boolean isRunning){
        running = isRunning;
    }


    private int dpToPx(int dp){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
    }


    private Path producePath(float[] data){
        path = new Path();
        float coefficientY; //ratio between height of canvas and possible values
        float coefficientX; //ratio between width of canvas and possible values

        if (canvasHeight>canvasWidth){  // landscape
            coefficientX = canvasWidth/256; //255 here - max value
            coefficientY = canvasHeight/581; // 581 max num of values

            for (int i=0; i<data.length; i++){
                path.lineTo(data[i]*coefficientX, i*coefficientY);  //resample in order to fit screen
            }
        } else if (canvasWidth>canvasHeight){ //portrait
            coefficientX = canvasWidth/581; //max num of values
            coefficientY = canvasHeight/256; // max value

            for (int i=0; i<data.length; i++){
                path.lineTo(i*coefficientX, data[i]*coefficientY);
            }
        }
        return path;
    }



}
