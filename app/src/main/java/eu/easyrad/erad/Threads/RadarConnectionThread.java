package eu.easyrad.erad.Threads;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import eu.easyrad.erad.Activities.MainActivity;
import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.AdaptiveFilter;
import eu.easyrad.mylibrary.usbSerialForAndroid.src.main.java.com.hoho.android.usbserial.driver.Cp21xxSerialDriver;
import eu.easyrad.mylibrary.usbSerialForAndroid.src.main.java.com.hoho.android.usbserial.driver.UsbSerialPort;
import eu.easyrad.mylibrary.usbSerialForAndroid.src.main.java.com.hoho.android.usbserial.util.SerialInputOutputManager;

/**
 * Created by vanya on 15-8-20.
 */
public final class RadarConnectionThread extends Thread {

    public static final String TAG = RadarConnectionThread.class.getSimpleName();

    private SerialInputOutputManager mSerialManager;
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    public boolean isRunning = true;

    private UsbDevice device;
    private UsbManager manager;
    private UsbSerialPort usbPort;

    public static Handler mHandler;

    UsbDeviceConnection connection;

    private float filterPart1;
    private float filterPart2;
    private float filterPart3;
    private float filterPart4;
    private float filterPart5;

    private float gradient1;
    private float gradient2;
    private float gradient3;
    private float gradient4;
    private float gradient5;


    private int timeWindow;
    private int localFilter;

    private boolean isCalibrating;
    private int wheelCalibration;

    private boolean withFFT;



    private boolean withWheel;

    private AdaptiveFilter filter;


    public RadarConnectionThread(){
    }



    public RadarConnectionThread(UsbDevice device, UsbManager manager, int timeWindow, int localFilter){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new RadarConnectionThread");
        }
        this.device = device;
        this.manager = manager;
        this.timeWindow = timeWindow;
        this.localFilter = localFilter;
        withWheel = false;
        isForward = true;
        withFFT = false;
        filter = new AdaptiveFilter();

        isCalibrating = false;
        filterPart1 =1;
        filterPart2 =1;
        filterPart3 =1;
        filterPart4 =1;
        filterPart5 =1;
        calculateGradients();
    }

    public void setWithFFT(boolean withFFT){
        this.withFFT = withFFT;
    }

    public void setRunning(boolean running){
        isRunning = running;
    }

    public void setCalibrating(boolean isCalibrating){
        this.isCalibrating = isCalibrating;
    }

    public int getWheelCalibration(){
        return wheelCalibration;
    }

    public void resetWheelCalibration(){
        wheelCalibration = 0;
    }

    public void setIsRecordingWithWheel(boolean withWheel) {
        this.withWheel = withWheel;
    }

    public void run(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "run");
        }
        while (isRunning) {

            try {
                this.sleep(200);
            } catch (InterruptedException e){
                e.printStackTrace();
            }

            Looper.prepare();
            mHandler = new Handler();
            findDriver(device);
            Looper.loop();
        }
    }

    public void findDriver(UsbDevice device){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "findDriver");
        }
        Cp21xxSerialDriver driver = new Cp21xxSerialDriver(device);
        List<UsbSerialPort> ports = driver.getPorts();

        usbPort = ports.get(0);
        connection = manager.openDevice(usbPort.getDriver().getDevice());

        if (connection == null){
            return;
        }

        try {
            usbPort.open(connection);
            usbPort.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);

        } catch (IOException e) {

            try {
                usbPort.close();
            }catch (IOException e2){

            }
            usbPort = null;
            return;
        }
        onDeviceStateChange();
        sendSignal(timeWindow);
        sendSignal(localFilter);
    }


    public synchronized void stopIoManager(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopIOManager");
        }
        if (mSerialManager != null){

            mSerialManager.stop();
            mSerialManager = null;

        }
    }


    public synchronized void startIoManager(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "startIOManager");
        }
        if (usbPort != null){
            if (mSerialManager== null) {
                mSerialManager = new SerialInputOutputManager(usbPort, mListener);
            }
            mExecutor.submit(mSerialManager);
            sendSignal(lastSent);

        }
    }

    public synchronized void onDeviceStateChange(){
        stopIoManager();
        startIoManager();
    }

    int lastSent;

    public void sendSignal(int signal){

        lastSent = signal;
        byte[] foe = new byte[2];
        switch (signal){
            case 1:
                foe[0] = (byte)49;
                break;
            case 2:
                foe[0] = (byte)50;
                break;
            case 3:
                foe[0] = (byte)51;
                break;
            case 4:
                foe[0] = (byte)52;
                break;
            case 5:
                foe[0] = (byte)53;
                break;
            case 6:
                foe[0] = (byte)54;
                break;
            case 7:
                foe[0] = (byte)55;
                break;
        }

        if (mSerialManager != null) {
            mSerialManager.writeAsync(foe);
        }
    }

    int previous = 48;
    boolean isForward;


    private final SerialInputOutputManager.Listener mListener = new SerialInputOutputManager.Listener() {
        @Override
        public void onNewData(final byte[] data) {

            if (isCalibrating) {
                if (data.length >= 572) {
                    int unsigned = data[573] & 0xFF;
                    if (unsigned == 70 && unsigned!=previous) {
                        wheelCalibration++;
                        Runnable newRunnable = new Runnable(){
                            @Override
                            public void run(){}

                        };

                    }
                    previous = unsigned;
                }
            } else {

                float[] points = new float[data.length];

                for (int i = 0; i < data.length; i++) {

                    int unsignedInt = data[i] & 0xFF;

                    if (i >= 0 && i <= 115) {
                        points[i] = applyFilter(unsignedInt, gradient1, i, 1);
                    } else if (i > 115 && i <= 231) {
                        points[i] = applyFilter(unsignedInt, gradient2, i, filterPart1);
                    } else if (i > 231 && i <= 347) {
                        points[i] = applyFilter(unsignedInt, gradient3, i, filterPart2);
                    } else if (i > 347 && i <= 463) {
                        points[i] = applyFilter(unsignedInt, gradient4, i, filterPart3);
                    } else if (i > 463 && i <= 560) {
                        points[i] = applyFilter(unsignedInt, gradient5, i, filterPart4);
                    } else if (i > 560 && i <= 580) {
                        points[i] = (float) unsignedInt;
                    }
                }

                if (withFFT) {
                    if (points.length >= 581 ) {
                        filter.feed(points);
                        points = filter.apply(points);
                    }

                }

//                float[] test = filter.test();
                Message waveData = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putFloatArray("data", points);
                waveData.setData(bundle);
                if (WaveThread.mHandler!=null) { //WaveThread.mHandler!=null && // if I do that I lose control over the thread
                WaveThread.mHandler.sendMessage(waveData);
                }

                Message spectroData = Message.obtain();
                Bundle other = new Bundle();
                other.putFloatArray("spectroData", points);
                spectroData.setData(other);
                if (SpectroThread.mHandler != null) { //SpectroThread.mHandler!=null &&
                SpectroThread.mHandler.sendMessage(spectroData);
                }

                if (WriteEradThread.mHandler != null) {

                    if (withWheel) {
                        int signalNow = data[573] & 0xFF;
                        if (signalNow == 70) {
                            isForward = true;
                        } else if (signalNow == 56) {
                            isForward = false;
                        }

                        if (isForward) {
                            Message writeData = Message.obtain();
                            Bundle write = new Bundle();
                            write.putByteArray("writeData", data);
                            writeData.setData(write);
                            WriteEradThread.mHandler.sendMessage(writeData);
                        }

                    } else {
                        Message writeData = Message.obtain();
                        Bundle write = new Bundle();
                        write.putByteArray("writeData", data);
                        writeData.setData(write);
                        WriteEradThread.mHandler.sendMessage(writeData);
                    }
                }


            }
        }

        @Override
        public void onRunError(Exception e) {


        }
    };

    public synchronized void setTimeWindow(int signal){
        timeWindow = signal;
    }

    public synchronized void setLocalFilter(int signal){
        localFilter = signal;
    }


    public synchronized void setFilterPart1(float filter){
        this.filterPart1 = filter;


    }

    public synchronized void setFilterPart2(float filter){
        this.filterPart2 = filter;


    }

    public synchronized void setFilterPart3(float filter){
        this.filterPart3 = filter;

    }

    public synchronized void setFilterPart4(float filter){
        this.filterPart4 = filter;

    }

    public synchronized void setFilterPart5(float filter){
        this.filterPart5 = filter;

    }

    public void calculateGradients(){
        gradient1 = (filterPart1 - 1)/116;
        gradient2 = (filterPart2-filterPart1)/116;
        gradient3 = (filterPart3-filterPart2)/116;
        gradient4 = (filterPart4 - filterPart3)/116;
        gradient5 = (filterPart5-filterPart4)/116;
    }

    /*     applies filter to a point
         @param unsigned - raw sample value
         @param gradient - gradient for calculating filter
         @param position - the position of the point in the byte array from radar
         @param minValue - the preceding filter
         @return the filtered value of the point
        */
    private float applyFilter(int unsigned, float gradient,int position, float minValue){

        int step = position/116;
        int minX = step * 116;

        float point = 0;
        float filter = ((position-minX) * gradient)+minValue;


        if (unsigned < 128){
            point = 128 - ((128 - (float) unsigned) * filter);


            if (point <0){
                point = 0;
            }

        } else if (unsigned >127){
            point = (((float)unsigned - 128)*filter) + 128;


            if (point >255){
                point = 255;
            }
        }

        return point;
    }



}
