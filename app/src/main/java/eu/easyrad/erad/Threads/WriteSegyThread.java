package eu.easyrad.erad.Threads;

import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.SegyBinaryHeader;
import eu.easyrad.erad.Model.SegyTextHeader;
import eu.easyrad.erad.Model.SegyTrace;

/**
 * Created by vanya on 15-8-20.
 */
public class WriteSegyThread extends Thread {

    public static final String TAG = WriteSegyThread.class.getSimpleName();

    public static android.os.Handler mHandler;
    private boolean running;
    private String filename;

    Calendar calendar;
    float[] data;
    int sample_interval;


    public WriteSegyThread(String filename){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "WriteSegyThread");
        }
        this.filename = filename;
        running = false;
        sample_interval = (int)(15*1.66);
        i=1;
        calendar = Calendar.getInstance();

    }
    int i;

    @Override
    public void run() {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "run");
        }
        while (running) {
//            Looper.prepare();
            SegyTextHeader segyTextHeader = new SegyTextHeader();
            SegyBinaryHeader segyBinaryHeader = new SegyBinaryHeader();

            segyTextHeader.setLocation("Plovdiv");
            segyTextHeader.setMedium("medium");
            segyTextHeader.setOperator("operator");
            segyTextHeader.setTimeWindow(15);

            segyBinaryHeader.setJob_id_number(1);
            segyBinaryHeader.setLine_number(1);
            segyBinaryHeader.setReel_number(4);
            segyBinaryHeader.setSample_interval((short) sample_interval);
            segyBinaryHeader.setSamples_per_data_trace((short) 581);



            try {
                FileOutputStream fos = new FileOutputStream(filename);
                final DataOutputStream dos = new DataOutputStream(fos);

                segyTextHeader.write(dos);

                segyBinaryHeader.write(dos);


                Looper.prepare();
                /*if (running) {

                    mHandler = new android.os.Handler() {
                        @Override
                        public void handleMessage(Message msg) {

                            super.handleMessage(msg);
                            data = msg.getData().getFloatArray("writeData");



                            SegyTrace segyTrace = new SegyTrace(data);

                            segyTrace.setTrace_sequence_line(i);
                            segyTrace.setField_record(i);
                            segyTrace.setEnsemble_num(i);
                            segyTrace.setTrace_sequence_segy(i);

                            segyTrace.setNum_of_samples((short) data.length);
                            segyTrace.setSample_interval((short) sample_interval);
                            segyTrace.setDay((short) calendar.get(Calendar.DAY_OF_YEAR));
                            segyTrace.setYear((short) calendar.get(Calendar.YEAR));
                            segyTrace.setMinute((short) calendar.get(Calendar.MINUTE));
                            segyTrace.setHour((short) calendar.get(Calendar.HOUR_OF_DAY));
                            segyTrace.setSecond((short) calendar.get(Calendar.SECOND));



                            i++;


                            try {
                                segyTrace.write(dos);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    };

                }*/
//                Looper.loop();
                for (int i=1; i<=42; i++){
                    dos.writeByte(0);
                }

                dos.flush();
                dos.close();



            } catch (IOException e) {

            }
//            Looper.loop();
        }
    }

    public void setRunning(boolean isRunning){
        this.running = isRunning;
    }

    public boolean getRunning(){
        return running;
    }

}
