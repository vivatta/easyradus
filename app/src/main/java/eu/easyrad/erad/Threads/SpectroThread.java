package eu.easyrad.erad.Threads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;

import java.util.ArrayList;

import eu.easyrad.erad.Activities.MainActivity;
import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Gradient;
import eu.easyrad.erad.Model.Utilities;


/**
 * Created by vanya on 15-8-20.
 */
public class SpectroThread extends Thread{

    public static final String TAG = SpectroThread.class.getSimpleName();

    private SurfaceHolder holder;
    public static Handler mHandler;

    //data dump
    private float[] data;

    private boolean running = false;
    private boolean spatialUpdate = false;
    private boolean imperial;

    private int spatialUpdateSteps=0;
    public float canvasHeight;
    public int canvasWidth;
    public int paletteFlag;
    private int pxPerDp;
    private int brushSize;
    private int smallBrushSize;

    //scaling
    private float coefficientY;
    private final int SIGNAL_SIZE = 580;

    //on-screen canvas
    private Canvas canvas;
    public Paint paint;
    private Paint labelPaint;

    //the buffered canvas that is drawn on the surface
    private Canvas bufferedCanvas;
    private Bitmap bufferedBitmap;

    //another buffered canvas to store what has been drawn thus far (would try to get rid of it)
    private Bitmap offLoadBitmap;
    private Canvas offLoadCanvas;

    private Matrix matrix;

    //coordinates of any given point
    private float y;
    private float x;

    //x coordinate incremented on every new signal
    private int traceCounter;

    //pointer to color value
    private int colorValue;

    //helper rects for scroll
    private Rect source;
    private Rect destination;
    private Rect spatialSource;
    private Rect spatialDestination;

    //palette modes
    public static final int MONO =1;
    public static final int RED = 2;
    public static final int BLUE = 3;
    public static final int GREEN = 4;
    public static final int PALETTE_5 = 5;
    public static final int PALETTE_RAINBOW = 6;

    ArrayList<Float> labels;

    //0-black; 100- black; 120 - gray; 255 - white;
    private Gradient mono = new Gradient(
            new int[]{0, 100,120,140, 255},
            new int[]{Color.rgb(0,0,0),
                    Color.rgb(0,0,0),
                    Color.rgb(153,153,153),
                    Color.rgb(255,255,255),
                    Color.rgb(255,255,255)});

    //0-deep red; 120-ERred; 131-ERgreen; 255-ERgreen
    private Gradient green = new Gradient(
            new int[]{0,120,131,255},
            new int[]{Color.rgb(141,16,21),
                    Color.rgb(236,28,36),
                    Color.rgb(132,194,37),
                    Color.rgb(132,194,37)}
    );

    //0-yellow; 115-red; 128-green; 135-blue;255-black
    private Gradient red = new Gradient(
            new int[]{0,115,128,135,255},
            new int[]{Color.rgb(255,255,0),
                    Color.rgb(255,0,0),
                    Color.rgb(0,255,0),
                    Color.rgb(0,0,255),
                    Color.rgb(0,0,0)}
    );

    //0-blue; 120-black, 131-yellow, 255-yellow
    private Gradient blue = new Gradient(
            new int[]{0,100,120,131,255},
            new int[]{Color.rgb(0, 255, 255),
                    Color.rgb(0, 255, 255),
                    Color.rgb(0,0,0),
                    Color.rgb(255,255,0),
                    Color.rgb(255,255,0)}
    );

    //0-dark red; 115-red; 128-yellow; 140-blue;255-dark blue;
    private Gradient palette5 = new Gradient(
            new int[]{0,115,128,140,255},
            new int[]{Color.rgb(102,0,0),
                    Color.rgb(255,0,0),
                    Color.rgb(255,255,0),
                    Color.rgb(0,0,255),
                    Color.rgb(102,0,0)}
    );

    private Gradient rainbow = new Gradient(
            new int[]{0,17,34,51,68,85,102,119,136,153,170,187,204,221,238,255},
            new int[]{Color.rgb(187,170,188),
                    Color.rgb(136,25,130),
                    Color.rgb(18, 9, 136),
                    Color.rgb(39,113,2),
                    Color.rgb(254, 22, 0),
                    Color.rgb(105, 36, 29),
                    Color.rgb(60,58,61),
                    Color.rgb(60, 58, 61),
                    Color.rgb(130, 121, 122),
                    Color.rgb(145,11,2),
                    Color.rgb(253,33,0),
                    Color.rgb(251,238,2),
                    Color.rgb(5, 215, 2),
                    Color.rgb(2,20,250),
                    Color.rgb(253,58,250),
                    Color.rgb(255,255,255)}
    );


    public SpectroThread(SurfaceHolder holder, int pxPerDp ){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new SpectroThread");
        }
        this.holder = holder;
        this.pxPerDp = pxPerDp;

        //init paint elements
        paint = new Paint();
        paint.setStrokeWidth(dpToPx(brushSize));
        paint.setColor(Color.argb(255, 255, colorValue, 255)); // default value

        labelPaint = new Paint();
        labelPaint.setTextSize(dpToPx(15));
        labelPaint.setColor(Color.WHITE);

        paletteFlag = MONO;

        bufferedCanvas = new Canvas();
        offLoadCanvas = new Canvas();

        matrix = new Matrix();
        traceCounter = 0;
    }


    public void setCanvasHeight(int newHeight){
        this.canvasHeight = (float) newHeight;
        coefficientY = canvasHeight/SIGNAL_SIZE;
    }

    public void setCanvasWidth(int newWidth){
        this.canvasWidth = newWidth;

        int numberOfLabels = (int) canvasWidth/dpToPx(60);

        if (spatialUpdateSteps!=0) {
            if (labels != null) {
                int newLabels = numberOfLabels - labels.size();
                for (int i = 0; i < newLabels; i++) {
                    float next = labels.get(labels.size() - 1) + (dpToPx(60) / dpToPx(brushSize)) / 2;
                    labels.add(next);
                }
            } else {
                labels = new ArrayList<>();

                for (int i = 0, j = dpToPx(10); i < numberOfLabels; i++, j += dpToPx(60)) {
                    int traceIndexAtPoint = (int) j / dpToPx(brushSize); //5
                    //TODO find out why we divide by 2 pls
                    float distanceFloat = (traceIndexAtPoint / 2) * (100 / spatialUpdateSteps);

                    labels.add(distanceFloat);
                }
            }
        }

    }




    public void setBrushSize(int brushSize){
        this.brushSize = brushSize;

        if (brushSize-1<=0){
            smallBrushSize = 1;
        } else {
            smallBrushSize = brushSize - 1;
        }
    }

    public void setImperial(boolean imperial){
        this.imperial = imperial;
    }

    public void setBufferedBitmaps(int width, int height){
        bufferedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        bufferedCanvas.setBitmap(bufferedBitmap);

        offLoadBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        offLoadCanvas.setBitmap(offLoadBitmap);

        source = new Rect(dpToPx(smallBrushSize),0,offLoadBitmap.getWidth(), offLoadBitmap.getHeight()); //leaving 2dp room for new line  //3
        destination = new Rect(0, 0, bufferedBitmap.getWidth()-dpToPx(smallBrushSize), bufferedBitmap.getHeight()); //2

        spatialSource = new Rect(dpToPx(brushSize),0,offLoadBitmap.getWidth(), offLoadBitmap.getHeight()); //5
        spatialDestination = new Rect(0,0,bufferedBitmap.getWidth()-dpToPx(brushSize), offLoadBitmap.getHeight()); //5
    }

    public void setPaintColor(int pointValue){
        switch (paletteFlag){
            case MONO:
                paint.setColor(mono.getColorForPoint(pointValue));
                break;

            case GREEN:
                paint.setColor(green.getColorForPoint(pointValue));

                break;

            case RED:
                paint.setColor(red.getColorForPoint(pointValue));

                break;
            case BLUE:
                paint.setColor(blue.getColorForPoint(pointValue));
                break;

            case PALETTE_5:
                paint.setColor(palette5.getColorForPoint(pointValue));
                break;

            case PALETTE_RAINBOW:
                paint.setColor(rainbow.getColorForPoint(pointValue));
                break;
        }
    }

    public void setSpatialUpdate(boolean isSpatial){
        spatialUpdate = isSpatial;
    }

    public void setSpatialUpdateSteps(int steps){
        spatialUpdateSteps = steps;
    }

    public void setPaletteFlag(int flag){
        this.paletteFlag = flag;
    }

    public void setRunning(boolean isRunning) {
        running = isRunning;
    }




    public void reset(){
        traceCounter = 1;
        bufferedCanvas.drawColor(Color.BLACK);
    }

    private int dpToPx(int dp){
        return dp*pxPerDp;
    }

    public void recycleBitmaps(){
        bufferedBitmap.recycle();
        offLoadBitmap.recycle();
    }



    public Bitmap getBufferedBitmap(){
        return bufferedBitmap;
    }

    public int getPaletteFlag(){
        return paletteFlag;
    }


    int previous;
    float distance;
    boolean forward;

    @Override
    public void run(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "run");
        }
        while (running){

            previous = 48;
            distance = 0;
            forward = true;

            Looper.prepare();

            canvas = null;
            mHandler = new Handler(){
                @Override
                public void handleMessage(Message msg){
                    super.handleMessage(msg);
                    data = msg.getData().getFloatArray("spectroData");

                    if (spatialUpdate && data.length>=572){

                        //update distance
                        int signalNow = (int) data[573];


                        if (signalNow == 70 && signalNow!=previous) {
                            forward = true;

                        } else if (signalNow == 56 && signalNow!= previous){
                            forward = false;
                        }

                        if (traceCounter == canvasWidth) {
                            bufferedCanvas.drawBitmap(offLoadBitmap, spatialSource, spatialDestination, null);
                            for (int i=0; i<labels.size(); i++){
                                float current = labels.get(i);
                                current += 100/spatialUpdateSteps;
                                labels.set(i, current);
                            }
                            traceCounter -= dpToPx(brushSize);
                        }

                        if (traceCounter == 0){
                            bufferedCanvas.drawBitmap(offLoadBitmap, spatialDestination, spatialSource, null);
                            traceCounter +=dpToPx(brushSize);
                        }

                        //draw labels
                        labelPaint.setARGB(255,255,255,255);
                        bufferedCanvas.drawRect(0, 560 * coefficientY, canvasWidth, canvasHeight, labelPaint);
                        labelPaint.setARGB(255, 132, 194, 37);
                        labelPaint.setStrokeWidth(dpToPx(2));
                        int labelCount = (int)canvasWidth/dpToPx(60);


                        for (int i=0, j=dpToPx(10); i<labelCount; i++, j+=dpToPx(60)){
                            labelPaint.setARGB(255, 132, 194, 37);
                            bufferedCanvas.drawLine(j, 560*coefficientY, j, 577*coefficientY, labelPaint);
                            String distance;
                            if (imperial){
                                distance = Utilities.metricToImperial(labels.get(i)/100);
                            } else {
                                distance = String.valueOf(labels.get(i) / 100);
                            }
                            labelPaint.setARGB(255,0,0,0);
                            bufferedCanvas.drawText(distance, j+2, 577*coefficientY, labelPaint);
                        }





                        if (forward && signalNow!= previous){



                            for (int i=0; i<data.length-20; i++){
                                colorValue = (int) data[i];
                                setPaintColor(colorValue);
                                x = (float) traceCounter;
                                y = (float) i * coefficientY;


                                paint.setStrokeWidth(dpToPx(brushSize));


                                bufferedCanvas.drawPoint(x, y, paint);
                                labelPaint.setStrokeWidth(dpToPx(1));
                                labelPaint.setColor(Color.BLACK);
                                bufferedCanvas.drawLine(x+dpToPx(2),0,x+dpToPx(2), 560*coefficientY, labelPaint); //3

                            }
                            traceCounter += dpToPx(brushSize); //was 5

                            if (traceCounter >canvasWidth){
                                traceCounter = canvasWidth;
                            }



                        }

                        if (!forward && signalNow!=previous){

                            traceCounter -= dpToPx(brushSize); //5

                            if (traceCounter <0){
                                traceCounter = 0;
                            }

                            x = (float) traceCounter;

                            bufferedCanvas.drawBitmap(offLoadBitmap, matrix, null);
                            labelPaint.setStrokeWidth(dpToPx(1));
                            labelPaint.setColor(Color.WHITE);
                            bufferedCanvas.drawLine(x, 0, x, 560*coefficientY, labelPaint);
                        }
                        previous = signalNow;

                        try {
                            canvas = holder.lockCanvas();

                            synchronized (holder){
                                if (canvas != null) {

                                    canvas.drawBitmap(bufferedBitmap, matrix, null); //drawing to the back buffer from bufferedBitmap
                                    if(forward) { // if going backwards don't overwrite this
                                        offLoadCanvas.drawBitmap(bufferedBitmap, matrix, null);
                                    }

                                }
                            }
                        }finally {
                            if (canvas != null){
                                holder.unlockCanvasAndPost(canvas); //switch back buffer with front buffer
                            }
                        }


                    } else {

                        if (traceCounter < canvasWidth) {
                            traceCounter += dpToPx(smallBrushSize); //3
                        } else if (traceCounter > canvasWidth) {
                            traceCounter = canvasWidth;
                        }


                        for (int i = 0; i < data.length; i++) {

                            colorValue = (int) data[i];
                            setPaintColor(colorValue);
                            x = (float) traceCounter;
                            y = (float) i * coefficientY;
                            paint.setStrokeWidth(dpToPx(smallBrushSize)); //3
                            bufferedCanvas.drawPoint(x, y, paint);

                        }


                    try {
                        canvas = holder.lockCanvas();

                        synchronized (holder){
                            if (canvas != null) {

                                canvas.drawBitmap(bufferedBitmap, matrix, null); //drawing to the back buffer from bufferedBitmap
                                offLoadCanvas.drawBitmap(bufferedBitmap, matrix, null); //
                            } else {
                                return;
                            }
                        }
                    }finally {
                        if (canvas != null){
                            holder.unlockCanvasAndPost(canvas); //switch back buffer with front buffer
                        }
                    }

                    if (traceCounter == canvasWidth) {

                        bufferedCanvas.drawBitmap(offLoadBitmap, source, destination, null);
                    }
                    }


                }
            };
            Looper.loop();
        }
    }


}
