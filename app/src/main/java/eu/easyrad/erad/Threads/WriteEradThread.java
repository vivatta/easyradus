package eu.easyrad.erad.Threads;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.EradFileHeader;
import eu.easyrad.erad.Model.EradTraceHeader;

/**
 * Created by vanya on 15-12-22.
 */
public class WriteEradThread extends Thread {

    public static final String TAG = WriteEradThread.class.getSimpleName();

    public static Handler mHandler;
    private boolean running;
    private boolean finished;
    private String fileName;

    private long index; //start at 0
    private int slice_number; // start at 0
    private int trace_number_slice; // start at 0

    private short radar;
    private short dimension;
    private float timeWindow;
    private float x;
    private float y;
    private short steps;
    private float dielectric_coefficient;
    private short offset_to_data;
    private String operator;
    private String location;

    private long totalTraceCount;

    Calendar calendar;
    byte[] data;


    public void setRunning(boolean running) {
        this.running = running;
    }

    public void setRadar(short radar) {
        this.radar = radar;
    }

    public void setDimension(short dimension) {
        this.dimension = dimension;
    }

    public void setTimeWindow(float timeWindow) {
        this.timeWindow = timeWindow;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setSteps(short steps) {
        this.steps = steps;
    }

    public void setDielectric_coefficient(float dielectric_coefficient){
        this.dielectric_coefficient = dielectric_coefficient;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isFinished(){
        return finished;
    }

    public void setFinished(boolean finished){
        this.finished = finished;
    }


    public void resetTraceNumberSlice(){
        trace_number_slice = 0;
    }

    public void incrementSliceNumber(){
        slice_number++;
    }


    public WriteEradThread(String fileName, boolean is2d){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "new WriteEradThread");
        }
        this.fileName = fileName;
        running = false;
        index = 1;
        calendar = Calendar.getInstance();

        if (is2d){
//            dimension = 1;
            x = 0;
            y = 0;
        } else {
            dimension = 2;
        }
        totalTraceCount = 0;
        finished = false;

    }

    @Override
    public void run(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "run");
        }
        while (running){
            EradFileHeader fileHeader = new EradFileHeader();


            fileHeader.setRadarType(radar);

            fileHeader.setYear((short) calendar.get(Calendar.YEAR));

            fileHeader.setMonth((short) (calendar.get(Calendar.MONTH) + 1));

            fileHeader.setDay((short) calendar.get(Calendar.DAY_OF_MONTH));

            fileHeader.setDimension(dimension);

            fileHeader.setTimeWindow(timeWindow);

            fileHeader.setX(x);

            fileHeader.setY(y);

            fileHeader.setWheelCalibrationSteps(steps);

            fileHeader.setDielectric_coefficient(dielectric_coefficient);

            fileHeader.setOperator(operator);

            fileHeader.setLocation(location);

            try {
                FileOutputStream fos = new FileOutputStream(fileName);
                final DataOutputStream dos = new DataOutputStream(fos);
                fileHeader.write(dos);
                dos.flush();

                Looper.prepare();
                mHandler = new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        data = msg.getData().getByteArray("writeData");
                        if (data.length == 581) {
                            EradTraceHeader eradTraceHeader = new EradTraceHeader();
                            eradTraceHeader.setIndex(index);
                            eradTraceHeader.setSamples((short) data.length);
                            eradTraceHeader.setHour((short) calendar.get(Calendar.HOUR_OF_DAY));
                            eradTraceHeader.setMinute((short) calendar.get(Calendar.MINUTE));
                            eradTraceHeader.setSecond((short) calendar.get(Calendar.SECOND));
                            eradTraceHeader.setMillisecond(calendar.get(Calendar.MILLISECOND));
                            eradTraceHeader.setSliceNumber(slice_number);
                            eradTraceHeader.setTraceNumberSlice(trace_number_slice);


                            try {
                                eradTraceHeader.write(dos);
                                dos.flush();

                                for (int i = 0; i < data.length; i++) {
                                    dos.writeByte(data[i]);
                                }
                                dos.flush();

                                totalTraceCount++;

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            index++;
                            if (dimension == 2) {
                                trace_number_slice++;
                            }


                        }
                    }
                };
                Looper.loop();
                dos.writeLong(totalTraceCount);
                dos.flush();

                dos.close();
                finished = true;



            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}
