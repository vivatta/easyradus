package eu.easyrad.erad.View;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Threads.WaveThread;

/**
 * Created by vanya on 15-7-17.
 */
public class WaveSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    public static final String TAG = WaveSurfaceView.class.getSimpleName();
    private SurfaceHolder surfaceHolder;
    private WaveThread waveThread = null;
    volatile boolean running = false;


    public WaveSurfaceView(Context context){
        super(context);
        init();
    }

    public WaveSurfaceView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        init();
    }

    public WaveSurfaceView(Context context, AttributeSet attributeSet, int defStyle){
        super(context, attributeSet, defStyle);
        init();
    }

    public void init(){

        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

    }

    public boolean getRunning(){
        return running;
    }

    public void onResumeWaveSurfaceView(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResumeWaveSurfaceView");
        }
        running = true;
        waveThread = new WaveThread(surfaceHolder, getContext());
        waveThread.start();

    }

    public void pauseWaveSurfaceView(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "pauseWaveSurfaceView");
        }
        running = false;
        if (waveThread != null) {
            waveThread.setRunning(false);
        } else {

        }

    }

    public void stopWaveThread(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopWaveThread");
        }
        if (waveThread!=null){
            waveThread.setRunning(false);
            WaveThread.mHandler.getLooper().quit();
            WaveThread.mHandler = null;
        }
    }





    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "surfaceCreated");
        }
        this.setWillNotDraw(false);
        waveThread = new WaveThread(holder, getContext());
        waveThread.setRunning(true);
        waveThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "surfaceChanged");
        }
        waveThread.setCanvasHeight(height);
        waveThread.setCanvasWidth(width);

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "surfaceDestroyed");
        }
        waveThread.setRunning(false);


    }

}
