package eu.easyrad.erad.View;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.speech.tts.Voice;
import android.telephony.NeighboringCellInfo;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Utilities;
import eu.easyrad.erad.R;

/**
 * Custom vertical progress bar used for visualising location
 */
public class VerticalProgressBar extends FrameLayout {

    public static final String TAG = VerticalProgressBar.class.getSimpleName();

    boolean loadsUpwards;
    int max;
    int height;
    int progress;
    double singleExpandAmount;


    View innerView;
    ImageView imageView;
    Context context;

    public VerticalProgressBar(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public VerticalProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.VerticalProgressBar,
                0,0);

        try {
            loadsUpwards = a.getBoolean(R.styleable.VerticalProgressBar_loadsUpwards, false);
            max = a.getInteger(R.styleable.VerticalProgressBar_max, 0);
            progress = a.getInteger(R.styleable.VerticalProgressBar_progress, 0);
            if (progress>max) progress=max;

        } finally {
            a.recycle();
        }

        this.context = context;
        init();
    }

    public VerticalProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.VerticalProgressBar,
                0,0);

        try {
            loadsUpwards =  a.getBoolean(R.styleable.VerticalProgressBar_loadsUpwards, false);
            max = a.getInteger(R.styleable.VerticalProgressBar_max, 0);
            progress = a.getInteger(R.styleable.VerticalProgressBar_progress, 0);
            if (progress>max) progress=max;


        } finally {
            a.recycle();
        }

        this.context = context;
        init();
    }

    private void init(){
        this.setWillNotDraw(false);
        innerView = new View(context);
        innerView.setBackgroundColor(0xFF84c225);

        imageView = new ImageView(context);

        FrameLayout.LayoutParams imageViewParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageViewParams.gravity = Gravity.CENTER;
        FrameLayout.LayoutParams innerViewParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);

        if (loadsUpwards){
            innerViewParams.gravity = Gravity.BOTTOM;
            imageView.setImageResource(R.drawable.icon_arrow_up);
        } else {
            innerViewParams.gravity = Gravity.TOP;
            imageView.setImageResource(R.drawable.icon_arrow_down);
        }

        ShapeDrawable drawable = new ShapeDrawable();
        Paint paint = drawable.getPaint();
        paint.setColor(Color.GRAY);
        paint.setStrokeWidth(Utilities.dpToPx(context, 1));
        paint.setStyle(Paint.Style.STROKE);

        this.setBackground(drawable);
        this.addView(imageView, 0, imageViewParams);
        this.addView(innerView, 1, innerViewParams);



    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh){
        super.onSizeChanged(w,h,oldw,oldh);
        height = h;

    }

    @Override
    public void onDraw(Canvas canvas){

        if (height!=0 && max!=0 && progress!=0) {

            singleExpandAmount = (double)height / (double)max;
            innerView.getLayoutParams().height = (int)(progress * singleExpandAmount);
            innerView.requestLayout();
        }
        super.onDraw(canvas);

    }



    public void setMax(int max){
        this.max = max;
    }

    public void setProgress(int progress){
        int height = this.getHeight();
    }

    public synchronized void updateProgress(){
        progress++;
        animateProgress();
    }

    public synchronized void incrementProgress() {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "incrementProgress");
        }
        progress++;
        animateProgress();
    }

    public synchronized void decrementProgress(){
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "decrementProgress");
        }
        progress--;
        animateProgress();
    }

    public void setLoadsUpwards(boolean loadsUpwards){
        this.loadsUpwards = loadsUpwards;

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) innerView.getLayoutParams();

        if (loadsUpwards){
            params.gravity = Gravity.BOTTOM;
            imageView.setImageResource(R.drawable.icon_arrow_up);
        } else {
            params.gravity = Gravity.TOP;
            imageView.setImageResource(R.drawable.icon_arrow_down);
        }

        this.requestLayout();
    }


    public boolean loadsUpwards(){
        return loadsUpwards;
    }

    public int getMax(){
        return max;
    }

    public int getProgress(){
        int progress = 0;
        if (innerView.getHeight()!=0 && singleExpandAmount!=0){
            progress = (int) (innerView.getHeight() / singleExpandAmount);
        }
        return progress;
    }

    private synchronized void animateProgress(){
//        int height = this.getHeight();

        singleExpandAmount = height/max;

/*
        Log.i(TAG, "singleExpandAmount: " + singleExpandAmount + " progress: " + progress + " innerView.Height: " + (int)(progress * singleExpandAmount));
        Log.i(TAG, "innerView.height before: " + innerView.getLayoutParams().height);
        innerView.getLayoutParams().height = (int)(progress * singleExpandAmount);
        Log.i(TAG, "innerView.height after: " + innerView.getLayoutParams().height);


//        innerView.postInvalidate();
//        innerView.requestLayout();

        invalidate();
        requestLayout();

        Log.i(TAG, "innerView.height after layoutRequest: " + innerView.getLayoutParams().height);
*/

        int originalSize = innerView.getHeight();
        ValueAnimator animator = ValueAnimator.ofInt(originalSize, (int)(progress * singleExpandAmount));
        animator.setDuration(20);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                innerView.getLayoutParams().height = value.intValue();
//                innerView.requestLayout();
                innerView.postInvalidate();
                postInvalidate();
            }
        });
        animator.start();


    }


}
