package eu.easyrad.erad.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by vanya on 16-6-14.
 */
public class LocationTrackerView extends RelativeLayout {

    public static final String TAG = LocationTrackerView.class.getSimpleName();


    public LocationTrackerView(Context context) {
        super(context);
    }

    public LocationTrackerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocationTrackerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




}
