package eu.easyrad.erad.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import eu.easyrad.erad.BuildConfig;
import eu.easyrad.erad.Model.Constants;
import eu.easyrad.erad.Threads.Erad2DrawThread;
import eu.easyrad.erad.Threads.SpectroThread;


/**
 * Created by vanya on 15-9-8.
 */
public class SpectroSurfaceView extends SurfaceView implements SurfaceHolder.Callback{

    public static final String TAG = SpectroSurfaceView.class.getSimpleName();

    private SurfaceHolder holder;
    private SpectroThread spectroThread = null;
    private Erad2DrawThread eradDrawThread_2d = null;
    volatile boolean running = false;
    boolean imperial;
    int lastPalette;
    short workMode;

    int traceCount;
    int steps;
    int pxPerDp;
    int brushSize;
    boolean withWheel;


    public SpectroSurfaceView(Context context){
        super(context);
        init();
    }

    public SpectroSurfaceView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        init();
    }

    public SpectroSurfaceView(Context context, AttributeSet attributeSet, int defStyle){
        super(context, attributeSet, defStyle);
        init();
    }

    private void init(){

        holder = getHolder();
        holder.addCallback(this);

    }




    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "surfaceCreated");
        }
        this.setWillNotDraw(false);
        holder.setFormat(0x00000004); //less space, doesn't lag
        if (workMode == Constants.MODE_SCAN) {
            spectroThread = new SpectroThread(holder, dpToPx(1));
            spectroThread.setBrushSize(brushSize);
            spectroThread.setBufferedBitmaps(getWidth() + dpToPx(125), getHeight());  //+125dp to account for shrinking the wave view
            spectroThread.setSpatialUpdateSteps(steps);
            spectroThread.setImperial(imperial);
            spectroThread.setRunning(true);
            spectroThread.setPaletteFlag(lastPalette);
            spectroThread.start();

        } else if (workMode == Constants.MODE_READ_2D){
            eradDrawThread_2d = new Erad2DrawThread(holder, dpToPx(1));
            eradDrawThread_2d.setBufferedBitmaps(getWidth() + dpToPx(6), getHeight());
            eradDrawThread_2d.setRunning(true);
            eradDrawThread_2d.setTraceCount(traceCount);
            eradDrawThread_2d.setPaletteFlag(lastPalette);

            eradDrawThread_2d.start();
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "surfaceChanged");
        }
        if (workMode==Constants.MODE_SCAN) {
            spectroThread.setCanvasWidth(width);
            spectroThread.setCanvasHeight(height);

        } else if (workMode ==Constants.MODE_READ_2D){
            eradDrawThread_2d.setCanvasWidth(width);
            eradDrawThread_2d.setCanvasHeight(height);
            eradDrawThread_2d.setRecordedWithWheel(withWheel);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (BuildConfig.DEBUG){
            Log.i(TAG, "surfaceDestroyed");
        }
        if (workMode == Constants.MODE_SCAN) {
            spectroThread.setRunning(false);
            spectroThread.recycleBitmaps();
            SpectroThread.mHandler.getLooper().quit();


        } else if (workMode == Constants.MODE_READ_2D){
            eradDrawThread_2d.setRunning(false);
            eradDrawThread_2d.recycleBitmaps();
            Erad2DrawThread.mHandler.getLooper().quit();

        }
    }




    public void setWorkMode(short workMode){

        this.workMode = workMode;
    }

    public void setImperial(boolean imperial){
        this.imperial = imperial;
    }

    public void setPxPerDp(int pxPerDp){
        this.pxPerDp = pxPerDp;
    }

    public void setUpdateMode(boolean isSpatial){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setUpdate mode: isSpatial " + isSpatial);
        }
        if (spectroThread != null){
            spectroThread.setSpatialUpdate(isSpatial);
        }
    }

    public void setSpatialUpdateSteps(int steps){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setSpatialUpdateSteps "  + steps);
        }
        this.steps = steps;
        if (workMode==Constants.MODE_SCAN) {
            if (spectroThread != null) {
                spectroThread.setSpatialUpdateSteps(steps);
            }
        }
    }

    public void setRecordedWithWheel(boolean withWheel){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setRecordedWithWheel " + withWheel);
        }
        this.withWheel = withWheel;
        if (eradDrawThread_2d!=null){
            eradDrawThread_2d.setRecordedWithWheel(withWheel);
        }
    }

    public void setPalette(int palette){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setPalette " + palette);
        }
        lastPalette = palette;
        if (workMode==Constants.MODE_SCAN) {
            if (spectroThread != null) {
                spectroThread.setPaletteFlag(palette);
            } else {

            }

        } else if (workMode==Constants.MODE_READ_2D){
            if (eradDrawThread_2d != null){
                eradDrawThread_2d.setPaletteFlag(palette);
            } else  {

            }
        }
    }

    public void setBrushSize(int brushSize){
        this.brushSize = brushSize;
    }

    public void setTraceCount(int traceCount){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "setTraceCount " + traceCount );
        }
        this.traceCount = traceCount;
        if (eradDrawThread_2d!=null){
            eradDrawThread_2d.setTraceCount(traceCount);
        }
    }



    public void onResumeSpectroSV(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onResumeSV");
        }
        running = true;
        spectroThread = new SpectroThread(holder, dpToPx(1));
        spectroThread.start();
    }

    public void onPauseSpectroSV(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "onPauseSV");
        }
        running = false;
        if (workMode == Constants.MODE_SCAN) {
            if (spectroThread != null) {
                spectroThread.setRunning(false);
            } else {

            }


            //not sure if i need this
        } else if (workMode == Constants.MODE_READ_2D){
            if (eradDrawThread_2d != null){
                eradDrawThread_2d.setRunning(false);
            } else {

            }
        }
    }

    public void stopSpectroThread(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "stopSpectroThread");
        }
        if (spectroThread!=null){
            spectroThread.setRunning(false);
            SpectroThread.mHandler.getLooper().quit();
            SpectroThread.mHandler = null;

        }

        if (eradDrawThread_2d!=null){
            eradDrawThread_2d.setRunning(false);
            Erad2DrawThread.mHandler.getLooper().quit();
            Erad2DrawThread.mHandler = null;
        }
    }



    private int dpToPx(int dp){
        return dp*pxPerDp;
    }

    public void test(){

        if (eradDrawThread_2d != null){
            eradDrawThread_2d.setSomeCounterForward();
        }
    }



    public void resetErad(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "resetErad");
        }
        if (eradDrawThread_2d!=null){
            eradDrawThread_2d.reset();
        }
    }

    public void resetSpectro(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "resetSPectro");
        }
         if (spectroThread!=null){
             spectroThread.reset();
         }
    }




    public int getLastDrawnTrace(){

        int lastDrawn = 0;
        if (eradDrawThread_2d!=null){
            lastDrawn = eradDrawThread_2d.getLastDrawn();
        } else {

        }
        return lastDrawn;
    }

    public int getFirstDrawnTrace(){

        int firstDrawn = 0;
        if (eradDrawThread_2d!=null){
            firstDrawn = eradDrawThread_2d.getFirstDrawn();
        } else {

        }
        return firstDrawn;
    }

    public Bitmap getBitmap(){
        if (BuildConfig.DEBUG){
            Log.i(TAG, "getBitmap");
        }
        Bitmap bitmap = null;
        if (workMode==Constants.MODE_SCAN){
            if (spectroThread!=null){
                bitmap = spectroThread.getBufferedBitmap();
            }
        } else{
            if (eradDrawThread_2d!=null){
                bitmap = eradDrawThread_2d.getBufferedBitmap();
            }
        }
         return  bitmap;
    }

}
