package eu.easyrad.erad.View;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import eu.easyrad.erad.Fragments.WaveFragment;
import eu.easyrad.erad.Model.Utilities;


/**
 * Created by vanya on 15-8-18.
 */
public class WaveControl extends FrameLayout{


    public final static String TAG = WaveControl.class.getSimpleName();
    LayoutInflater inflater;

    public Button minus;
    public Button plus;
    public TextView filter;
    private ExpandingBar bar;
    private Context context;


    private final int STATE_1 = 10;
    private final int STATE_2 = 40;
    private final int STATE_3 = 70;
    private final int STATE_4 = 100;
    private final int STATE_5 = 130;
    private final int STATE_6 = 160;
    private final int STATE_7 = 190;

/*
    private final float STATE_1 = (float)0.14;
    private final float STATE_2 = (float)0.28;
    private final float STATE_3 = (float)0.42;
    private final float STATE_4 = (float)0.57;
    private final float STATE_5 = (float)0.71;
    private final float STATE_6 = (float)0.85;
    private final float STATE_7 = 1;
*/

    public int counter;


    public WaveControl(Context context) {
        super(context);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(eu.easyrad.erad.R.layout.wave_control_view, this, true);
        this.context = context;
        init();
    }

    public WaveControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(eu.easyrad.erad.R.layout.wave_control_view, this, true);
        this.context = context;
        init();
    }

    public WaveControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(eu.easyrad.erad.R.layout.wave_control_view, this, true);

        this.context = context;
        init();
    }

    private void init(){
        filter = (TextView)findViewById(eu.easyrad.erad.R.id.filter);
        bar = (ExpandingBar)findViewById(eu.easyrad.erad.R.id.bar);
        minus = (Button)findViewById(eu.easyrad.erad.R.id.minus);
        plus = (Button)findViewById(eu.easyrad.erad.R.id.plus);
        counter = 3;

    }


    public int getCounter(){
        return counter;
    }

    public void setCounter(int counter){
        this.counter = counter;
        animateBar(counter);

    }



    public synchronized void incrementCounter(){
        if (counter <7){
            counter ++;
        }
        animateBar(counter);

    }

    public synchronized void decrementCounter(){
        if (counter >1){
            counter--;
        }
        animateBar(counter);
    }





    //animates bar to the correct position depending on the value of counter
    public synchronized void animateBar(int counter){
        int originalSize;
        if (Utilities.isLandscape(context)) {
            originalSize = bar.getWidth();


        } else {
            originalSize = bar.getHeight();


        }
        ValueAnimator animator = null;
        switch (counter){
            case 1:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_1));
                filter.setText("x" + WaveFragment.STEP_1);
                break;

            case 2:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_2));
                filter.setText("x" + WaveFragment.STEP_2);
                break;

            case 3:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_3));
                filter.setText("x" + WaveFragment.STEP_3);
                break;

            case 4:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_4));
                filter.setText("x" + WaveFragment.STEP_4);
                break;

            case 5:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_5));
                filter.setText("x" + WaveFragment.STEP_5);
                break;

            case 6:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_6));
                filter.setText("x" + WaveFragment.STEP_6);
                break;

            case 7:
                animator = ValueAnimator.ofInt(originalSize, Utilities.dpToPx(context, STATE_7));
                filter.setText("x" + WaveFragment.STEP_7);
                break;
        }
        if (animator != null) {
            animator.setDuration(100);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer value = (Integer) animation.getAnimatedValue();
                    if (Utilities.isLandscape(context)) {
                        bar.getLayoutParams().width = value.intValue();
                    } else {
                        bar.getLayoutParams().height = value.intValue();
                    }
                    bar.requestLayout();
                }
            });
            animator.start();
        }
    }




}
