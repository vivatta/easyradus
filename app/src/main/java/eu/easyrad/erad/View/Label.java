package eu.easyrad.erad.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.BounceInterpolator;

import java.util.ArrayList;
import java.util.List;

import eu.easyrad.erad.Model.Utilities;
import eu.easyrad.erad.R;

/**
 * Created by vanya on 15-8-19.
 */
public class Label extends View{

    public static final String TAG = Label.class.getSimpleName();

    private List<float[]> labelPosition;
    public List<String> labels;

    public int numOfValues;
    private Paint textPaint;
    private Paint linePaint;
    private boolean imperial;

    float timeWindow;
    float medium;


    public final static float AIR = 1;
    public final static float WATER = 81;
    public final static float SAND_DRY = 4.5f;
    public final static float SAND_WET = 27.5f;
    public static final float CLAY_WET = 11.5f;
    public static final float GRANITE = 6.5f;
    public static final float BASALT = 8;
    public static final float CONCRETE = 7;
    public static final float EM_SPEED_VACUUM = 0.3f;


    public Label(Context context) {
        super(context);
        init();
    }

    public Label(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Label(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int height = MeasureSpec.getSize(heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setLabelPosition(height);

    }


    private void init() {

        numOfValues = 9;
        setStaticLabels();


        textPaint = new Paint();
        textPaint.setColor(Color.parseColor("#414543"));
        textPaint.setTextSize(dpToPx(13));

        linePaint = new Paint();
        linePaint.setColor(getResources().getColor(R.color.EasyRadGreen));
        linePaint.setStrokeWidth(dpToPx(2));

    }



    public void setMedium(float medium){
        this.medium = medium;
        setStaticLabels();
        invalidate();

    }

    public void setTimeWindow(float window){
        this.timeWindow = window;
        setStaticLabels();
        invalidate();
    }

    public void setImperial(boolean imperial){
        this.imperial = imperial;
    }



    //sets the labels of the y axis
    private void setStaticLabels(){
        labels = null;
        labels = new ArrayList<>();

        //the maximum value (calculated distance) is propagation velocity*timeWindow
        // propagation velocity is EM_SPEED_VACUUM/Math.sqrt(dielectric coefficient)
        // The dielectric coefficient is denoted as the medium

        float maxValue = ((EM_SPEED_VACUUM/(float)Math.sqrt(medium))*timeWindow)/2;
        String finalValue;
        float minValue = 0;
        float interval = maxValue/numOfValues;
        for (int i=0; i<numOfValues; i++){
            minValue += interval;
            if (imperial){
                finalValue = Utilities.metricToImperial(minValue);
            } else {
                finalValue = String.format("%.2f", minValue);
            }
            labels.add(finalValue);

        }


    }

    private void setLabelPosition(int height){

        labelPosition = new ArrayList<>(numOfValues);

            float x = dpToPx(3);
            float distance = height/(numOfValues);
            float y = distance;
            for (int i=0; i<numOfValues; i++){

                float[] xy = {x, y};
                labelPosition.add(xy);
                y += distance;

                //This is to display the last label just above the end of the view
                if (y>=height){
                    y = height - dpToPx(10);
                }
            }
    }

    public List<float[]> getLabelPosition(){
        return labelPosition;

    }


    @Override
    protected void onDraw(Canvas canvas){

        for (int i=0; i<labels.size(); i++){
            String label = labels.get(i);
            float[] positions = labelPosition.get(i);
            canvas.drawLine(positions[0], positions[1], this.getWidth(), positions[1], linePaint);
            canvas.drawText(label, positions[0], positions[1]-dpToPx(2), textPaint);

        }

    }


    private int dpToPx(int dp){
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
    }
}
