package eu.easyrad.erad.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by vanya on 15-9-25.
 */
public class ExpandingBar extends View {

    public int color;
    public ExpandingBar(Context context) {
        super(context);
        init();
    }

    public ExpandingBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExpandingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        color = Color.argb(255, 132, 194, 37);
    }

    public void setColor(int newColor){
        this.color = newColor;
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawColor(color);
    }
}
